export WORKON_HOME=~/envs
source /usr/local/bin/virtualenvwrapper.sh

workon vagrant
cd /vagrant/ProjectTango
alias rs='cd /vagrant/ProjectTango;python manage.py runserver 0.0.0.0:9000'
