#!/usr/bin/env bash

# VIRTUALENV
echo "Create virtualenv"
mkdir /home/vagrant/envs
export WORKON_HOME=~/envs
source /usr/local/bin/virtualenvwrapper.sh
mkvirtualenv vagrant -p python3

# install python dependencies
pip install -r /vagrant/requirements/local.txt


echo "------------------------------------------------------------------------------"
echo "Project migrations and syncdb"
cd /vagrant/ProjectTango
python manage.py migrate contenttypes 0001 --fake
python manage.py migrate contenttypes
python manage.py migrate aircraft --fake
python manage.py migrate --fake-initial
python manage.py migrate
cd /vagrant

cp -p /vagrant/vagrant-files/templates/.bashrc /home/vagrant/.bashrc

echo "------------------------------------------------------------------------------"
echo "Provisioning finished!"
echo "------------------------------------------------------------------------------"
echo "run server:"
echo "cd /vagrant/aviesc/ProjectTango"
echo "python manage.py runserver 0.0.0.0:9000"
echo "Or use an alias:"
echo "rs"
echo ""
echo "run celery:"
echo "celery -A celery worker -l info"
echo "------------------------------------------------------------------------------"


