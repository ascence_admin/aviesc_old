#!/usr/bin/env bash

echo "Setup swap to overcome out of memory errors (fe. during the installation of lxml)"
swapfile="/swapfile"
if [ ! -f "$swapfile" ]
then
    fallocate -l 4G /swapfile
    chmod 600 $swapfile
    mkswap $swapfile
    swapon $swapfile
    echo "/swapfile     none    swap    sw      0       0" |tee -a /etc/fstab
fi

cd /vagrant/vagrant-files
#sudo dpkg -i mysql-connector-python_1.2.2-1ubuntu12.04_all.deb
apt-get update -y
apt-get install -y git python-pip python3-dev python-lxml libcairo2 libpango1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info rabbitmq-server libcurl4-openssl-dev
apt-get install -y libjpeg8 libjpeg8-dev libtiff-dev zlib1g-dev libfreetype6-dev liblcms2-dev libxml2-dev libxslt1-dev libssl-dev swig dos2unix

echo "Setup pip and virtualenv"
pip install virtualenv
pip install virtualenvwrapper

echo "Setup mysql in virtual machine"

## Installing MySQL
# use debconf to omit mysql's root password question
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'
apt-get install -y mysql-server libmysqlclient-dev
service mysql restart

# Create and load MySQL database
# ---------------------------------

DATABASE_NAME='vagrant'
if [ "$DATABASE_NAME" != "" ];
then
    # Create user
    echo "CREATE USER 'django_dev'@'localhost' IDENTIFIED BY 'Password1';" | mysql -u root -pvagrant
    echo "CREATE DATABASE tango;" | mysql -u root -pvagrant
    echo "GRANT ALL PRIVILEGES ON tango.* to 'django_dev'@'localhost';" | mysql -u root -pvagrant
    echo "FLUSH PRIVILEGES;" | mysql -u root -pvagrant

    # Create database
    if [ -f "/vagrant/vagrant-files/aviesc_db.sql" ];
    then
        mysql -u django_dev -pPassword1 < /vagrant/vagrant-files/aviesc_db.sql
    else
        echo "*******************************************************************"
        echo "*******************************************************************"
        echo "*******************************************************************"
        echo "*******************************************************************"
        echo "*******************************************************************"
        echo "Grab db dump and load db using: mysql -u django_dev -pPassword1 tango < /vagrant/vagrant-files/aviesc_db.sql"
        echo "*******************************************************************"
        echo "*******************************************************************"
        echo "*******************************************************************"
        echo "*******************************************************************"
        echo "*******************************************************************"
    fi
fi
## Restart MySQL to reload edited configuration file
service mysql restart


# Dependencies for wkhtml2pdf
apt-get install -y gsfonts fontconfig fonts-dejavu-core ttf-bitstream-vera fonts-freefont-ttf gsfonts-x11 fontconfig-config libfontconfig1
#cd /vagrant/
#mkdir wkhtml2pdf
#cd /vagrant/wkhtml2pdf
#wget http://downloads.sourceforge.net/project/wkhtmltopdf/0.12.2-dev/wkhtmltox-0.12.2-dev-200b9a6_linux-trusty-i386.deb
#wget http://download.gna.org/wkhtmltopdf/0.12/0.12.2.1/wkhtmltox-0.12.2.1_linux-precise-i386.deb
#dpkg -i wkhtmltox-0.12.2.1_linux-precise-i386.deb


#echo "------------------------------------------------------------------------------"
#echo "install/copy pdf libs"
#sudo cp -a lxml /usr/local/lib/python3.4/dist-packages
#sudo cp -a weasyprint /usr/local/lib/python3.4/dist-packages
