from rest_framework import serializers

from customer.models import Customer


class CustomerSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Customer
        fields = ('id', 'name', 'company', 'email', 'phone', 'address', 'city', 'zipcode',)