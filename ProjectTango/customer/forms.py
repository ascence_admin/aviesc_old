# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse

from crispy_forms_foundation.layout import Layout, Fieldset, Field, SplitDateTimeField, Row, RowFluid, Column, Div, \
    ButtonHolder, Submit, HTML
from crispy_forms.helper import FormHelper

from customer.models import Customer


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ('account', 'name', 'company', 'email', 'phone', 'address', 'city', 'zipcode')

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_action = '.'
        self.helper.form_method = 'post'
        self.helper.layout = Layout(
            Fieldset(
                _('Basic information'),
                Row(
                    Column('account', css_class='large-6'),
                ),
                Row(
                    Column('name', css_class='large-6'),
                    Column('company', css_class='large-6'),
                ),
            ),
            Fieldset(
                _('Contact'),
                Row(
                    Column('email', css_class='large-8'),
                    Column('phone', css_class='large-4'),
                ),
            ),
            Fieldset(
                _('Address'),
                Row(
                    Column('address', css_class='large-6'),
                    Column('city', css_class='large-4'),
                    Column('zipcode', css_class='large-2'),
                ),
            ),
            ButtonHolder(
                Submit('submit', _('Save')),
            ),
        )
        super(CustomerForm, self).__init__(*args, **kwargs)