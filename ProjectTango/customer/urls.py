from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import login_required, permission_required

from rest_framework import routers

from customer import views, api


router = routers.DefaultRouter()
router.register(r'api/customers', api.CustomerViewSet, base_name="api-customer")

urlpatterns = patterns("",
                       url(r"^$", login_required(views.CustomerList.as_view()), name="customer-list"),
                       url(r'^(?P<pk>[-_\w]+)/$', login_required(views.CustomerDetail.as_view()),
                           name='customer-detail'),
                       url(r"^(?P<pk>[-_\w]+)/update/$",
                           permission_required('aircraft.change_aircraft')(views.CustomerUpdate.as_view()),
                           name="customer-update"),
                       # API
                       url(r'^', include(router.urls)),
)