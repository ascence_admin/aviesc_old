from django.shortcuts import render_to_response, HttpResponseRedirect
from django.template.context import RequestContext
from django.contrib import messages
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse
from django.contrib.messages.views import SuccessMessageMixin

from customer.models import Customer
from customer.forms import CustomerForm
from core.constructs import ActionList, DetailList


class CustomerList(ListView):
    model = Customer
    template_name = 'customer/customer_list.html'

    def get_queryset(self):
        return Customer.objects.all()

    def get_context_data(self, **kwargs):
        #Add actions to the left hand actions
        context = super(CustomerList, self).get_context_data(**kwargs)
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('customer-list', kwargs={}), 'List Customers', True)
        #actions.insertAction(reverse('customer-create', kwargs={}), 'Add new')
        return context


class CustomerDetail(SuccessMessageMixin, DetailView):
    model = Customer

    def get_queryset(self):
        return Customer.objects.filter()

    def get_context_data(self, **kwargs):
        context = super(SuccessMessageMixin, self).get_context_data(**kwargs)
        invoice = context['object']

        #Add an "Update this" link to the left hand actions
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('customer-list', kwargs={}), 'List Customers')
        actions.insertAction(reverse('customer-update', kwargs={'pk': self.object.pk}), 'Update', True)
        #Display the audit fields on the left as well
        details = DetailList.GetLeftDetailsForContext(context)
        details.insertDetail("Account", self.object.account)

        return context


class CustomerUpdate(SuccessMessageMixin, UpdateView):
    model = Customer
    template_name = 'customer/customer_update.html'
    form_class = CustomerForm
    success_message = "Customer was updated successfully"
    success_url = '.'

    def get_queryset(self):
        return Customer.objects.all()

    def form_valid(self, form):
        response = super(SuccessMessageMixin, self).form_valid(form)
        messages.success(self.request, self.success_message)
        return response

    def get_context_data(self, **kwargs):
        #Add actions to the left hand actions
        context = super(CustomerUpdate, self).get_context_data(**kwargs)
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('customer-list', kwargs={}), 'List Customer')
        actions.insertAction(reverse('customer-detail', kwargs={'pk':context['object'].pk}), 'Back', True)
        return context