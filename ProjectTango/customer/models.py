# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

from core.models import AccountModel


class CustomerManager(models.Manager):
    # Not Implemented yet
    pass


class Customer(AccountModel):
    objects = CustomerManager()
    name = models.CharField(max_length=128, )
    company = models.CharField(max_length=64, )
    email = models.EmailField()
    phone = models.CharField(max_length=16, )
    address = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    zipcode = models.CharField(max_length=5,
                               blank=True,
                               default='')

    class Meta:
        verbose_name = "Customer"
        verbose_name_plural = "Customers"
        ordering = ('name', 'company', )

    def __unicode__(self):
        return "{0} - {1}".format(self.name, self.company)