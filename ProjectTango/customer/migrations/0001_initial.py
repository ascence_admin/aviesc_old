# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('name', models.CharField(max_length=128)),
                ('company', models.CharField(max_length=64)),
                ('email', models.EmailField(max_length=254)),
                ('phone', models.CharField(max_length=16)),
                ('address', models.CharField(max_length=100)),
                ('city', models.CharField(max_length=100)),
                ('zipcode', models.CharField(blank=True, max_length=5, default='')),
                ('account', models.ForeignKey(to='core.Account')),
            ],
            options={
                'verbose_name': 'Customer',
                'verbose_name_plural': 'Customers',
                'ordering': ('name', 'company'),
            },
        ),
    ]
