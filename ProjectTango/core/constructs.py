#################
## Actions 
#
# Used by the left hand menu, within Context objects

class ActionList():
    class Action():
        url = ''
        text = ''
        active = False
        disabled = False
        divider_before = False
        class_args = ''

        def __init__(self, *args, **kwargs):
            self.url = kwargs.pop('url', '')
            self.text = kwargs.pop('text', '')
            self.active = kwargs.pop('active', False)
            self.disabled = kwargs.pop('disabled', False)
            self.divider_before = kwargs.pop('divider_before', False)
            self.params = kwargs.pop('params', False)

    def __init__(self, *args, **kwargs):
        self.actions = []
        for action in args:
            self.actions.append(action)

    def insertAction(self, url, text, active=False, disabled=False, divider_before=False, params=''):
        self.actions.append(ActionList.Action(url=url, text=text, active=active, disabled=disabled, divider_before=divider_before, params=params))

    def __iter__(self):
        self._index = 0
        return self
    
    def __next__(self):
        if self._index >= len(self.actions):
            raise StopIteration
        self._index += 1
        return self.actions[self._index - 1]

    @staticmethod
    def GetActionsForContext(context):
        if 'actions' not in context:
            context['actions'] = ActionList()
        return context['actions']

#################
## Left Hand Details 
#
# Used by the left hand menu, within Context objects

class DetailList():
    class Detail():
        heading = ''
        value = ''

        def __init__(self, *args, **kwargs):
            if 'heading' in kwargs and 'value' in kwargs:
                self.heading = kwargs['heading']
                self.value = kwargs['value']
            else:
                self.heading = ''
                self.value = ''

    def __init__(self, *args, **kwargs):
        self.details = []
        for detail in args:
            self.details.append(detail)

    def insertDetail(self, heading, value):
        self.details.append(DetailList.Detail(heading=heading, value=value))

    def __iter__(self):
        self._index = 0
        return self
    
    def __next__(self):
        if self._index >= len(self.details):
            raise StopIteration
        self._index += 1
        return self.details[self._index - 1]

    @staticmethod
    def GetLeftDetailsForContext(context):
        if 'left_details' not in context:
            context['left_details'] = DetailList()
        return context['left_details']

