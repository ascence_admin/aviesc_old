# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE core_subscription SET created_by = 0 WHERE created_by = %s", [""])
        # Renaming column for 'Subscription.created_by' to match new field type.
        db.rename_column('core_subscription', 'created_by', 'created_by_id')
        # Changing field 'Subscription.created_by'
        db.alter_column('core_subscription', 'created_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'Subscription', fields ['created_by']
        db.create_index('core_subscription', ['created_by_id'])


        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE core_subscription SET modified_by = 0 WHERE modified_by = %s", [""])
        # Renaming column for 'Subscription.modified_by' to match new field type.
        db.rename_column('core_subscription', 'modified_by', 'modified_by_id')
        # Changing field 'Subscription.modified_by'
        db.alter_column('core_subscription', 'modified_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'Subscription', fields ['modified_by']
        db.create_index('core_subscription', ['modified_by_id'])


        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE core_userdetails SET modified_by = 0 WHERE modified_by = %s", [""])
        # Renaming column for 'UserDetails.modified_by' to match new field type.
        db.rename_column('core_userdetails', 'modified_by', 'modified_by_id')
        # Changing field 'UserDetails.modified_by'
        db.alter_column('core_userdetails', 'modified_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'UserDetails', fields ['modified_by']
        db.create_index('core_userdetails', ['modified_by_id'])


        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE core_userdetails SET created_by = 0 WHERE created_by = %s", [""])
        # Renaming column for 'UserDetails.created_by' to match new field type.
        db.rename_column('core_userdetails', 'created_by', 'created_by_id')
        # Changing field 'UserDetails.created_by'
        db.alter_column('core_userdetails', 'created_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'UserDetails', fields ['created_by']
        db.create_index('core_userdetails', ['created_by_id'])


        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE core_account SET created_by = 0 WHERE created_by = %s", [""])
        # Renaming column for 'Account.created_by' to match new field type.
        db.rename_column('core_account', 'created_by', 'created_by_id')
        # Changing field 'Account.created_by'
        db.alter_column('core_account', 'created_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'Account', fields ['created_by']
        db.create_index('core_account', ['created_by_id'])


        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        #db.execute("UPDATE core_account SET modified_by = 0 WHERE modified_by = %s", [""])
        # Renaming column for 'Account.modified_by' to match new field type.
        db.rename_column('core_account', 'modified_by', 'modified_by_id')
        # Changing field 'Account.modified_by'
        db.alter_column('core_account', 'modified_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'Account', fields ['modified_by']
        db.create_index('core_account', ['modified_by_id'])


    def backwards(self, orm):
        # Removing index on 'Account', fields ['modified_by']
        db.delete_index('core_account', ['modified_by_id'])

        # Removing index on 'Account', fields ['created_by']
        db.delete_index('core_account', ['created_by_id'])

        # Removing index on 'UserDetails', fields ['created_by']
        db.delete_index('core_userdetails', ['created_by_id'])

        # Removing index on 'UserDetails', fields ['modified_by']
        db.delete_index('core_userdetails', ['modified_by_id'])

        # Removing index on 'Subscription', fields ['modified_by']
        db.delete_index('core_subscription', ['modified_by_id'])

        # Removing index on 'Subscription', fields ['created_by']
        db.delete_index('core_subscription', ['created_by_id'])


        # Renaming column for 'Subscription.created_by' to match new field type.
        db.rename_column('core_subscription', 'created_by_id', 'created_by')
        # Changing field 'Subscription.created_by'
        db.alter_column('core_subscription', 'created_by', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Renaming column for 'Subscription.modified_by' to match new field type.
        db.rename_column('core_subscription', 'modified_by_id', 'modified_by')
        # Changing field 'Subscription.modified_by'
        db.alter_column('core_subscription', 'modified_by', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Renaming column for 'UserDetails.modified_by' to match new field type.
        db.rename_column('core_userdetails', 'modified_by_id', 'modified_by')
        # Changing field 'UserDetails.modified_by'
        db.alter_column('core_userdetails', 'modified_by', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Renaming column for 'UserDetails.created_by' to match new field type.
        db.rename_column('core_userdetails', 'created_by_id', 'created_by')
        # Changing field 'UserDetails.created_by'
        db.alter_column('core_userdetails', 'created_by', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Renaming column for 'Account.created_by' to match new field type.
        db.rename_column('core_account', 'created_by_id', 'created_by')
        # Changing field 'Account.created_by'
        db.alter_column('core_account', 'created_by', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Renaming column for 'Account.modified_by' to match new field type.
        db.rename_column('core_account', 'modified_by_id', 'modified_by')
        # Changing field 'Account.modified_by'
        db.alter_column('core_account', 'modified_by', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Group']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'db_table': "'django_content_type'", 'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType'},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_account_created_by'", 'to': "orm['auth.User']"}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_account_modified_by'", 'to': "orm['auth.User']"}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'core.subscription': {
            'Meta': {'object_name': 'Subscription'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_subscription_created_by'", 'to': "orm['auth.User']"}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_subscription_modified_by'", 'to': "orm['auth.User']"}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'core.userdetails': {
            'Meta': {'object_name': 'UserDetails'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_userdetails_created_by'", 'to': "orm['auth.User']"}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_pilot': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_userdetails_modified_by'", 'to': "orm['auth.User']"}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['auth.User']"}),
            'user_name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.userpreferences': {
            'Meta': {'object_name': 'UserPreferences'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        }
    }

    complete_apps = ['core']