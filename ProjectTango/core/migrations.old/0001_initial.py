# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Account'
        db.create_table('core_account', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('created_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('created_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('modified_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('modified_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('modified_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('account_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('start_dt', self.gf('django.db.models.fields.DateField')(blank=True, null=True)),
            ('inactive', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('core', ['Account'])

        # Adding model 'User'
        db.create_table('core_user', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('created_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('created_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('modified_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('modified_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('modified_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Account'])),
            ('user_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('is_pilot', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('core', ['User'])

        # Adding model 'Subscription'
        db.create_table('core_subscription', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('created_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('created_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('modified_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('modified_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('modified_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Account'])),
        ))
        db.send_create_signal('core', ['Subscription'])

        # Adding model 'UserPreferences'
        db.create_table('core_userpreferences', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.User'])),
        ))
        db.send_create_signal('core', ['UserPreferences'])


    def backwards(self, orm):
        # Deleting model 'Account'
        db.delete_table('core_account')

        # Deleting model 'User'
        db.delete_table('core_user')

        # Deleting model 'Subscription'
        db.delete_table('core_subscription')

        # Deleting model 'UserPreferences'
        db.delete_table('core_userpreferences')


    models = {
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        },
        'core.subscription': {
            'Meta': {'object_name': 'Subscription'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'})
        },
        'core.user': {
            'Meta': {'object_name': 'User'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_pilot': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'user_name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.userpreferences': {
            'Meta': {'object_name': 'UserPreferences'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_id': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.User']"})
        }
    }

    complete_apps = ['core']