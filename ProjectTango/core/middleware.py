from django.utils.functional import curry
from django.utils.decorators import decorator_from_middleware
from django.contrib.auth.models import User
from core.models import AuditableModel, auditable_presave, AccountModel, accountable_presave, UserDetails


class CurrentUserMiddleware(object):
    def process_request(self, request):
        if request.method in ('GET', 'HEAD', 'OPTION', 'TRACE'):
            # this request shouldn't update anything
            # so no signal handler should be attached
            return

        if hasattr(request, 'user') and request.user.is_authenticated():
            user = request.user
        else:
            #TODO review this to make sure its what we want
            user = User.objects.filter(is_superuser=True).first()

        if not user:
            return

        try:
            account = user.userdetails.account

            update_account = curry(self.update_account, account=account)
            accountable_presave.connect(update_account, dispatch_uid=request, weak=False)
        except UserDetails.DoesNotExist:
            pass

        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[-1].strip()
        else:
            ip = request.META.get('REMOTE_ADDR')
        ip = ip if ip != '' else '127.0.0.1'

        update_users = curry(self.update_users, user=user, ip=ip, request=request)
        auditable_presave.connect(update_users, dispatch_uid=request, weak=False)

    def update_users(self, sender, instance, user, ip, request, **kwargs):
        if kwargs['new']:
            instance.created_by = user.pk
            instance.created_ip = ip
            instance.request = request
        instance.modified_by = user.pk
        instance.modified_ip = ip
        instance.request = request

    def update_account(self, sender, instance, account, **kwargs):
        if kwargs['new']:
            instance.account = account

    def process_response(self, request, response):
        auditable_presave.disconnect(dispatch_uid=request)
        return response

record_current_user = decorator_from_middleware(CurrentUserMiddleware)