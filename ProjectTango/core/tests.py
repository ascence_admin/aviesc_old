from core.models import Account, UserDetails, auditable_presave, accountable_presave
from django.utils.functional import curry
from django.contrib.auth.models import User
from django.test import TestCase
from datetime import date

#########################
### Generic Test Case ###
# Used to ensure that test cases using AuditableModels function correctly
class AuditableTestCase(TestCase):
    fixtures = ['core.json', 'auth.json']

    def setUp(self):
        self.account = Account.objects.get(pk=1)
        auditable_presave.connect(self.update_users, weak=False)
        accountable_presave.connect(self.update_account, weak=False)

    def update_users(self, sender, instance, **kwargs):
        created_ip = '192.168.1.1'
        modified_ip = '192.168.1.2'
        test_user = 1
        if kwargs['new']:
            instance.created_by = test_user
            instance.created_ip = created_ip
        instance.modified_by = test_user
        instance.modified_ip = modified_ip

    def update_account(self, sender, instance, **kwargs):
        if kwargs['new']:
            instance.account = self.account

    def tearDown(self):
        auditable_presave.disconnect()

class SimpleTest(AuditableTestCase):

    def setUp(self):
        super(SimpleTest, self).setUp()
        self.new_user = {
                         'email': 'web@ascence.com',
                         'password': 'pwd',
                         }

        self.new_account = {
                            'account_name': 'Test Account',
                            'start_dt': date(2013, 8, 1)
                            }

    def test_new_user(self):
        # Create a new user, simple test case, no existing accounts, etc

        user = User.objects.create_user(username="testacc1", **self.new_user)
        user.save()

        #Verify that a UserDetails object has been created
        self.assertIsInstance(user.userdetails, UserDetails, "UserDetails object not created for new user")
        self.assertFalse(user.userdetails.is_pilot, "UserDetails is_pilot not defaulting to False")

        #Verify that a new account has been created for the user
        self.assertIsInstance(user.userdetails.account, Account, "UserDetails not created with default account")
        account = user.userdetails.account
        self.assertEqual(account.account_name, user.get_full_name(), "Default account name doesn't match user's full name")
        self.assertEqual(date(account.start_dt.year, account.start_dt.month, account.start_dt.day), date.today(), "New Account start_dt is not defaulted to today")
        self.assertFalse(account.inactive, "New Account inactive flag not defaulted to False")

        # Test adding a second user to the existing account

        account = user.userdetails.account
        account.account_name = self.new_account['account_name']
        account.start_dt = self.new_account['start_dt']
        account.save()

        user = User.objects.create_user(username="testacc2", **self.new_user)
        user.userdetails.account = account
        user.save()

        self.assertEqual(user.userdetails.account.account_name, self.new_account['account_name'], "New user existing account account_name failed")
        self.assertEqual(user.userdetails.account.start_dt, self.new_account['start_dt'], "New user existing account start_dt failed")

        #Test the AuditableModel fields are being set correctly
        self.assertEqual(account.created_ip, self.created_ip, "Audit created_ip incorrect")
        self.assertEqual(account.modified_ip, self.modified_ip, "Audit modified ip incorrect")
        self.assertEqual(date(account.created_on.year, account.created_on.month, account.created_on.day), date.today(), "Audit created_on incorrect")
        self.assertEqual(date(account.modified_on.year, account.modified_on.month, account.modified_on.day), date.today(), "Audit modified_on incorrect")
        self.assertIsInstance(account.created_by, User, "Audit created_by incorrect")
        self.assertIsInstance(account.modified_by, User, "Audit modified_by incorrect")
