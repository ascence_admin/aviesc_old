from django import forms 
from django.forms import ModelForm
from django.utils.html import format_html
from django.utils.safestring import mark_safe

class InlineSplitDateTimeWidget(forms.SplitDateTimeWidget):
    def __init__(self, *args, **kwargs):
        # Ensure that we add the widget attributes
        # to allow the date field to have the right semantics
        super(InlineSplitDateTimeWidget, self).__init__(*args, **kwargs)
        self.widgets[0].attrs['class'] = 'datepicker'
        self.widgets[1].attrs['class'] = 'timepicker'

    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs)
        id_ = final_attrs.get('id', None)
        self.widgets[0].attrs['altField'] = '#%s' % (id_ + '_1')
        html = super(InlineSplitDateTimeWidget, self).render(name, value, attrs)
        return mark_safe(html)

    def format_output(self, rendered_widgets):
        html_string = '<div class="row">{}</div>'
        wrapper = '<div class="large-6 columns">{}</div>'
        return format_html(html_string.format(''.join(wrapper.format(widget) for widget in rendered_widgets)))

    class Media:
        js = ("core/js/jquery-ui-1.10.4.custom.min.js","core/js/jquery-ui-timepicker-addon.js","core/js/datetime.init.js")


class AccountModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.account = kwargs.pop('account', None)
        super(AccountModelForm, self).__init__(*args, **kwargs)

#from django.forms import TextInput, EmailField, IntegerField, DecimalField, FloatField
#from core.models import auditable_presave, accountable_presave, AccountModel, AuditableModel

#class AuditableModelFormMixin():
#    def full_clean(self):
#        if isinstance(self.instance, AccountModel):
#            accountable_presave.send_robust(sender=self.instance, new=(self.instance.pk is None))
#        if isinstance(self.instance, AuditableModel):
#            auditable_presave.send_robust(sender=self.instance, new=(self.instance.pk is None))
#        return super(AuditableModelFormMixin, self).full_clean()
#    class Meta:
#        exclude = ('account',)
        
#class DivFormRendererMixin():

#    def as_div(self):
#        "Returns this form rendered as HTML <div>s."
#        return self._html_output(
#            normal_row = '<div%(html_class_attr)s>%(label)s %(field)s%(help_text)s</div>',
#            error_row = '%s',
#            row_ender = '</p>',
#            help_text_html = ' <span class="helptext">%s</span>',
#            errors_on_separate_row = True)

#class CssTextInput(TextInput):
#    def render(self, name, value, attrs=None):
#        if value is None:
#            value = ''
#        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
#        if value != '':
#            # Only add the 'value' attribute if a value is non-empty.
#            final_attrs['value'] = force_text(self._format_value(value))

#        return format_html('<input{0} />', flatatt(final_attrs))
