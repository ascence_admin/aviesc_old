from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required, permission_required

from . import views

urlpatterns = patterns("",
    url(r'^profile/pilot/(?P<pk>[-_\w]+)/$', views.home, name='pilot-detail'),
    url(r'^profile/inspector/(?P<pk>[-_\w]+)/$', views.home, name='inspector-detail'),
)