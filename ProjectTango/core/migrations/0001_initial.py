# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('account_name', models.CharField(max_length=255, editable=False)),
                ('start_dt', models.DateField(blank=True, null=True)),
                ('inactive', models.BooleanField(default=False)),
                ('maint_work_start_hr', models.IntegerField(default=7)),
                ('maint_work_end_hr', models.IntegerField(default=17)),
                ('maint_work_wkend', models.BooleanField(default=False)),
                ('schedule_rule_threshold', models.FloatField(default=0.9)),
                ('forecast_ahead_days', models.IntegerField(default=90)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('account', models.ForeignKey(to='core.Account')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('user_name', models.CharField(max_length=100)),
                ('is_pilot', models.BooleanField()),
                ('account', models.ForeignKey(to='core.Account')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserPreferences',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_id', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
