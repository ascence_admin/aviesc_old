from django.contrib import admin
from core.models import Account, UserDetails

admin.site.register([Account, ])
admin.site.register(UserDetails)
