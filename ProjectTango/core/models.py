import django.dispatch
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_init, post_save
from django.dispatch import receiver
from django.core.exceptions import ObjectDoesNotExist

auditable_presave = django.dispatch.Signal(providing_args=["new"])
accountable_presave = django.dispatch.Signal(providing_args=["account", "new"])

######################
## Abstract Classes ##
class BaseModel(models.Model):

    class Meta:
        abstract = True


class AuditableModel(BaseModel):
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.IntegerField(editable=False)
    created_ip = models.CharField(max_length=45, editable=False)
    modified_on = models.DateTimeField(auto_now=True, editable=False)
    modified_by = models.IntegerField(editable=False)
    modified_ip = models.CharField(max_length=45, editable=False)

    #Allow the user object to be kept here for easy use, though not using Django's foreign keys
    # since too many objects will have this foreign key, it becomes unweildly on the User object
    # (particularly for cascade deletes, when we do not want any records related to the user deleted)
    #We're defining them as properties here to allow a lazy-get approach - only retrieve the objects if they're
    # actually called
    _created_user = None
    _modified_user = None

    @property
    def created_user(self):
        if self._created_user is None:
            self._created_user = User.objects.get(pk=self.created_by)
        return self._created_user

    @property
    def modified_user(self):
        if self._modified_user is None:
            self._modified_user = User.objects.get(pk=self.modified_by)
        return self._modified_user

    def save(self, *args, **kwargs):
        #Raise the signal to populate the user ID and IP address from either the session (middleware) or the unit test
        #Note that the dates are defined to update themselves using 'auto_now'
        auditable_presave.send_robust(sender=self.__class__, instance=self, new=(self.pk is None))
        return super(AuditableModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True

class Account(AuditableModel):
    account_name = models.CharField(max_length=255, editable=False)
    start_dt = models.DateField(blank=True, null=True)
    inactive = models.BooleanField(default=False)

    # Account Settings for the Maintenance Forecaster
    maint_work_start_hr = models.IntegerField(blank=False, null=False, default=7)
    maint_work_end_hr = models.IntegerField(blank=False, null=False, default=17)
    maint_work_wkend = models.BooleanField(blank=False, null=False, default=False)
    schedule_rule_threshold = models.FloatField(blank=False, null=False, default=0.9)
    forecast_ahead_days = models.IntegerField(blank=False, null=False, default=90)

    def __str__(self):
        _status = "Not active" if self.inactive else "Active"
        _status = self.account_name + " (" + _status + ")" 
        return "%s" % _status


class AccountModel(AuditableModel):
    account = models.ForeignKey(Account)

    def save(self, *args, **kwargs):
        #accountable_presave.send_robust(sender=self, new=(self.pk is None))
        accountable_presave.send_robust(sender=self.__class__, instance=self, new=(self.pk is None))
        return super().save(*args, **kwargs)

    class Meta:
        abstract = True


######################
## Core Classes ###
    
class UserDetails(AccountModel):
    user = models.OneToOneField(User)
    user_name = models.CharField(max_length=100)
    is_pilot = models.BooleanField()
    #pilot_detail = models.OneToOneField(PilotDetail)

    def __str__(self):
        return "%s" % self.user_name


class Subscription(AccountModel):
    models.ForeignKey(Account)
    #TODO Flesh out


class UserPreferences(BaseModel):
    user_id = models.ForeignKey(User)
    #TODO Flesh out or possibly get rid of


######################
## Signal Handlers ###
@receiver(post_save, sender=User)
def user_userdetails_save(sender, instance, created, **kwargs):
    # Ensure that if a new user is created, we also create an Account and UserDetails object

    if created:
        try:
            details = instance.userdetails
        except ObjectDoesNotExist:
            details = UserDetails(user=instance,
                                  created_by=instance.pk,
                                  modified_by=instance.pk,
                                  is_pilot=False)
            instance.userdetails = details
        try:
            account = instance.userdetails.account
        except ObjectDoesNotExist:
            account =  Account(account_name=instance.get_full_name(),
                               start_dt=instance.date_joined,
                               created_by=instance.pk,
                               modified_by=instance.pk)
            account.save()
            instance.userdetails.account = account
        finally:
            instance.userdetails.save()