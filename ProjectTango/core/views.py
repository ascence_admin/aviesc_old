from django.shortcuts import get_object_or_404, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.core.urlresolvers import reverse
from schedule.models.calendars import Calendar
from aircraft.models.aircraft import Aircraft

def home(request):

    # Construct drop down menus for the top bar
    context = RequestContext(request)

    if not request.user.is_authenticated():
        # Display public home page
        pass

    else:
        user_account = request.user.userdetails.account

        #List the aircraft
        aircraft_list = {plane.pk: str(plane) for plane in Aircraft.objects.filter(account=user_account)}
        context['aircraft_list'] = aircraft_list

        #List the calendars

    return render_to_response('core/base.html',context_instance=context)