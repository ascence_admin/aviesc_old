# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('location_name', models.CharField(max_length=255)),
                ('country', models.CharField(blank=True, max_length=100)),
                ('iata_cd', models.CharField(blank=True, max_length=5)),
                ('icao_cd', models.CharField(blank=True, max_length=5)),
                ('latitude', models.DecimalField(blank=True, null=True, decimal_places=17, max_digits=20)),
                ('longitude', models.DecimalField(blank=True, null=True, decimal_places=17, max_digits=20)),
                ('is_deleted', models.BooleanField()),
                ('account', models.ForeignKey(to='core.Account')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UserFavLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('is_favourite', models.BooleanField()),
                ('account', models.ForeignKey(to='core.Account')),
                ('location', models.ForeignKey(to='location.Location')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
