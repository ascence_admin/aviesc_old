from decimal import *
from django.test import TestCase
from core.models import Account, User
from location.models import Location, UserFavLocation

class ModelTests(TestCase):
    def setUp(self):
        if Account.objects.filter(pk=1).count() == 1:
            self.acc_pk = 1
        else:
            new_acc = Account(account_name="LOCATION TEST ACCOUNT")
            new_acc.save()
            self.acc_pk = new_acc.pk
        self.loc_pk = 0

        self.location_data = {
                                     'location_name' : 'Test Location 1',
                                     'country' : 'AUS',
                                     'latitude' : Decimal('123.1555332'),
                                     'longitude' : Decimal('-35.324329109'),
                                     'iata_cd' : 'BNE',
                                     'icao_cd' : 'YBBN',
                                     }

    def test_load_global_location(self):
        """
        Tests a global location can be loaded and returned in search results
        """

        #Retrieve the current number of locations
        orig_count = Location.objects.count()

        #Create a custom location
        loc = Location(**self.location_data)
        loc.account = Account.objects.get(pk=self.acc_pk)
        loc.save()
      
        #Ensure it has saved correctly
        self.loc_pk = loc.pk
        self.assertIsNotNone(loc.pk, "Custom location not saved correctly. PK is None")
        self.assertEqual(Location.objects.filter(location_name=self.location_data['location_name']).count(), 1, "Filter on name did not return just 1 result")
        self.assertEqual(Location.objects.count(), orig_count + 1, "Total count of locations did not increase by 1")

        loc2 = Location.objects.get(pk=self.loc_pk)
        self.assertIsInstance(loc2.account, Account, "Account ID of retrieved does not equal saved value")
        self.assertEqual(loc2.location_name, self.location_data['location_name'], "Name of retrieved does not equal saved value")
        self.assertEqual(loc2.country, self.location_data['country'], "Country of retrieved does not equal saved value")
        self.assertEqual(loc2.latitude, self.location_data['latitude'], "Latitude of retrieved does not equal saved value")
        self.assertEqual(loc2.longitude, self.location_data['longitude'], "Longitude of retrieved does not equal saved value")
        self.assertEqual(loc2.iata_cd, self.location_data['iata_cd'], "IATA of retrieved does not equal saved value")
        self.assertEqual(loc2.icao_cd, self.location_data['icao_cd'], "ICAO of retrieved does not equal saved value")
        self.assertFalse(loc2.is_deleted, "Is Deleted is not false on new Location")

        #Try logicially deleting
        loc2.delete()

        #Ensure it's not physically deleted
        loc3 = Location.objects.get(pk=self.loc_pk)
        self.assertIsInstance(loc3, Location, "Retrieval of logically deleted location failed")

        #Now test the standard search results
        self.assertEqual(Location.objects.filter(location_name=self.location_data['location_name']).count(), 0, "Logically deleted Location still in standard filter")
        self.assertEqual(Location.objects.get_with_history().filter(location_name=self.location_data['location_name']).count(), 1, "Logically deleted Location not returned in history search")

    def test_user_preferences(self):

        new_user = User(user_name="Location Test User")
        new_user.account = Account.objects.get(pk=self.acc_pk)
        new_user.save()

        loc = Location(**self.location_data)
        loc.account = Account.objects.get(pk=self.acc_pk)
        loc.save()

        #Get the current list
        num_of_favs = UserFavLocation.objects.count()

        #Add
        fav = UserFavLocation()
        fav.user = new_user
        fav.account = Account.objects.get(pk=self.acc_pk)
        fav.is_favourite = True
        fav.location = loc
        fav.save()
        
        fav_pk = fav.pk
        self.assertIsNotNone(fav.pk, "User Location Favourite not saved")
        self.assertEqual(UserFavLocation.objects.count(), num_of_favs + 1, "User favourite locations count did not increase")

        #Verify that the new favourite appears at the top of this list
        #self.assertEqual(self.acc_pk, 1, "Acc ID not 1")
        self.assertEqual(new_user.pk, 1, "User id not 1")
        first_fav = Location.objects.get_user_favourites()[0]
        self.assertEqual(first_fav.location_name, self.location_data['location_name'], "User favourite not first in list")