# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Location'
        db.create_table('location_location', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('created_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('created_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('modified_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('modified_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('modified_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Account'])),
            ('location_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('country', self.gf('django.db.models.fields.CharField')(blank=True, max_length=100)),
            ('iata_cd', self.gf('django.db.models.fields.CharField')(blank=True, max_length=5)),
            ('icao_cd', self.gf('django.db.models.fields.CharField')(blank=True, max_length=5)),
            ('latitude', self.gf('django.db.models.fields.DecimalField')(blank=True, max_digits=20, decimal_places=17, null=True)),
            ('longitude', self.gf('django.db.models.fields.DecimalField')(blank=True, max_digits=20, decimal_places=17, null=True)),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('location', ['Location'])

        # Adding model 'UserFavLocation'
        db.create_table('location_userfavlocation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('created_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('created_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('modified_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('modified_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('modified_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Account'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['location.Location'])),
            ('is_favourite', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('location', ['UserFavLocation'])


    def backwards(self, orm):
        # Deleting model 'Location'
        db.delete_table('location_location')

        # Deleting model 'UserFavLocation'
        db.delete_table('location_userfavlocation')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'blank': 'True', 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'blank': 'True', 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'blank': 'True', 'symmetrical': 'False'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'db_table': "'django_content_type'", 'object_name': 'ContentType'},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        },
        'location.location': {
            'Meta': {'object_name': 'Location'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'country': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'iata_cd': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '5'}),
            'icao_cd': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '5'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'max_digits': '20', 'decimal_places': '17', 'null': 'True'}),
            'location_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'max_digits': '20', 'decimal_places': '17', 'null': 'True'}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'})
        },
        'location.userfavlocation': {
            'Meta': {'object_name': 'UserFavLocation'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_favourite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Location']"}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        }
    }

    complete_apps = ['location']