# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE location_location SET modified_by = 0 WHERE modified_by = %s", [""])
        # Renaming column for 'Location.modified_by' to match new field type.
        db.rename_column('location_location', 'modified_by', 'modified_by_id')
        # Changing field 'Location.modified_by'
        db.alter_column('location_location', 'modified_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'Location', fields ['modified_by']
        db.create_index('location_location', ['modified_by_id'])


        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE location_location SET created_by = 0 WHERE created_by = %s", [""])
        # Renaming column for 'Location.created_by' to match new field type.
        db.rename_column('location_location', 'created_by', 'created_by_id')
        # Changing field 'Location.created_by'
        db.alter_column('location_location', 'created_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'Location', fields ['created_by']
        db.create_index('location_location', ['created_by_id'])


        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE location_userfavlocation SET modified_by = 0 WHERE modified_by = %s", [""])
        # Renaming column for 'UserFavLocation.modified_by' to match new field type.
        db.rename_column('location_userfavlocation', 'modified_by', 'modified_by_id')
        # Changing field 'UserFavLocation.modified_by'
        db.alter_column('location_userfavlocation', 'modified_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'UserFavLocation', fields ['modified_by']
        db.create_index('location_userfavlocation', ['modified_by_id'])


        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE location_userfavlocation SET created_by = 0 WHERE created_by = %s", [""])
        # Renaming column for 'UserFavLocation.created_by' to match new field type.
        db.rename_column('location_userfavlocation', 'created_by', 'created_by_id')
        # Changing field 'UserFavLocation.created_by'
        db.alter_column('location_userfavlocation', 'created_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'UserFavLocation', fields ['created_by']
        db.create_index('location_userfavlocation', ['created_by_id'])


    def backwards(self, orm):
        # Removing index on 'UserFavLocation', fields ['created_by']
        db.delete_index('location_userfavlocation', ['created_by_id'])

        # Removing index on 'UserFavLocation', fields ['modified_by']
        db.delete_index('location_userfavlocation', ['modified_by_id'])

        # Removing index on 'Location', fields ['created_by']
        db.delete_index('location_location', ['created_by_id'])

        # Removing index on 'Location', fields ['modified_by']
        db.delete_index('location_location', ['modified_by_id'])


        # Renaming column for 'Location.modified_by' to match new field type.
        db.rename_column('location_location', 'modified_by_id', 'modified_by')
        # Changing field 'Location.modified_by'
        db.alter_column('location_location', 'modified_by', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Renaming column for 'Location.created_by' to match new field type.
        db.rename_column('location_location', 'created_by_id', 'created_by')
        # Changing field 'Location.created_by'
        db.alter_column('location_location', 'created_by', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Renaming column for 'UserFavLocation.modified_by' to match new field type.
        db.rename_column('location_userfavlocation', 'modified_by_id', 'modified_by')
        # Changing field 'UserFavLocation.modified_by'
        db.alter_column('location_userfavlocation', 'modified_by', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Renaming column for 'UserFavLocation.created_by' to match new field type.
        db.rename_column('location_userfavlocation', 'created_by_id', 'created_by')
        # Changing field 'UserFavLocation.created_by'
        db.alter_column('location_userfavlocation', 'created_by', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Group']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_account_created_by'", 'to': "orm['auth.User']"}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_account_modified_by'", 'to': "orm['auth.User']"}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        },
        'location.location': {
            'Meta': {'object_name': 'Location'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'country': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_location_created_by'", 'to': "orm['auth.User']"}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'iata_cd': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '5'}),
            'icao_cd': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '5'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '17', 'blank': 'True', 'null': 'True'}),
            'location_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '20', 'decimal_places': '17', 'blank': 'True', 'null': 'True'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_location_modified_by'", 'to': "orm['auth.User']"}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'})
        },
        'location.userfavlocation': {
            'Meta': {'object_name': 'UserFavLocation'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_userfavlocation_created_by'", 'to': "orm['auth.User']"}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_favourite': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['location.Location']"}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_userfavlocation_modified_by'", 'to': "orm['auth.User']"}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        }
    }

    complete_apps = ['location']