from core.query import LogicalDeleteQuerySet
from core.models import AccountModel, User
from django.db import models, DEFAULT_DB_ALIAS
from django.db.models import Q

GLOBAL_ACCOUNT_ID = 0 

### Custom Location Manager ###
class LocationManager(models.Manager):
    def get_query_set(self):
        if self.model:
            return LogicalDeleteQuerySet(self.model, using=self._db).filter(
                is_deleted=False
            )
        #return super().get_query_set().filter(is_deleted=False)
        # Allow a query to return logically deleted rows from the RefLocation table
    def get_with_history(self):
        return super(LocationManager, self).get_query_set()
        # Provide a list including custom locations on the account
    def get(self, *args, **kwargs):
        return self.get_with_history().get(*args, **kwargs)
    def filter(self, *args, **kwargs):
        if "pk" in kwargs:
            return self.get_with_history().filter(*args, **kwargs)
        return self.get_query_set().filter(*args, **kwargs)
    def get_full_acount_list(self, acc_id):
        #TODO Get current account id
        return_set = super(LocationManager, self).get_query_set().filter(~Q(is_deleted=True), Q(account_id=GLOBAL_ACCOUNT_ID)|Q(account_id=acc_id))
        return super(LocationManager, self).get_query_set().filter(is_deleted=False)
        # Provide a list including custom locations, ordered with user favourites first
    def get_user_favourites(self, acc_id, user_id):
        parameters = [1, 1]
        return super().raw("""
            SELECT r.*, f.is_favourite
            FROM location_location r
            LEFT OUTER JOIN location_userfavlocation f
            ON (r.id = f.location_id AND f.user_id = %s)
            WHERE (r.account_id = 0 or r.account_id = %s)
            AND r.is_deleted = FALSE
            ORDER BY f.is_favourite DESC, r.account_id DESC, r.location_name DESC""", params=[acc_id, user_id]) #TODO Get account id and user id

class Location(AccountModel):
    location_name = models.CharField(max_length=255)
    country = models.CharField(max_length=100, blank=True)
    iata_cd = models.CharField(max_length=5, blank=True)
    icao_cd = models.CharField(max_length=5, blank=True)
    latitude = models.DecimalField(max_digits=20, decimal_places=17, blank=True, null=True)
    longitude = models.DecimalField(max_digits=20, decimal_places=17, blank=True, null=True)
    is_deleted = models.BooleanField()

    # Override the standard Delete method to only logically delete, but provide an alternative method to also physically delete
    # Note the custom manager prevents logically deleted items being return in the default result set, but not from being read by ID
    def delete(self, using=DEFAULT_DB_ALIAS):
        if self.pk is not None:
            self.is_deleted = True
            return self.save()
        else:
            return super(Location, self).delete(using)

    def delete_physical(self, using=DEFAULT_DB_ALIAS):
        return super(Location, self).delete(using)

    # Use the custom manager
    objects = LocationManager()

class UserFavLocation(AccountModel):
    user = models.ForeignKey(User)
    location = models.ForeignKey(Location)
    is_favourite = models.BooleanField()
