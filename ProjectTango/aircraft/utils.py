import datetime, math, itertools, pytz
from dateutil.relativedelta import relativedelta
from django.utils import timesince, timezone
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum, Max
from django.db.models.signals import post_save
from django.dispatch import receiver

from aircraft.models.aircraft import Aircraft, AircraftType
from aircraft.models.maintenancerule import AircraftMaintenanceRule
from aircraft.models.log import LogEntry
from aircraft.models.event import AircraftCalendarEventData
from schedule.models.calendars import Calendar, CalendarManager
from schedule.models.events import Event, EventManager
from schedule.utils import EventListManager
from schedule.periods import Period, Day

class MaintenanceForecaster():
    aircraft = None
    rule_set = None
    calendar = None

    # These are defaulted here but adjusted to the Account's preferences in _get_objects()
    AUTO_SCHED_THRESHOLD = 0.9
    PROJECT_AHEAD_DAYS = 90
    WORK_DAY_START_HR = 7
    WORK_DAY_END_HR = 18
    WORK_WKENDS = False

    # Ensure that the dates forecast are weekdays only if the account does not do maintenance on weekends
    def _return_next_working_date(self, from_date, backwards = False):
        if not self.WORK_WKENDS:
            if (from_date.weekday() % 7) < 5:
                return from_date
            else:
                if backwards:
                    delta = datetime.timedelta(days=((from_date.weekday() % 5) +1)*-1)
                else:
                    delta = datetime.timedelta(days=(7 - from_date.weekday()))
                return from_date + delta
        else:
            return from_date

    def _check_threshold(self, current, threshold):
        if threshold > 0 and current >= (threshold * self.AUTO_SCHED_THRESHOLD):
            return True
        return False

    def find_calendar_day_free(self, start, rule):
        if rule.scheduled_event:
            #Make sure we're not evaluating the same Event we're trying to reschedule!
            event_set = AircraftCalendarEventData.objects.get_events_for_aircraft(rule.aircraft, future_only=False).exclude(pk=rule.scheduled_event.pk)
        else:
            event_set = AircraftCalendarEventData.objects.get_events_for_aircraft(rule.aircraft, future_only=False)
        working = self._return_next_working_date(start)
        period = Day(event_set, working)
        if not period.has_occurrences():
            # The day we wanted is free
            return working
        else:
            next_day = Day(event_set, self._return_next_working_date(working + datetime.timedelta(days=1)))
            # Check the following day only, otherwise we'll need to go earlier
            if not next_day.has_occurrences():
                return next_day.start
            else:
                # Wind back in time to find a day
                prev_day = Day(event_set, self._return_next_working_date(working - datetime.timedelta(days=1)))
                while prev_day.start >= (start - datetime.timedelta(weeks=1)):
                    if not prev_day.has_occurrences():
                        return prev_day.start
                    prev_day = Day(event_set, self._return_next_working_date(working - datetime.timedelta(days=1)))

                # No spare days, so just schedule and raise an alert
                #TODO Raise alert
                return working

    def find_calendar_free(self, start, rule):
        # Check to see if there is enough time in the day
        # to schedule the maintenance event
        if rule.scheduled_event:
            #Make sure we're not evaluating the same Event we're trying to reschedule!
            event_set = AircraftCalendarEventData.objects.get_events_for_aircraft(rule.aircraft, future_only=False).exclude(pk=rule.scheduled_event.pk)
        else:
            event_set = AircraftCalendarEventData.objects.get_events_for_aircraft(rule.aircraft, future_only=False)
        working = self._return_next_working_date(start)
        period = Period(event_set, working - datetime.timedelta(weeks=1), working)
        occurrences = period.get_occurrences()
        if len(occurrences) == 0:
            return working
        #Reverse the list so we work backwards through time
        occurrences = sorted(occurrences, reverse=True)
        prev_occurrence_start = None
        # Using a 1 week window, get the current list of occurrences from the calendar
        # prior to when we need to start
        current_tz = timezone.get_current_timezone()
        for occurrence in occurrences:
            time_to_finish = occurrence.end + datetime.timedelta(seconds=rule.downtime_duration_secs)
            print('  Occur %s - %s\n\t TTF = %s' % (occurrence.start, occurrence.end, time_to_finish))
            print('\tWork Hours Check: %s\tGap Check: %s' % (current_tz.normalize(time_to_finish.astimezone(current_tz)).hour < self.WORK_DAY_END_HR,(prev_occurrence_start is None or prev_occurrence_start >= time_to_finish)))
            if current_tz.normalize(time_to_finish.astimezone(current_tz)).hour < self.WORK_DAY_END_HR and (prev_occurrence_start is None or prev_occurrence_start >= time_to_finish):
                # There is enough time in between to schedule, so go ahead
                return occurrence.end
            # Not enough time, so keep skipping back
            prev_occurrence_start = occurrence.start
        # Now, test if we got all the way back through 1 weeks with no openings, or is there something before the earliest event?
        if prev_occurrence_start <= working - datetime.timedelta(weeks=1):
            # There are no openings, so schedule at the due time and
            # TODO raise an alert
            return working
        else:
            local_prev_occurrence_start = current_tz.normalize(prev_occurrence_start.astimezone(current_tz))
            if (local_prev_occurrence_start - datetime.timedelta(seconds=rule.downtime_duration_secs)).hour < self.WORK_DAY_START_HR:
                return datetime.datetime(local_prev_occurrence_start.year, local_prev_occurrence_start.month, local_prev_occurrence_start.day-1, self.WORK_DAY_END_HR, 0, 0) - datetime.timedelta(seconds=rule.downtime_duration_secs)
            else:
                return local_prev_occurrence_start - datetime.timedelta(seconds=rule.downtime_duration_secs)

    def _calculate_future_events(self, rule, hist_hrs, hist_cycle):
        futures = AircraftCalendarEventData.objects.get_events_for_aircraft(rule.aircraft)
        cycles = hist_cycle
        flight_time_secs = hist_hrs
        # TODO Investigate if this is a better way - events = set(future_event for future_event in futures)
        events = list(futures)
        elm = EventListManager(events)
        for occurrence in elm.occurrences_after():
            if not occurrence.event.aircraft_data.is_maintenance:
                if (occurrence.start - timezone.now()).days > self.PROJECT_AHEAD_DAYS:
                    # Then we've gone past our future threshold, so stop evaluating
                    return None
                else:
                    flight_time_secs += occurrence.event.aircraft_data.flight_hours.seconds
                    cycles += occurrence.event.aircraft_data.cycles
                    if self._check_threshold(flight_time_secs, rule.flight_secs_incr) or self._check_threshold(cycles, rule.cycles_incr):
                        return occurrence.end
        return None

    def _get_objects(self, craft = None, rules = None, single_rule = None, cal = None):
        self.aircraft = craft
        self.rule_set = rules
        self.calendar = cal

        if self.aircraft is None and single_rule is not None:
            self.aircraft = single_rule.aircraft

        if self.rule_set is None and self.aircraft is not None:
            self.rule_set = self.aircraft.aircraftmaintenancerule_set

        if self.calendar is None and self.aircraft is not None:
            self.calendar = Calendar.objects.get_calendar_for_object(self.aircraft)
        
        # We don't enough enough to continue without these
        if self.aircraft is None and self.calendar is None:
            raise ObjectDoesNotExist

        # Grab the account's settings to use
        account = self.aircraft.account
        self.WORK_DAY_START_HR = account.maint_work_start_hr 
        self.WORK_DAY_END_HR = account.maint_work_end_hr 
        self.WORK_WKENDS = account.maint_work_wkend 
        self.AUTO_SCHED_THRESHOLD = account.schedule_rule_threshold 
        self.PROJECT_AHEAD_DAYS = account.forecast_ahead_days 


    def next_date_for_calendar_rule(self, start, increment, period):
        actual_start = start
        now = datetime.datetime.now()
        if isinstance(start, datetime.date):
            start = datetime.datetime.combine(start, datetime.time())
        if start < now:
            diff = now - start
            if period == AircraftMaintenanceRule.CAL_TYPE_DAY:
                skips = math.ceil(diff.days / increment) 
                actual_start = start + datetime.timedelta(days=skips*increment)
            elif period == AircraftMaintenanceRule.CAL_TYPE_WK:
                skips = math.ceil(diff.days / 7 / increment)
                actual_start = start + datetime.timedelta(weeks=skips*increment)
            elif period == AircraftMaintenanceRule.CAL_TYPE_MONTH:
                skips = math.floor((now.month - start.month) / increment)
                actual_start = start + relativedelta(months=skips*increment)
                if datetime.date(actual_start) < datetime.date(now):
                    #It would be this month but we've passed that day, so let it be the next time
                    actual_start = actual_start + relativedelta(months=increment)
            elif period == AircraftMaintenanceRule.CAL_TYPE_YR:
                skips = math.floor((now.year - start.year) / increment)
                actual_start = start + relativedelta(years=skips*increment)
                if datetime.date(actual_start) < datetime.date(now): actual_start = actual_start + relativedelta(years=increment)
        return actual_start

    #Forecast the events for a particular maintenance rule
    def forecastForRule(self, rule, save_rule=False):
        self._get_objects(single_rule=rule)
        # Does the rule already have an Event?
        if rule.scheduled_event:
            if not rule.auto_schedule:
                #Then we should no longer keep any future unconfirmed occurrences, since this is turned off
                for occurrence in rule.scheduled_event.occurrences_after():
                    if not occurrence.confirmed and not occurrence.cancelled: occurrence.cancel
        if rule.auto_schedule and (not rule.scheduled_event or not rule.scheduled_event.aircraft_data.confirmed):
            #Then we need to update any future, unconfirmed occurrences to match the rule
            projected_date = None

            #First, aggregate where the aircraft's current stastics are so that we can project
            log_set, last_date = LogEntry.objects.get_entries_since_maintenance(rule)
            metrics = log_set.all().aggregate(Max('tach'), Max('hobbs'), Sum('cycles'), Sum('flight_time'))
            
            if rule.calendar_incr > 0 and (rule.flight_hrs_incr == 0 and rule.cycles_incr == 0):
                #Calendar rule only. This is the easiest - just create an Event at the appropriate time
                #Calculate the proper start date, so that we don't create a pile of historical maintenance occurrences
                projected_date = self.next_date_for_calendar_rule(last_date, rule.calendar_incr, rule.calendar_incr_type)
            elif not (metrics['flight_time__sum'] is None and metrics['cycles__sum'] is None): 
                # Ensure that there are logs for the aircraft, otherwise we've nothing to forecast on
                if rule.flight_secs_incr > 0 and (rule.calendar_incr == 0 and rule.cycles_incr == 0):
                    # Flight hours only rule. Test to see how close we are
                    if metrics['flight_time__sum'] >= (rule.flight_secs_incr * self.AUTO_SCHED_THRESHOLD):
                        # It is close enough to schedule
                        projected_date = timezone.now()
                    else:
                        # See if we can project out based on futures
                        projected_date = self._calculate_future_events(rule, metrics['flight_time__sum'], metrics['cycles__sum'])
                elif rule.cycles_incr > 0 and (rule.calendar_incr == 0 and rule.flight_hrs_incr == 0):
                    # Flight hours only rule. Test to see how close we are
                    if metrics['cycles__sum'] >= (rule.cycles_incr * self.AUTO_SCHED_THRESHOLD):
                        # It is close enough to schedule
                        projected_date = timezone.now()
                    else:
                        # See if we can project out based on futures
                        projected_date = self._calculate_future_events(rule, metrics['flight_time__sum'], metrics['cycles__sum'])
            if projected_date:
                # Ensure its timezone aware
                if timezone.is_naive(projected_date):
                    projected_date = projected_date.replace(tzinfo=pytz.utc)
                # Then we've established a date that might hit the target
                if not rule.scheduled_event:
                    inspection = Event()
                    inspection.calendar = self.calendar
                else:
                    inspection = rule.scheduled_event
                # Start the Event, initially, on the next hour
                if rule.downtime_type == rule.CAL_TYPE_HR:
                    delta = relativedelta(minutes=-1*projected_date.minute, hours=1, seconds=-1*projected_date.second, microseconds=-1*projected_date.microsecond)
                    inspection.start = self.find_calendar_free(projected_date + delta, rule)
                elif rule.downtime_type == rule.CAL_TYPE_DAY:
                    delta = relativedelta(minutes=-1*projected_date.minute, hours=-1*projected_date.hour, seconds=-1*projected_date.second, microseconds=-1*projected_date.microsecond)
                    inspection.start = self.find_calendar_day_free(projected_date + delta, rule)
                else: #CAL_TYPE_WK
                    # We do not attempt to find open weeks - just schedule and alert
                    delta = relativedelta(minutes=-1*projected_date.minute, hours=-1*projected_date.hour, seconds=-1*projected_date.second, microseconds=-1*projected_date.microsecond)
                    inspection.start = projected_date + delta
                dur = datetime.timedelta(seconds=rule.downtime_duration_secs)
                inspection.end = inspection.start + dur
                inspection.title = rule.name
                inspection.save()
                inspection.aircraft_data.confirmed = False
                inspection.aircraft_data.is_maintenance = True
                inspection.aircraft_data.save()
                rule.scheduled_event = inspection
                if save_rule:
                    rule.save()
        # remove a scheduled event if no longer auto scheduling
        else:
            # remove a scheduled event if no longer auto scheduling, only if its still unconfirmed
            if not rule.auto_schedule and rule.scheduled_event and not rule.scheduled_event.aircraft_data.confirmed:
                rule.scheduled_event.delete()

    def forecastSchedule(self, craft = None, rules = None, cal = None):
        # Ensure we have enough information to do our job
        self._get_objects(craft=craft, rules=rules, cal=cal)
