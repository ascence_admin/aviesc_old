from django.contrib import admin
from aircraft.models.maintenancerule import AircraftMaintenanceRule
from aircraft.models.aircraft import Aircraft
from aircraft.models.log import LogEntry

admin.site.register(AircraftMaintenanceRule)
admin.site.register(Aircraft)
admin.site.register(LogEntry)