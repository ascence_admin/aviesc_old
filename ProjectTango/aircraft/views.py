from django.core.urlresolvers import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.contrib import messages
from django.utils import formats
from django.contrib.messages.views import SuccessMessageMixin

from aircraft.models.aircraft import Aircraft
from aircraft.models.log import LogEntry
from aircraft.forms import AircraftForm, LogForm
from core.constructs import ActionList, DetailList

CREATE_LOG_AIRCRAFT_REQUEST_KEY = 'Create-Log-Aircraft'
CREATE_LOG_PILOT_REQUEST_KEY = 'Create-Log-Pilot'
CREATE_LOG_RULE_REQUEST_KEY = 'Create-Log-Rule'
CREATE_LOG_INSPECTOR_REQUEST_KEY = 'Create-Log-Inspector'

class AircraftList(ListView):
    model = Aircraft
    template_name = 'aircraft/aircraft_list.html'

    def get_queryset(self):
        user_account = self.request.user.userdetails.account
        return Aircraft.objects.filter(account=user_account)

    def get_context_data(self, **kwargs):
        #Add actions to the left hand actions
        context = super().get_context_data(**kwargs)
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('aircraft-list', kwargs={}), 'List Aircraft', True)
        actions.insertAction(reverse('aircraft-create', kwargs={}), 'Add new')
        return context

class AircraftDetail(DetailView):
    model = Aircraft

    def get_queryset(self):
        user_account = self.request.user.userdetails.account
        return Aircraft.objects.filter(account=user_account)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        aircraft = context['object']
        #Grab recent log entries
        logs = aircraft.logentry_set.order_by('-entry_time')[:3]
        context['logs'] = logs
        #Grab the maintenance rule summary info
        rules = aircraft.aircraftmaintenancerule_set.all()
        context['rules'] = rules
        #Add an "Update this" link to the left hand actions
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('aircraft-list', kwargs={}), 'List Aircraft')
        actions.insertAction(reverse('aircraft-update', kwargs={'pk': self.object.pk}), 'Update', True)
        #Display the audit fields on the left as well
        details = DetailList.GetLeftDetailsForContext(context)
        details.insertDetail("Updated by", ''.join([self.object.modified_user.get_username(), ' on ', formats.date_format(self.object.modified_on, "SHORT_DATE_FORMAT")]))
        details.insertDetail("Created by", ''.join([self.object.created_user.get_username(), ' on ', formats.date_format(self.object.created_on, "SHORT_DATE_FORMAT")]))

        #Ensure that we prepopulate the CREATE_LOG_AIRCRAFT_REQUEST_KEY in case we navigate to Log Entry from here
        self.request.session[CREATE_LOG_AIRCRAFT_REQUEST_KEY] = aircraft
        return context

class AircraftAdd(CreateView):
    model = Aircraft
    template_name = 'aircraft/aircraft_create.html'
    form_class = AircraftForm
    success_message = "Aircraft was added successfully"

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        return super(AircraftAdd, self).form_valid(form)

    def get_context_data(self, **kwargs):
        #Add actions to the left hand actions
        context = super().get_context_data(**kwargs)
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('aircraft-list', kwargs={}), 'List Aircraft')
        return context

class AircraftUpdate(SuccessMessageMixin, UpdateView):
    model = Aircraft
    template_name = 'aircraft/aircraft_update.html'
    form_class = AircraftForm
    success_message = "Aircraft was updated successfully"

    def get_queryset(self):
        user_account = self.request.user.userdetails.account
        return Aircraft.objects.filter(account=user_account)

    def form_valid(self, form):
        response = super(SuccessMessageMixin, self).form_valid(form)
        messages.success(self.request, self.success_message)
        return response

    def get_context_data(self, **kwargs):
        #Add actions to the left hand actions
        context = super(AircraftUpdate, self).get_context_data(**kwargs)
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('aircraft-list', kwargs={}), 'List Aircraft')
        actions.insertAction(reverse('aircraft-detail', kwargs={'pk': context['object'].pk}), 'Back', True)
        return context

class LogDetail(DetailView):
    model = LogEntry

    def get_queryset(self):
        user_account = self.request.user.userdetails.account
        return LogEntry.objects.select_related().filter(account=user_account)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        log = context['object']

        #Add links to the left hand actions
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('log-update', kwargs={'pk':self.object.pk}), 'Update')
        divider_inserted = False
        if log.aircraft:
            divider_inserted = not divider_inserted
            actions.insertAction(reverse('aircraft-detail', kwargs={'pk':log.aircraft_id}), 'View this Aircraft', False, True)
        if log.pilot_id:
            actions.insertAction(reverse('pilot-detail', kwargs={'pk':log.pilot_id}), 'View this Pilot', False, True if not divider_inserted else False)
            divider_inserted = True
        if log.maintenance_rule_id:
            actions.insertAction(reverse('rule-detail', kwargs={'pk':log.maintenance_rule_id}), 'View this Rule', False, True if not divider_inserted else False)
            divider_inserted = True
        if log.maintenance_by_id:
            actions.insertAction(reverse('inspector-detail', kwargs={'pk':log.maintenance_by_id}), 'View this Engineer', False, True if not divider_inserted else False)
        #Display the audit fields on the left as well
        details = DetailList.GetLeftDetailsForContext(context)
        details.insertDetail("Updated by", ''.join([self.object.modified_user.get_username(), ' on ', formats.date_format(self.object.modified_on, "SHORT_DATE_FORMAT")]))
        details.insertDetail("Created by", ''.join([self.object.created_user.get_username(), ' on ', formats.date_format(self.object.created_on, "SHORT_DATE_FORMAT")]))
        return context

class LogCreate(CreateView):
    model = LogEntry
    template_name = 'aircraft/logentry_create.html'
    form_class = LogForm
    success_message = "Log entry was saved successfully"

    def get_form_kwargs(self):
        kwargs = super(LogCreate, self).get_form_kwargs()
        kwargs.update({'account': self.request.user.userdetails.account})
        kwargs.update({'user':self.request.user})
        # Depending on where we're coming from, some items may be pre-populated for us
        kwargs.update({'aircraft': self.request.session.pop(CREATE_LOG_AIRCRAFT_REQUEST_KEY, None)})
        kwargs.update({'pilot': self.request.session.pop(CREATE_LOG_PILOT_REQUEST_KEY, None)})
        kwargs.update({'rule': self.request.session.pop(CREATE_LOG_RULE_REQUEST_KEY, None)})
        kwargs.update({'inspector': self.request.session.pop(CREATE_LOG_INSPECTOR_REQUEST_KEY, None)})
        self.pre_pop_aircraft = True if kwargs['aircraft'] is not None else False
        self.pre_pop_rule = True if kwargs['rule'] is not None else False
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Tell the template if we are pre-populating
        context['pre_pop_aircraft'] = self.pre_pop_aircraft
        context['pre_pop_rule'] = self.pre_pop_rule
        #Add actions to the left hand actions
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('aircraft-list', kwargs={}), 'List Aircraft')
        return context
