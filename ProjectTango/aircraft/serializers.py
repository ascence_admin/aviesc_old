from rest_framework import serializers

from aircraft.models.aircraft import Aircraft


class AircraftSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Aircraft
        fields = ('id', 'aircraft_mark', 'aircraft_type_name', 'hourly_rate', 'takeoff_cost', 'is_deleted', )
