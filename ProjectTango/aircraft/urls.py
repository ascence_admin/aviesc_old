from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import login_required, permission_required

from rest_framework import routers

from aircraft import api
from . import views

router = routers.DefaultRouter()
router.register(r'aircrafts', api.AircraftViewSet)

urlpatterns = patterns("",
                       url(r'^api/', include(router.urls)),

                       url(r"^$", login_required(views.AircraftList.as_view()), name="aircraft-list"),
                       url(r"^add/$", permission_required('aircraft.add_aircraft')(views.AircraftAdd.as_view()),
                           name="aircraft-create"),
                       url(r'^(?P<pk>[-_\w]+)/$', login_required(views.AircraftDetail.as_view()),
                           name='aircraft-detail'),
                       url(r"^(?P<pk>[-_\w]+)/update/$",
                           permission_required('aircraft.change_aircraft')(views.AircraftUpdate.as_view()),
                           name="aircraft-update"),
)