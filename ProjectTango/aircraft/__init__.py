from django.dispatch import receiver
from django.db.models.signals import pre_save
from aircraft.models.maintenancerule import AircraftMaintenanceRule
from aircraft.utils import MaintenanceForecaster
                
##Listen for any rule updates
@receiver(pre_save, sender=AircraftMaintenanceRule)
def rules_pre_save(sender, instance, **kwargs):
    forecast = MaintenanceForecaster()
    forecast.forecastForRule(instance)
