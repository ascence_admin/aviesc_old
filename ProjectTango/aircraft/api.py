from rest_framework import viewsets

from aircraft.models.aircraft import Aircraft
from aircraft.serializers import AircraftSerializer


class AircraftViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows aircraft to be viewed or edited.
    """
    queryset = Aircraft.objects.all()
    serializer_class = AircraftSerializer
