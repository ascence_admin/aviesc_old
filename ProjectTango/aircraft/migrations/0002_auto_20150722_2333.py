# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aircraft', '0001_initial'),
        ('schedule', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='aircraftmaintenancerule',
            name='scheduled_event',
            field=models.ForeignKey(to='schedule.Event', blank=True, null=True, editable=False),
        ),
        migrations.AddField(
            model_name='aircraftcalendareventdata',
            name='event',
            field=models.OneToOneField(related_name='aircraft_data', to='schedule.Event'),
        ),
        migrations.AddField(
            model_name='logentry',
            name='aircraft',
            field=models.ForeignKey(to='aircraft.Aircraft', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='aircraftmaintenancerule',
            name='aircraft',
            field=models.ForeignKey(to='aircraft.Aircraft', editable=False),
        ),
        migrations.AddField(
            model_name='aircraft',
            name='casa_record',
            field=models.ForeignKey(to='aircraft.CasaAircraft', blank=True, null=True),
        ),
    ]
