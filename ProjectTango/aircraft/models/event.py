import datetime
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from schedule.models.events import Event, Occurrence, EventRelationManager
from django.dispatch import receiver
from django.db import models
from django.db.models import Q, Manager
from django.db.models.signals import post_save, pre_save
from core.models import BaseModel
###############################
## Extended Events for Aircraft
#
# We do this to track extra information for Events to allow
# for projecting maintenance and tracking extra information for an 
# aircraft's booking than what the standard Event caters for


##TODO
# add a post init and save methods that
#   for new, creates the assoc record
#   for all, updates hrs/cycles based on duration if the values have not been specified
class AircraftCalendarEventManager(Manager):

    def get_events_for_aircraft(self, aircraft, future_only = True):
        event_set = EventRelationManager().get_events_for_object(aircraft).select_related('aircraft_data') \
            .extra(select={'flight_hrs':'(CASE WHEN approx_flight_hrs > 0 THEN approx_flight_hrs ELSE event_duration END)', \
                    'cycles':'(CASE WHEN approx_cycles > 0 THEN approx_cycles ELSE event_cycles END)', })
        if future_only:
            event_set = event_set.filter(end__gt = timezone.now().date())
        return event_set

class AircraftCalendarEventData(BaseModel):

    ## We will assume that if not specified, an flight booking (calendar Event) will only have one cycle if the event duration
    #   is less than this number of hours (calculated as seconds to work with timedelta objects), otherwise we'll assume that it's 2 cycles
    #  This is used for maintenance forecasting.
    SINGLE_CYCLE_ASSUMPTION_THRESHOLD = 3 * 3600

    event = models.OneToOneField(Event, related_name="aircraft_data")
    objects = AircraftCalendarEventManager()

    # Maintenance Events - to allow tracking of downtime
    is_maintenance = models.BooleanField(blank=False, default=False)

    # If this is a maintenance Event, then it will be created non-confirmed
    # and the user is required to confirm this appointment
    confirmed = models.BooleanField(blank=False, editable=False, default=True)

    # If this is a flight, then hopefully it's populated with useful data
    # to allow accurate maintenance projections
    approx_flight_hrs = models.FloatField(blank=True, null=False, default=0.0) # as seconds
    approx_cycles = models.IntegerField(blank=True, null=False, default=0)
    event_duration = models.FloatField(blank=True, null=False, default=0.0) # as seconds
    event_cycles = models.IntegerField(blank=True, null=False, default=0)

    #Durations (datetime.timedelta) are stored as seconds
    def _deltaToFloat(self, delta):
        return delta.days*86400 + delta.seconds

    def _floatToDelta(self, float):
        return datetime.timedelta( float / 86400 )
                 
    @property
    def flight_hours(self):
        return self._floatToDelta(self.approx_flight_hrs) if self.approx_flight_hrs > 0 else self._floatToDelta(self.event_duration)
    
    @flight_hours.setter
    def flight_hours(self, value):
        self.approx_flight_hrs = self._deltaToFloat(value)

    @property
    def cycles(self):
        return self.approx_cycles if self.approx_cycles > 0 else self.event_cycles

    class Meta:
        app_label = 'aircraft'

#class AircraftCalendarOccurrenceData(model.Model):
#    occurrence = models.OneToOneField(Occurrence, related_name="aircraft_data")

#    # If this is a maintenance Event, then it will be created non-confirmed
#    # and the user is required to confirm this appointment
#    confirmed = models.BooleanField(blank=False, editable=False, default=True)

#    # If this is a flight, then hopefully it's populated with useful data
#    # to allow accurate maintenance projections
#    approx_flight_hrs = models.TimeField(blank=True, null=False, default=lambda:datetime.time(0,0))
#    approx_cycles = models.IntegerField(blank=True, null=False, default=0)

    
#    class Meta:
#        app_label = 'aircraft'

##########################
###  Signal Listeners  ###
@receiver(post_save, sender=Event)
def new_event(sender, instance, created, **kwargs):
    try:
        aircraft_data = instance.aircraft_data
    except ObjectDoesNotExist:
        aircraft_data = AircraftCalendarEventData()
        aircraft_data.event = instance
    # Ensure it's saved and not forgotten
    aircraft_data.save()
    # TODO perhaps raise an alert if a maintenance event overlaps another event

@receiver(pre_save, sender=AircraftCalendarEventData)
def save_data(sender, instance, **kwargs):
    if not instance.is_maintenance:
        instance.event_duration = instance._deltaToFloat(instance.event.end - instance.event.start)
        instance.event_cycles = 1 if instance.flight_hours.seconds < instance.SINGLE_CYCLE_ASSUMPTION_THRESHOLD else 2