import core
import django.core.exceptions
from django.core.urlresolvers import reverse
from django.db import models, DEFAULT_DB_ALIAS
from schedule.models import Calendar, Event

### CASA Registration Data ###
class CasaAircraft(core.models.BaseModel):
    mark = models.CharField(primary_key=True, max_length=3)
    manu = models.CharField(max_length=60, blank=True, db_index=True) 
    aircrafttype = models.CharField(max_length=30, blank=True) 
    model = models.CharField(max_length=28, blank=True, db_index=True) 
    aircraftserial = models.CharField(max_length=18, blank=True) 
    mtow = models.IntegerField(blank=True, null=True) 
    engnum = models.IntegerField(blank=True, null=True)
    engmanu = models.CharField(max_length=60, blank=True) 
    engtype = models.CharField(max_length=14, blank=True) 
    engmodel = models.CharField(max_length=50, blank=True) 
    fueltype = models.CharField(max_length=14, blank=True) 
    regtype = models.CharField(max_length=17, blank=True) 
    regholdname = models.CharField(max_length=255, blank=True)
    regholdadd1 = models.CharField(max_length=40, blank=True)
    regholdadd2 = models.CharField(max_length=40, blank=True)
    regholdsuburb = models.CharField(max_length=39, blank=True) 
    regholdstate = models.CharField(max_length=10, blank=True) 
    regholdpostcode = models.CharField(max_length=11, blank=True) 
    regholdcountry = models.CharField(max_length=100, blank=True) 
    regholdcommdate = models.DateField(blank=True, null=True) 
    regopname = models.CharField(max_length=74, blank=True) 
    regopadd1 = models.CharField(max_length=40, blank=True)
    regopadd2 = models.CharField(max_length=40, blank=True)
    regopsuburb = models.CharField(max_length=39, blank=True) 
    regopstate = models.CharField(max_length=10, blank=True) 
    regoppostcode = models.CharField(max_length=11, blank=True) 
    regopcountry = models.CharField(max_length=100, blank=True) 
    regopcommdate = models.DateField(blank=True, null=True) 
    datefirstreg = models.DateField(blank=True, null=True) 
    gear = models.CharField(max_length=30, blank=True)
    airframe = models.CharField(max_length=30, blank=True) 
    coacata = models.CharField(max_length=20, blank=True) 
    coacatb = models.CharField(max_length=20, blank=True) 
    coacatc = models.CharField(max_length=20, blank=True) 
    propmanu = models.CharField(max_length=100, blank=True) 
    propmodel = models.CharField(max_length=30, blank=True) 
    typecert = models.CharField(max_length=15, blank=True) 
    countrymanu = models.CharField(max_length=100, blank=True) 
    yearmanu = models.IntegerField(blank=True, null=True) 
    regexpirydate = models.DateField(blank=True, null=True) 
    suspendstatus = models.CharField(max_length=30, blank=True)
    suspenddate = models.DateField(blank=True, null=True)
    icaotypedesig = models.CharField(max_length=6, blank=True, db_index=True) 

    def __str__(self):
       _str = self.manu + self.aircrafttype + " (" + self.mark + ")"
       return "%s" % _str
   
    class Meta:
        app_label = 'aircraft'
