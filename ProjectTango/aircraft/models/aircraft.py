import core
from django.core.exceptions import ObjectDoesNotExist
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from django.core.urlresolvers import reverse
from django.db import models, DEFAULT_DB_ALIAS
from schedule.models import Calendar, Event
from aircraft.models.casaaircraft import CasaAircraft

### Model Manager ###
class AircraftManager(models.Manager):
    def get_query_set(self):
        return super(AircraftManager, self).get_query_set().filter(is_deleted=False)
    # Allow a query to return logically deleted rows 
    def get_with_history(self):
        return super(AircraftManager, self).get_query_set()

#################################
### Aircraft Data per Account ###
class AircraftType(core.models.AccountModel):
    aircraft_type_name = models.CharField(max_length=255, blank=True)
    hourly_rate = models.DecimalField(decimal_places=4, max_digits=17, null=False, blank=False, default=0.0)
    max_pax = models.IntegerField(blank=True, null=False, default=0)
    takeoff_cost = models.DecimalField(decimal_places=4, max_digits=17, null=False, blank=False, default=0.0)
    avg_speed = models.DecimalField(decimal_places=4, max_digits=17, null=False, blank=False, default=0.0)
    is_deleted = models.BooleanField(default=False)

    # Override the standard manager to handle the logical delete
    objects = AircraftManager()

    def __str__(self):
        return "%s" % self.aircraft_type_name

    # Override the standard Delete method to only logically delete, but provide an alternative method to also physically delete
    # Note the custom manager prevents logically deleted items being return in the default result set, but not from being read by ID
    def delete(self, using=DEFAULT_DB_ALIAS):
        if self.pk is not None:
            self.is_deleted = True
            return super(AircraftType, self).save()
        else:
            return super(Location, self).delete(using)

    def delete_physical(self, using=DEFAULT_DB_ALIAS):
        return super(AircraftType, self).delete(using)

    class Meta:
        app_label = 'aircraft'

class Aircraft(AircraftType):
    aircraft_mark = models.CharField(max_length=20, blank=True, db_index=True)
    aircraft_description = models.CharField(max_length=100, blank=True)
    casa_record =  models.ForeignKey(CasaAircraft, blank=True, null=True)
    calendar_colour = models.CharField(max_length=7, default='#FFFFFF')

    def __str__(self):
        _str = self.aircraft_type_name
        if len(self.aircraft_mark) > 0:
            _str += " (" + self.aircraft_mark + ")"
        return "%s" % _str

    #Used in templates as obj.str doesn't work as a variable
    def to_title(self):
        return self.__str__()
    
    def get_absolute_url(self):
        return reverse('aircraft-detail', kwargs={'pk': self.pk})
    
    class Meta:
        app_label = 'aircraft'

@receiver(pre_save, sender=Aircraft)
def aircraft_pre_save(sender, instance, **kwargs):
    try:
        # Attempt to default the CASA record
        if instance.casa_record is None and instance.aircraft_mark != '':
            instance.casa_record = CasaAircraft.objects.get(pk=instance.aircraft_mark)
            if instance.casa_record is not None and instance.pk == 0:
                #Only attempt these defaults for new records
                #to allow users to blank out these fields on updates if desired
                casa_item = instance.casa_record
                if instance.aircraft_mark == '':
                    instance.aircraft_mark = casa_item.mark
                if instance.aircraft_type_name == '' and casa_item.aircrafttype != '':
                    instance.aircraft_type_name = casa_item.aircrafttype
    except ObjectDoesNotExist:
        pass

@receiver(post_save, sender=Aircraft)
def aircraft_post_save(sender, instance, created, **kwargs):
    if created:
        #Ensure we create a calendar for this new aircraft
        cal = Calendar(name=str(instance))
        # Generate a slug
        if instance.aircraft_mark != '':
            slug = instance.aircraft_mark
        else:
            slug = 'ZZ'.join([str(instance.pk)])
        cal.slug = slug
        cal.save()
        cal.create_relation(instance)
