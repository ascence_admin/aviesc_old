import core
from django.db import models
from schedule.models import Calendar, Event
from aircraft.models.aircraft import *

class AircraftMaintenanceRule(core.models.AccountModel):
    CAL_TYPE_DAY = "d"
    CAL_TYPE_MONTH = "m"
    CAL_TYPE_HR = "h"
    CAL_TYPE_WK = "w"
    CAL_TYPE_YR = "y"
    CAL_INCR_TYPE_CHOICES = (
                             (CAL_TYPE_DAY, "Days"),
                             (CAL_TYPE_WK, "Weeks"),
                             (CAL_TYPE_MONTH, "Months"),
                             (CAL_TYPE_YR, "Years"),
                             )
    CAL_DUR_TYPE_CHOICES = (
                            (CAL_TYPE_DAY, "Days"),
                            (CAL_TYPE_HR, "Hours"),
                            (CAL_TYPE_WK, "Weeks"),
                            )
    CAL_DUR_TYPE_CHOICES_SINGULAR = (
                            (CAL_TYPE_DAY, "day"),
                            (CAL_TYPE_HR, "hour"),
                            (CAL_TYPE_WK, "week"),
                            )

    RULE_TYPE_OR = "OR"
    RULE_TYPE_AND = "AND"
    RULE_TYPE_CHOICES = (
                         (RULE_TYPE_AND, "All"),
                         (RULE_TYPE_OR, "Any"),
                         )

    aircraft = models.ForeignKey(Aircraft, blank=False, editable=False)
    name = models.CharField(max_length=100, blank=False)
    tach_incr = models.IntegerField(blank=True, default=0, verbose_name="Tach")
    hobbs_incr = models.IntegerField(blank=True, default=0, verbose_name="Hobbs")
    cycles_incr = models.IntegerField(blank=True, default=0, verbose_name="Cycles")
    flight_hrs_incr = models.IntegerField(blank=True, default=0, verbose_name="Hours") #As hours
    calendar_incr = models.IntegerField(blank=True, default=0, verbose_name="Time")
    calendar_start = models.DateField(blank=True, null=True, verbose_name="Starting from")
    calendar_incr_type = models.CharField(max_length=1, choices=CAL_INCR_TYPE_CHOICES, default=CAL_TYPE_WK, blank=True, verbose_name="")
    downtime_duration = models.IntegerField(blank=True, default=0, verbose_name="Downtime")
    downtime_type = models.CharField(max_length=1, choices=CAL_DUR_TYPE_CHOICES, default=CAL_TYPE_HR, blank=True, verbose_name="")
    rule_type = models.CharField(max_length=3, choices=RULE_TYPE_CHOICES, default=RULE_TYPE_AND, blank=False)

    auto_schedule = models.BooleanField(blank=False, default=False, verbose_name="Auto-Schedule")
    raise_alert = models.BooleanField(blank=False, default=True, verbose_name="Alert")

    scheduled_event = models.ForeignKey(Event, blank=True, editable=False, null=True)

    # Allow retrieval of the flight hours increment in seconds for comparisons in the forecaster
    @property
    def flight_secs_incr(self):
        return self.flight_hrs_incr * 3600

    @property
    def calendar_incr_type_decode(self):
        return self.CAL_DUR_TYPE_CHOICES_SINGULAR[self.calendar_incr_type]

    @property
    def last_inspection(self):
        return self.logentry_set.order_by('-entry_time')[0]

    @property
    def next_due_value(self):
        if self.calendar_incr > 0:
            return self.last_inspection.entry_time + timedelta(seconds=self.flight_secs_incr)
        elif self.tach_incr > 0:
            return self.last_inspection.tach + self.tach_incr
        elif self.hobbs_incr > 0:
            return self.last_inspection.hobbs + self.hobbs_incr
        else:
            return ''

    @property
    def downtime_duration_secs(self):
        if self.downtime_type == self.CAL_TYPE_DAY:
            return self.downtime_duration * 86400
        elif self.downtime_type == self.CAL_TYPE_HR:
            return self.downtime_duration * 3600
        else: #CAL_TYPE_WK
            return self.downtime_duration * 604800

    class Meta:
        app_label = 'aircraft'

