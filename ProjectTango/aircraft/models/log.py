import django.core.exceptions
import datetime
from django.db import models
from django.db.models import Q, Max
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from schedule.models import Calendar, Event
from aircraft.models.aircraft import Aircraft
from aircraft.models.maintenancerule import AircraftMaintenanceRule
from core.models import AccountModel

class LogEntryManager(models.Manager):

    # Assist in getting the log entries for a particular aircraft after the last maintenance
    def get_entries_since_maintenance(self, rule):
        last_date = self.filter(aircraft=rule.aircraft,maintenance_rule=rule).aggregate(Max('entry_time')) 
        if not last_date['entry_time__max']:
            # There are no entries of that maintenance having occurred, so present all flight logs
            return self.filter(aircraft=rule.aircraft), rule.calendar_start
        return self.filter(aircraft=rule.aircraft, entry_time__gte=last_date['entry_time__max']), last_date['entry_time__max']

class LogEntry(AccountModel):
    # Store either as an aircraft log entry, pilot log entry or both 
    aircraft = models.ForeignKey(Aircraft, blank=True, null=True)
    pilot = models.ForeignKey(User, blank=True, null=True, related_name="aircraft_log_pilot")

    entry_time = models.DateTimeField(blank=False, db_index=True)

    # Store the relevant meters at the time of entry
    tach = models.IntegerField(blank=True, default=0)
    hobbs = models.IntegerField(blank=True, default=0)
    cycles = models.IntegerField(blank=True, default=1)
    flight_time = models.FloatField(blank=True, default=0) # as seconds

    # If this is a maintenance entry
    maintenance_rule = models.ForeignKey(AircraftMaintenanceRule, blank=True, null=True)
    maintenance_complete = models.BooleanField(default=False, blank=False)
    maintenance_by = models.ForeignKey(User, blank=True, null=True, related_name="aircraft_log_maintenance_user")

    # Any sundry comments or links to sqawks raised
    comments = models.TextField(blank=True)

    # Make it easy to enter in hours for flight time
    @property
    def flight_time_hrs(self):
        return self.flight_time / 3600

    @flight_time_hrs.setter
    def flight_time_hrs(self, value):
        self.flight_time = value * 3600

    # Use the custom manager
    objects = LogEntryManager()

    #Perform some validation to ensure that the log has the right types of information
    def clean(self):
        from django.core.exceptions import ValidationError

        # A log entry must be for at least either an aircraft or a pilot (otherwise, what's it for?)
        if not self.aircraft and not self.pilot:
            raise ValidationError('A log entry must relate to at least a Pilot or an Aircraft')

        # If this is a maintenance entry, then we don't need a pilot but we do need an aircraft
        if (self.maintenance_complete or self.maintenance_rule) and not self.aircraft:
            raise ValidationError('Maintenance entry requires an Aircraft to be specified')

        if self.maintenance_complete and not self.maintenance_rule:
            raise ValidationError('Maintenance entry requires a Rule to be specified')
        
        # If this is an aircraft entry, ensure that at least one meter has been provided along with a flight time
        if self.aircraft:
            if (self.tach == 0 and self.hobbs == 0 and self.cycles == 0) or self.flight_time == 0:
                raise ValidationError('Aircraft log entry requires flight time and a meter reading')
        
        ## TODO - move this somewhere appropriate if it turns out it's needed (will need to get the prev hobbs and subtract...) 
        ## If this is a pilot's entry, default to the Hobbs time if a flight time is not provided
        #if self.pilot and self.flight_time == datetime.time() and self.hobbs > 0:
        #    self.flight_time_hrs = 

    class Meta:
        app_label = 'aircraft'