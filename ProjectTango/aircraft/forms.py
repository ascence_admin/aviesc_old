from django import forms 
from django.utils import timezone
from django.forms import  TextInput
from aircraft.models.aircraft import Aircraft
from aircraft.models.maintenancerule import AircraftMaintenanceRule
from core.forms import AccountModelForm, InlineSplitDateTimeWidget
from django.contrib.auth.models import User
from django.utils.html import format_html

class AircraftForm(AccountModelForm):

    aircraft_mark = forms.CharField(label="Mark", widget=forms.TextInput(attrs={'placeholder':'Rego'}))
    aircraft_type_name = forms.CharField(label="Type", widget=forms.TextInput(attrs={'placeholder':'Aircraft Make/Model'}))
    aircraft_description = forms.CharField(label="Description", required=False, widget=forms.Textarea(attrs={'placeholder':'A brief description'}))
    hourly_rate = forms.DecimalField(label="Hourly Rate", initial=0.0, widget=forms.TextInput(attrs={}))
    takeoff_cost = forms.DecimalField(label="Takeoff Cost", initial=0.0, widget=forms.TextInput(attrs={}))
    avg_speed = forms.DecimalField(label="Average Speed", initial=0.0, widget=forms.TextInput(attrs={}))
    max_pax = forms.DecimalField(label="Pax", initial=0, widget=forms.TextInput(attrs={}))

    class Meta:
        model = Aircraft
        exclude = ('account', 'is_deleted',)

class LogForm(AccountModelForm):

    #SplitDateTimeWidget
    entry_time = forms.DateTimeField(label="Log Date/Time", widget=InlineSplitDateTimeWidget(attrs={}))
    tach = forms.IntegerField(label="Tach", initial=0, widget=forms.TextInput(attrs={}))
    hobbs = forms.IntegerField(label="Hobbs", initial=0, widget=forms.TextInput(attrs={}))
    flight_time_hrs = forms.DecimalField(label="Flight Time", initial=0.0, widget=forms.TextInput(attrs={}))
    cycles = forms.IntegerField(label="Cycles", initial=1, widget=forms.TextInput(attrs={}))
    comments = forms.DecimalField(label="Comments", required=False, widget=forms.Textarea(attrs={'placeholder':'Additional comments'}))

    # TODO Ajax autocomplete if this field is displayed
    #aircraft = forms.CharField(label="Aircraft", widget=forms.TextInput(attrs={}))
    aircraft = forms.ModelChoiceField(label="Aircraft", required=False, queryset=Aircraft.objects.all())
    pilot = forms.ModelChoiceField(label="Pilot", required=False, queryset=User.objects.filter(userdetails__account__exact=0, userdetails__is_pilot__exact=True))
    maintenance_rule = forms.ModelChoiceField(label='Rule', required=False, queryset=AircraftMaintenanceRule.objects.filter(account=0, aircraft=0))
    maintenance_by = forms.ModelChoiceField(label="Pilot", required=False, queryset=User.objects.filter(userdetails__account__exact=0))
    maintenance_complete = forms.BooleanField(label="Completed", required=False, initial=False, widget=forms.RadioSelect(attrs={}))

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        self.init_aircraft = kwargs.pop('aircraft', None)
        self.init_pilot = kwargs.pop('pilot', None)
        self.init_rule = kwargs.pop('rule', None)
        self.init_inspector = kwargs.pop('inspector', None)
        super().__init__(*args, **kwargs)
        if user:
            # If this user is a pilot, then default that field to them
            if user.userdetails.is_pilot:
                self.fields['pilot'].initial = user
        # If we have already been provided one of the references, then set them up here for form validation & saving
        if self.init_aircraft:
            self.fields['aircraft'].initial = self.init_aircraft
        if self.init_pilot:
            self.fields['pilot'].initial = self.init_pilot
        if self.init_rule:
            self.fields['maintenance_rule'].initial = self.init_rule
        if self.init_inspector:
            self.fields['maintenance_by'].initial = self.init_inspector
        # Set up all the Select box choices 
        self.fields['pilot'].queryset = User.objects.filter(userdetails__account__exact=self.account.pk, userdetails__is_pilot__exact=True)
        self.fields['maintenance_by'].queryset = User.objects.filter(userdetails__account__exact=self.account.pk)
        self.fields['aircraft'].queryset = Aircraft.objects.get_query_set().filter(account=self.account)
        self.fields['maintenance_rule'].queryset = AircraftMaintenanceRule.objects.filter(account=self.account, aircraft=self.init_aircraft)

    class Meta:
        model = Aircraft
        exclude = ('account',)
