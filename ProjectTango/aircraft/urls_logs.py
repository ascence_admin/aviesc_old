from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required, permission_required

from . import views

urlpatterns = patterns("",
    url(r'^add/$', login_required(views.LogCreate.as_view()), name='log-create'),
    url(r'^(?P<pk>[-_\w]+)/$', login_required(views.LogDetail.as_view()), name='log-detail'),
    url(r'^(?P<pk>[-_\w]+)/update/$', login_required(views.LogDetail.as_view()), name='log-update'),
)