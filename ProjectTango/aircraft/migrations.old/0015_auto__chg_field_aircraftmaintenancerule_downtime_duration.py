# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'AircraftMaintenanceRule.downtime_duration'
        db.delete_column('aircraft_aircraftmaintenancerule', 'downtime_duration')
        db.add_column('aircraft_aircraftmaintenancerule', 'downtime_duration', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True))

    def backwards(self, orm):

        # Changing field 'AircraftMaintenanceRule.downtime_duration'
        db.delete_column('aircraft_aircraftmaintenancerule', 'downtime_duration')
        db.add_column('aircraft_aircraftmaintenancerule', 'downtime_duration', self.gf('django.db.models.fields.TimeField')(default=0, blank=True))

    models = {
        'aircraft.aircraft': {
            'Meta': {'_ormbases': ['aircraft.AircraftType'], 'object_name': 'Aircraft'},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'blank': 'True', 'db_index': 'True', 'max_length': '20'}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'unique': 'True', 'to': "orm['aircraft.AircraftType']"}),
            'casa_record': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.CasaAircraft']"})
        },
        'aircraft.aircraftcalendareventdata': {
            'Meta': {'object_name': 'AircraftCalendarEventData'},
            'approx_cycles': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'approx_flight_hrs': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'default': '0.0'}),
            'confirmed': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'event': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'related_name': "'aircraft_data'", 'to': "orm['schedule.Event']"}),
            'event_cycles': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'event_duration': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_maintenance': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'aircraft.aircraftmaintenancerule': {
            'Meta': {'object_name': 'AircraftMaintenanceRule'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aircraft.Aircraft']"}),
            'auto_schedule': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'calendar_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'calendar_incr_type': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '1', 'default': "'w'"}),
            'calendar_start': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'cycles_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'downtime_duration': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'downtime_type': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '1', 'default': "'h'"}),
            'flight_hrs_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'hobbs_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'raise_alert': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'rule_type': ('django.db.models.fields.CharField', [], {'max_length': '3', 'default': "'AND'"}),
            'scheduled_event': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['schedule.Event']"}),
            'tach_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'max_digits': '17', 'default': '0.0'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'max_digits': '17', 'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'max_digits': '17', 'default': '0.0'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '18'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'airframe': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'coacata': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '50'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'gear': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'blank': 'True', 'db_index': 'True', 'max_length': '6'}),
            'manu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'db_index': 'True', 'max_length': '60'}),
            'mark': ('django.db.models.fields.CharField', [], {'primary_key': 'True', 'max_length': '3'}),
            'model': ('django.db.models.fields.CharField', [], {'blank': 'True', 'db_index': 'True', 'max_length': '28'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regopname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '74'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '17'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'typecert': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '15'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'})
        },
        'aircraft.logentry': {
            'Meta': {'object_name': 'LogEntry'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.Aircraft']"}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'cycles': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '1'}),
            'entry_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'flight_time': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'default': '0'}),
            'hobbs': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maintenance_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'related_name': "'aircraft_log_maintenance_user'", 'to': "orm['auth.User']"}),
            'maintenance_complete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'maintenance_rule': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.AircraftMaintenanceRule']"}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'pilot': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'related_name': "'aircraft_log_pilot'", 'to': "orm['auth.User']"}),
            'tach': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'unique_together': "(('content_type', 'codename'),)", 'ordering': "('content_type__app_label', 'content_type__model', 'codename')"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Group']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'db_table': "'django_content_type'", 'object_name': 'ContentType', 'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['schedule.Calendar']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'end_recurring_period': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'rule': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['schedule.Rule']"}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'schedule.rule': {
            'Meta': {'object_name': 'Rule'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'frequency': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'params': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'})
        }
    }

    complete_apps = ['aircraft']