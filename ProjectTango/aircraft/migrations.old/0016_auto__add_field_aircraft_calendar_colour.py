# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Aircraft.calendar_colour'
        db.add_column('aircraft_aircraft', 'calendar_colour',
                      self.gf('django.db.models.fields.CharField')(max_length=7, default='#FFFFFF'),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Aircraft.calendar_colour'
        db.delete_column('aircraft_aircraft', 'calendar_colour')


    models = {
        'aircraft.aircraft': {
            'Meta': {'object_name': 'Aircraft', '_ormbases': ['aircraft.AircraftType']},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '20'}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'to': "orm['aircraft.AircraftType']", 'unique': 'True'}),
            'calendar_colour': ('django.db.models.fields.CharField', [], {'max_length': '7', 'default': "'#FFFFFF'"}),
            'casa_record': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.CasaAircraft']"})
        },
        'aircraft.aircraftcalendareventdata': {
            'Meta': {'object_name': 'AircraftCalendarEventData'},
            'approx_cycles': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'approx_flight_hrs': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'default': '0.0'}),
            'confirmed': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'event': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'aircraft_data'", 'to': "orm['schedule.Event']", 'unique': 'True'}),
            'event_cycles': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'event_duration': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_maintenance': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'aircraft.aircraftmaintenancerule': {
            'Meta': {'object_name': 'AircraftMaintenanceRule'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aircraft.Aircraft']"}),
            'auto_schedule': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'calendar_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'calendar_incr_type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True', 'default': "'w'"}),
            'calendar_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'cycles_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'downtime_duration': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'downtime_type': ('django.db.models.fields.CharField', [], {'max_length': '1', 'blank': 'True', 'default': "'h'"}),
            'flight_hrs_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'hobbs_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'raise_alert': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'rule_type': ('django.db.models.fields.CharField', [], {'max_length': '3', 'default': "'AND'"}),
            'scheduled_event': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['schedule.Event']"}),
            'tach_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'max_digits': '17', 'default': '0.0'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'max_digits': '17', 'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'max_digits': '17', 'default': '0.0'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'max_length': '18', 'blank': 'True'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'airframe': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'coacata': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'gear': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '6'}),
            'manu': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '60'}),
            'mark': ('django.db.models.fields.CharField', [], {'primary_key': 'True', 'max_length': '3'}),
            'model': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '28'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regopname': ('django.db.models.fields.CharField', [], {'max_length': '74', 'blank': 'True'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regtype': ('django.db.models.fields.CharField', [], {'max_length': '17', 'blank': 'True'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'typecert': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'aircraft.logentry': {
            'Meta': {'object_name': 'LogEntry'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.Aircraft']"}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'cycles': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '1'}),
            'entry_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'flight_time': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'default': '0'}),
            'hobbs': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maintenance_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['auth.User']", 'related_name': "'aircraft_log_maintenance_user'"}),
            'maintenance_complete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'maintenance_rule': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.AircraftMaintenanceRule']"}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'pilot': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['auth.User']", 'related_name': "'aircraft_log_pilot'"}),
            'tach': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'object_name': 'Permission', 'unique_together': "(('content_type', 'codename'),)"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Group']", 'symmetrical': 'False', 'related_name': "'user_set'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False', 'related_name': "'user_set'"}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'db_table': "'django_content_type'", 'object_name': 'ContentType', 'unique_together': "(('app_label', 'model'),)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'forecast_ahead_days': ('django.db.models.fields.IntegerField', [], {'default': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'maint_work_end_hr': ('django.db.models.fields.IntegerField', [], {'default': '17'}),
            'maint_work_start_hr': ('django.db.models.fields.IntegerField', [], {'default': '7'}),
            'maint_work_wkend': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'schedule_rule_threshold': ('django.db.models.fields.FloatField', [], {'default': '0.9'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['schedule.Calendar']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['aircraft']