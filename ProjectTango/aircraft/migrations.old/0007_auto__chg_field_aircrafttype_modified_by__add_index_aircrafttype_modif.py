# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE aircraft_aircrafttype SET modified_by = 0 WHERE modified_by = %s", [""])
        # Renaming column for 'AircraftType.modified_by' to match new field type.
        db.rename_column('aircraft_aircrafttype', 'modified_by', 'modified_by_id')
        # Changing field 'AircraftType.modified_by'
        db.alter_column('aircraft_aircrafttype', 'modified_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'AircraftType', fields ['modified_by']
        db.create_index('aircraft_aircrafttype', ['modified_by_id'])


        # Convert these columns from emtpy strings to zeros to aid the ALTER COLUMN
        db.execute("UPDATE aircraft_aircrafttype SET created_by = 0 WHERE created_by = %s", [""])
       # Renaming column for 'AircraftType.created_by' to match new field type.
        db.rename_column('aircraft_aircrafttype', 'created_by', 'created_by_id')
        # Changing field 'AircraftType.created_by'
        db.alter_column('aircraft_aircrafttype', 'created_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))
        # Adding index on 'AircraftType', fields ['created_by']
        db.create_index('aircraft_aircrafttype', ['created_by_id'])


    def backwards(self, orm):
        # Removing index on 'AircraftType', fields ['created_by']
        db.delete_index('aircraft_aircrafttype', ['created_by_id'])

        # Removing index on 'AircraftType', fields ['modified_by']
        db.delete_index('aircraft_aircrafttype', ['modified_by_id'])


        # Renaming column for 'AircraftType.modified_by' to match new field type.
        db.rename_column('aircraft_aircrafttype', 'modified_by_id', 'modified_by')
        # Changing field 'AircraftType.modified_by'
        db.alter_column('aircraft_aircrafttype', 'modified_by', self.gf('django.db.models.fields.CharField')(max_length=100))

        # Renaming column for 'AircraftType.created_by' to match new field type.
        db.rename_column('aircraft_aircrafttype', 'created_by_id', 'created_by')
        # Changing field 'AircraftType.created_by'
        db.alter_column('aircraft_aircrafttype', 'created_by', self.gf('django.db.models.fields.CharField')(max_length=100))

    models = {
        'aircraft.aircraft': {
            'Meta': {'object_name': 'Aircraft', '_ormbases': ['aircraft.AircraftType']},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '20'}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'primary_key': 'True', 'to': "orm['aircraft.AircraftType']"}),
            'casa_record': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.CasaAircraft']"})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aircraft_aircrafttype_created_by'", 'to': "orm['auth.User']"}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aircraft_aircrafttype_modified_by'", 'to': "orm['auth.User']"}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'max_length': '18', 'blank': 'True'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'airframe': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'coacata': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'gear': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '6'}),
            'manu': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '60'}),
            'mark': ('django.db.models.fields.CharField', [], {'max_length': '3', 'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '28'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regopname': ('django.db.models.fields.CharField', [], {'max_length': '74', 'blank': 'True'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regtype': ('django.db.models.fields.CharField', [], {'max_length': '17', 'blank': 'True'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'typecert': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Permission']"})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'object_name': 'ContentType', 'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_account_created_by'", 'to': "orm['auth.User']"}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'core_account_modified_by'", 'to': "orm['auth.User']"}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['aircraft']