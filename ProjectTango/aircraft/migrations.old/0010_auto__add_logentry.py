# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'LogEntry'
        db.create_table('aircraft_logentry', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('created_by', self.gf('django.db.models.fields.IntegerField')()),
            ('created_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('modified_on', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('modified_by', self.gf('django.db.models.fields.IntegerField')()),
            ('modified_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Account'])),
            ('aircraft', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, null=True, to=orm['aircraft.Aircraft'])),
            ('pilot', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, null=True, related_name='aircraft_log_pilot', to=orm['auth.User'])),
            ('entry_time', self.gf('django.db.models.fields.DateTimeField')()),
            ('tach', self.gf('django.db.models.fields.IntegerField')(blank=True, default=0)),
            ('hobbs', self.gf('django.db.models.fields.IntegerField')(blank=True, default=0)),
            ('engine_starts', self.gf('django.db.models.fields.IntegerField')(blank=True, default=0)),
            ('flight_time', self.gf('django.db.models.fields.TimeField')(blank=True, default=0)),
            ('maintenance_rule', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, null=True, to=orm['aircraft.AircraftMaintenanceRule'])),
            ('maintenance_complete', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('maintenance_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, null=True, related_name='aircraft_log_maintenance_user', to=orm['auth.User'])),
            ('comments', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('aircraft', ['LogEntry'])


    def backwards(self, orm):
        # Deleting model 'LogEntry'
        db.delete_table('aircraft_logentry')


    models = {
        'aircraft.aircraft': {
            'Meta': {'object_name': 'Aircraft', '_ormbases': ['aircraft.AircraftType']},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20', 'db_index': 'True'}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['aircraft.AircraftType']", 'unique': 'True', 'primary_key': 'True'}),
            'casa_record': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.CasaAircraft']"})
        },
        'aircraft.aircraftmaintenancerule': {
            'Meta': {'object_name': 'AircraftMaintenanceRule'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aircraft.Aircraft']"}),
            'auto_schedule': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'calendar_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'calendar_incr_type': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "'w'", 'max_length': '1'}),
            'calendar_start': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'downtime_duration': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'downtime_type': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "'h'", 'max_length': '1'}),
            'engine_start_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'hobbs_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'raise_alert': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'rule_type': ('django.db.models.fields.CharField', [], {'default': "'AND'", 'max_length': '3'}),
            'scheduled_event': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['schedule.Event']"}),
            'tach_incr': ('django.db.models.fields.IntegerField', [], {'blank': 'True'})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'default': '0.0', 'decimal_places': '4'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'default': '0.0', 'decimal_places': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'default': '0.0', 'decimal_places': '4'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '18'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'airframe': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'coacata': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '50'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'gear': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '6', 'db_index': 'True'}),
            'manu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60', 'db_index': 'True'}),
            'mark': ('django.db.models.fields.CharField', [], {'primary_key': 'True', 'max_length': '3'}),
            'model': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '28', 'db_index': 'True'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regopname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '74'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '17'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'typecert': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '15'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'})
        },
        'aircraft.logentry': {
            'Meta': {'object_name': 'LogEntry'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.Aircraft']"}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'engine_starts': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'entry_time': ('django.db.models.fields.DateTimeField', [], {}),
            'flight_time': ('django.db.models.fields.TimeField', [], {'blank': 'True', 'default': '0'}),
            'hobbs': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maintenance_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'related_name': "'aircraft_log_maintenance_user'", 'to': "orm['auth.User']"}),
            'maintenance_complete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'maintenance_rule': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.AircraftMaintenanceRule']"}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'pilot': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'related_name': "'aircraft_log_pilot'", 'to': "orm['auth.User']"}),
            'tach': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'symmetrical': 'False', 'to': "orm['auth.Permission']"})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'unique_together': "(('content_type', 'codename'),)", 'ordering': "('content_type__app_label', 'content_type__model', 'codename')"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'symmetrical': 'False', 'to': "orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'symmetrical': 'False', 'to': "orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'object_name': 'ContentType', 'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)", 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['schedule.Calendar']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'end_recurring_period': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_maintenance': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'rule': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['schedule.Rule']"}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'schedule.rule': {
            'Meta': {'object_name': 'Rule'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'frequency': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'params': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'})
        }
    }

    complete_apps = ['aircraft']