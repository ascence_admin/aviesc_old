# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CasaAircraft'
        db.create_table('aircraft_casaaircraft', (
            ('mark', self.gf('django.db.models.fields.CharField')(primary_key=True, max_length=3)),
            ('manu', self.gf('django.db.models.fields.CharField')(blank=True, max_length=60)),
            ('aircrafttype', self.gf('django.db.models.fields.CharField')(blank=True, max_length=30)),
            ('model', self.gf('django.db.models.fields.CharField')(blank=True, max_length=28)),
            ('aircraftserial', self.gf('django.db.models.fields.CharField')(blank=True, max_length=18)),
            ('mtow', self.gf('django.db.models.fields.IntegerField')(blank=True, null=True)),
            ('engnum', self.gf('django.db.models.fields.IntegerField')(blank=True, null=True)),
            ('engmanu', self.gf('django.db.models.fields.CharField')(blank=True, max_length=60)),
            ('engtype', self.gf('django.db.models.fields.CharField')(blank=True, max_length=14)),
            ('engmodel', self.gf('django.db.models.fields.CharField')(blank=True, max_length=50)),
            ('fueltype', self.gf('django.db.models.fields.CharField')(blank=True, max_length=14)),
            ('regtype', self.gf('django.db.models.fields.CharField')(blank=True, max_length=17)),
            ('regholdname', self.gf('django.db.models.fields.CharField')(blank=True, max_length=255)),
            ('regholdadd1', self.gf('django.db.models.fields.CharField')(blank=True, max_length=40)),
            ('regholdadd2', self.gf('django.db.models.fields.CharField')(blank=True, max_length=40)),
            ('regholdsuburb', self.gf('django.db.models.fields.CharField')(blank=True, max_length=39)),
            ('regholdstate', self.gf('django.db.models.fields.CharField')(blank=True, max_length=10)),
            ('regholdpostcode', self.gf('django.db.models.fields.CharField')(blank=True, max_length=11)),
            ('regholdcountry', self.gf('django.db.models.fields.CharField')(blank=True, max_length=100)),
            ('regholdcommdate', self.gf('django.db.models.fields.DateField')(blank=True, null=True)),
            ('regopname', self.gf('django.db.models.fields.CharField')(blank=True, max_length=74)),
            ('regopadd1', self.gf('django.db.models.fields.CharField')(blank=True, max_length=40)),
            ('regopadd2', self.gf('django.db.models.fields.CharField')(blank=True, max_length=40)),
            ('regopsuburb', self.gf('django.db.models.fields.CharField')(blank=True, max_length=39)),
            ('regopstate', self.gf('django.db.models.fields.CharField')(blank=True, max_length=10)),
            ('regoppostcode', self.gf('django.db.models.fields.CharField')(blank=True, max_length=11)),
            ('regopcountry', self.gf('django.db.models.fields.CharField')(blank=True, max_length=100)),
            ('regopcommdate', self.gf('django.db.models.fields.DateField')(blank=True, null=True)),
            ('datefirstreg', self.gf('django.db.models.fields.DateField')(blank=True, null=True)),
            ('gear', self.gf('django.db.models.fields.CharField')(blank=True, max_length=30)),
            ('airframe', self.gf('django.db.models.fields.CharField')(blank=True, max_length=30)),
            ('coacata', self.gf('django.db.models.fields.CharField')(blank=True, max_length=20)),
            ('coacatb', self.gf('django.db.models.fields.CharField')(blank=True, max_length=20)),
            ('coacatc', self.gf('django.db.models.fields.CharField')(blank=True, max_length=20)),
            ('propmanu', self.gf('django.db.models.fields.CharField')(blank=True, max_length=100)),
            ('propmodel', self.gf('django.db.models.fields.CharField')(blank=True, max_length=30)),
            ('typecert', self.gf('django.db.models.fields.CharField')(blank=True, max_length=15)),
            ('countrymanu', self.gf('django.db.models.fields.CharField')(blank=True, max_length=100)),
            ('yearmanu', self.gf('django.db.models.fields.IntegerField')(blank=True, null=True)),
            ('regexpirydate', self.gf('django.db.models.fields.DateField')(blank=True, null=True)),
            ('suspendstatus', self.gf('django.db.models.fields.CharField')(blank=True, max_length=30)),
            ('suspenddate', self.gf('django.db.models.fields.DateField')(blank=True, null=True)),
            ('icaotypedesig', self.gf('django.db.models.fields.CharField')(blank=True, max_length=6)),
        ))
        db.send_create_signal('aircraft', ['CasaAircraft'])

        # Adding model 'AircraftType'
        db.create_table('aircraft_aircrafttype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('created_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('created_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('modified_on', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('modified_by', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('modified_ip', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Account'])),
            ('aircraft_type_name', self.gf('django.db.models.fields.CharField')(blank=True, max_length=255)),
            ('hourly_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=17, blank=True, decimal_places=4, null=True)),
            ('max_pax', self.gf('django.db.models.fields.IntegerField')(blank=True, null=True)),
            ('takeoff_cost', self.gf('django.db.models.fields.DecimalField')(max_digits=17, blank=True, decimal_places=4, null=True)),
            ('avg_speed', self.gf('django.db.models.fields.DecimalField')(max_digits=17, blank=True, decimal_places=4, null=True)),
        ))
        db.send_create_signal('aircraft', ['AircraftType'])

        # Adding model 'Aircraft'
        db.create_table('aircraft_aircraft', (
            ('aircrafttype_ptr', self.gf('django.db.models.fields.related.OneToOneField')(primary_key=True, to=orm['aircraft.AircraftType'], unique=True)),
            ('aircraft_mark', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, to=orm['aircraft.CasaAircraft'])),
            ('aircraft_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['aircraft.AircraftType'], related_name='type_of_aircraft')),
            ('aircraft_description', self.gf('django.db.models.fields.CharField')(blank=True, max_length=100)),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('aircraft', ['Aircraft'])


    def backwards(self, orm):
        # Deleting model 'CasaAircraft'
        db.delete_table('aircraft_casaaircraft')

        # Deleting model 'AircraftType'
        db.delete_table('aircraft_aircrafttype')

        # Deleting model 'Aircraft'
        db.delete_table('aircraft_aircraft')


    models = {
        'aircraft.aircraft': {
            'Meta': {'_ormbases': ['aircraft.AircraftType'], 'object_name': 'Aircraft'},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'aircraft_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aircraft.AircraftType']", 'related_name': "'type_of_aircraft'"}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'to': "orm['aircraft.AircraftType']", 'unique': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'blank': 'True', 'decimal_places': '4', 'null': 'True'}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'blank': 'True', 'decimal_places': '4', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'blank': 'True', 'decimal_places': '4', 'null': 'True'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '18'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'airframe': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'coacata': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '50'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'gear': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '6'}),
            'manu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60'}),
            'mark': ('django.db.models.fields.CharField', [], {'primary_key': 'True', 'max_length': '3'}),
            'model': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '28'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regopname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '74'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '17'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'typecert': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '15'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        }
    }

    complete_apps = ['aircraft']