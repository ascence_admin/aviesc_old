# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AircraftCalendarEventData'
        db.create_table('aircraft_aircraftcalendareventdata', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['schedule.Event'], unique=True, related_name='aircraft_data')),
            ('is_maintenance', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('confirmed', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('approx_flight_hrs', self.gf('django.db.models.fields.FloatField')(default=0.0, blank=True)),
            ('approx_cycles', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
            ('event_duration', self.gf('django.db.models.fields.FloatField')(default=0.0, blank=True)),
            ('event_cycles', self.gf('django.db.models.fields.IntegerField')(default=0, blank=True)),
        ))
        db.send_create_signal('aircraft', ['AircraftCalendarEventData'])


        # Adding index on 'LogEntry', fields ['entry_time']
        db.create_index('aircraft_logentry', ['entry_time'])


        # Changing field 'LogEntry.flight_time'
        db.delete_column('aircraft_logentry', 'flight_time')
        db.add_column('aircraft_logentry', 'flight_time', self.gf('django.db.models.fields.FloatField')(default=0, blank=True))

    def backwards(self, orm):
        # Removing index on 'LogEntry', fields ['entry_time']
        db.delete_index('aircraft_logentry', ['entry_time'])

        # Deleting model 'AircraftCalendarEventData'
        db.delete_table('aircraft_aircraftcalendareventdata')


        # Changing field 'AircraftMaintenanceRule.downtime_duration'
        db.alter_column('aircraft_aircraftmaintenancerule', 'downtime_duration', self.gf('django.db.models.fields.IntegerField')())

        # Changing field 'LogEntry.flight_time'
        db.delete_column('aircraft_logentry', 'flight_time')
        db.add_column('aircraft_logentry', 'flight_time', self.gf('django.db.models.fields.TimeField')(default=0, blank=True))

    models = {
        'aircraft.aircraft': {
            'Meta': {'object_name': 'Aircraft', '_ormbases': ['aircraft.AircraftType']},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True', 'db_index': 'True'}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['aircraft.AircraftType']", 'unique': 'True', 'primary_key': 'True'}),
            'casa_record': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aircraft.CasaAircraft']", 'null': 'True', 'blank': 'True'})
        },
        'aircraft.aircraftcalendareventdata': {
            'Meta': {'object_name': 'AircraftCalendarEventData'},
            'approx_cycles': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'approx_flight_hrs': ('django.db.models.fields.FloatField', [], {'default': '0.0', 'blank': 'True'}),
            'confirmed': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'event': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['schedule.Event']", 'unique': 'True', 'related_name': "'aircraft_data'"}),
            'event_cycles': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'event_duration': ('django.db.models.fields.FloatField', [], {'default': '0.0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_maintenance': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'aircraft.aircraftmaintenancerule': {
            'Meta': {'object_name': 'AircraftMaintenanceRule'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aircraft.Aircraft']"}),
            'auto_schedule': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'calendar_incr': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'calendar_incr_type': ('django.db.models.fields.CharField', [], {'default': "'w'", 'max_length': '1', 'blank': 'True'}),
            'calendar_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'cycles_incr': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'downtime_duration': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(0, 0)', 'blank': 'True'}),
            'downtime_type': ('django.db.models.fields.CharField', [], {'default': "'h'", 'max_length': '1', 'blank': 'True'}),
            'flight_hrs_incr': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'hobbs_incr': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'raise_alert': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'rule_type': ('django.db.models.fields.CharField', [], {'default': "'AND'", 'max_length': '3'}),
            'scheduled_event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Event']", 'null': 'True', 'blank': 'True'}),
            'tach_incr': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '17', 'decimal_places': '4'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '17', 'decimal_places': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '17', 'decimal_places': '4'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'max_length': '18', 'blank': 'True'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'airframe': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'coacata': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'gear': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'max_length': '6', 'blank': 'True', 'db_index': 'True'}),
            'manu': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True', 'db_index': 'True'}),
            'mark': ('django.db.models.fields.CharField', [], {'max_length': '3', 'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '28', 'blank': 'True', 'db_index': 'True'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regopname': ('django.db.models.fields.CharField', [], {'max_length': '74', 'blank': 'True'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regtype': ('django.db.models.fields.CharField', [], {'max_length': '17', 'blank': 'True'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'typecert': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'aircraft.logentry': {
            'Meta': {'object_name': 'LogEntry'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aircraft.Aircraft']", 'null': 'True', 'blank': 'True'}),
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'cycles': ('django.db.models.fields.IntegerField', [], {'default': '1', 'blank': 'True'}),
            'entry_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'flight_time': ('django.db.models.fields.FloatField', [], {'default': '0', 'blank': 'True'}),
            'hobbs': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maintenance_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'related_name': "'aircraft_log_maintenance_user'", 'blank': 'True'}),
            'maintenance_complete': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'maintenance_rule': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aircraft.AircraftMaintenanceRule']", 'null': 'True', 'blank': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'pilot': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'related_name': "'aircraft_log_pilot'", 'blank': 'True'}),
            'tach': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Permission']"})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Calendar']", 'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'end_recurring_period': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'rule': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Rule']", 'null': 'True', 'blank': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'schedule.rule': {
            'Meta': {'object_name': 'Rule'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'frequency': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'params': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['aircraft']