# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Aircraft.is_deleted'
        db.delete_column('aircraft_aircraft', 'is_deleted')

        # Adding field 'AircraftType.is_deleted'
        db.add_column('aircraft_aircrafttype', 'is_deleted',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Aircraft.is_deleted'
        db.add_column('aircraft_aircraft', 'is_deleted',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Deleting field 'AircraftType.is_deleted'
        db.delete_column('aircraft_aircrafttype', 'is_deleted')


    models = {
        'aircraft.aircraft': {
            'Meta': {'object_name': 'Aircraft', '_ormbases': ['aircraft.AircraftType']},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'primary_key': 'True', 'to': "orm['aircraft.AircraftType']"}),
            'casa_record': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['aircraft.CasaAircraft']"})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'max_length': '18', 'blank': 'True'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'airframe': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'coacata': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'gear': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'max_length': '6', 'blank': 'True'}),
            'manu': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'mark': ('django.db.models.fields.CharField', [], {'max_length': '3', 'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '28', 'blank': 'True'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regopname': ('django.db.models.fields.CharField', [], {'max_length': '74', 'blank': 'True'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regtype': ('django.db.models.fields.CharField', [], {'max_length': '17', 'blank': 'True'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'typecert': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        }
    }

    complete_apps = ['aircraft']