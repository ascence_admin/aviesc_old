import datetime

from django.core.exceptions import ObjectDoesNotExist
from django.utils.timezone import utc
from django.utils import timezone

from core.tests import AuditableTestCase
from aircraft.models.aircraft import AircraftType, Aircraft
from aircraft.models.casaaircraft import CasaAircraft
from aircraft.models.log import LogEntry
from aircraft.models.maintenancerule import AircraftMaintenanceRule
from aircraft.utils import MaintenanceForecaster
from core.models import Account
from schedule.models.calendars import Calendar
from schedule.models.events import Event
from schedule.models.rules import Rule

class TestAircraft(AuditableTestCase):
    def _forward_one_working_day(self, from_date):
        if (from_date.weekday() % 7) < 5:
            return from_date
        else:
            delta = datetime.timedelta(days=(7 - from_date.weekday()))
            return from_date + delta

    def _backward_one_working_day(self, from_date):
        if (from_date.weekday() % 7) < 5:
            return from_date
        else:
            delta = datetime.timedelta(days=((from_date.weekday() % 5) +1)*-1)
            return from_date + delta

    def _print_event_debug(self, event):
        self.assertNotEqual(event.title, '', "Event with no title")
        print('DEBUG - Event: %s\n   %s - %s' % (event.title, event.start, event.end))

    def _print_log_debug(self, log):
        print('DEBUG - Log: at %s %s' % (log.entry_time, 'Maintenance' if log.maintenance_complete else ''))

    def setUp(self):
        super().setUp()
        self.account_details = {
                                'account_name' : 'Test Account',
                                'inactive' : False,
                                'start_dt' : datetime.date(2013, 1, 1),
                                }

        test_account = Account(**self.account_details)
        test_account.save()

        self.casa_details = {
                        'mark' : 'ABC',
                        'manu' : 'DE HAVILLAND AIRCRAFT PTY LTD',
                        'aircrafttype' : 'DH-82',
                        'model' : 'DH-82A',
                        'aircraftserial' : 'A17-148',
                        'mtow' : '828',
                        'engnum' : '1',
                        'yearmanu' : '1940',
                        }
        casa = CasaAircraft(**self.casa_details)
        casa.save()

        self.aircraft_type_details = {
                                      'account' : test_account,
                                      'aircraft_type_name' : 'DH-82A Fleet',
                                      'hourly_rate' : 120.00,
                                      'max_pax' : 10,
                                      'avg_speed' : 120.0,
                                      'takeoff_cost' : 10.0,
                                      }

        self.aircraft_details = dict({
                                 'aircraft_description' : 'a new test aircraft that we can start flying!',
                                 },**self.aircraft_type_details)
        self.aircraft = Aircraft(**self.aircraft_details)
        self.aircraft.save()
        
        baseline = timezone.now()
        
        self.entry_dates = {
                            'log1':baseline - datetime.timedelta(days=5),
                            'log2':baseline - datetime.timedelta(days=4),
                            'ruleALog':baseline - datetime.timedelta(days=2),
                            'ruleBLog':baseline - datetime.timedelta(days=2),
                            'log3':baseline - datetime.timedelta(days=1),
                            'booking1':baseline + datetime.timedelta(days=3, minutes=baseline.minute*-1, seconds=baseline.second*-1,microseconds=baseline.microsecond*-1),
                            'booking2':baseline + datetime.timedelta(days=3, hours=-3, minutes=baseline.minute*-1, seconds=baseline.second*-1,microseconds=baseline.microsecond*-1),
                            'booking3':baseline + datetime.timedelta(days=3, hours=-1, minutes=baseline.minute*-1, seconds=baseline.second*-1,microseconds=baseline.microsecond*-1),
                            }

    def test_casa_aircraft(self):
        """ Test the maintenance (creation, update, delete) of aircraft details
        where the aircraft is registered with CASA
        """
        print('====================\n  Test Casa Aircraft')
        casa = CasaAircraft.objects.get(pk=self.casa_details['mark'])
        self.assertEqual(casa.mark, self.casa_details['mark'], "Casa Aircraft not retrieved")
        self.assertEqual(casa.manu, self.casa_details['manu'], "Casa Aircraft not retrieved")

        #Register a new aircraft, providing the Casa record
        new_plane = Aircraft(**self.aircraft_details)
        new_plane.casa_record = casa
        new_plane.save()

        #Read the object and test
        new_plane = Aircraft.objects.get(aircraft_mark=self.casa_details['mark'])
        self.assertTrue(new_plane.pk is not None)

        new_pk = new_plane.pk

        #Ensure our defaults are set
        self.assertFalse(new_plane.is_deleted, "Aircraft is_deleted not False by default")
        self.assertEqual(new_plane.aircraft_mark, self.casa_details['mark'], "Aircraft mark not defaulted from casa")
        self.assertEqual(new_plane.aircraft_type_name, self.aircraft_type_details['aircraft_type_name'], "Aircraft type name not saved correctly")
        
        #Ensure we can still expect multi-table inheritance
        new_plane = AircraftType.objects.get(pk=new_pk)
        self.assertIsNotNone(new_plane.aircraft, "Aircraft Type not retrieving child Aircraft record")

        #Check that we have a calendar created for it
        cal = Calendar.objects.get_calendar_for_object(new_plane.aircraft)
        self.assertEqual(cal.name, str(new_plane.aircraft), "Aircraft calendar name not correct") 

        #Test the logical delete
        new_plane.delete()

        new_plane = Aircraft.objects.get(pk=new_pk)
        self.assertIsNotNone(new_plane.pk, "Logical delete failed")
        self.assertTrue(new_plane.is_deleted, "Logical delete flag not set")

        #Test the physical delete
        new_plane.delete_physical()
        self.assertRaises(ObjectDoesNotExist, Aircraft.objects.get, pk=new_pk)
            
    def test_custom_aircraft(self):
        print('====================\n  test_custom_aircraft')
        #Create a new aircraft with no link to a Casa record
        new_plane = Aircraft(**self.aircraft_details)
        new_plane.save()

        new_pk = new_plane.pk

        #Read the object and test
        new_plane = Aircraft.objects.get(pk=new_pk)
        self.assertTrue(new_plane.pk is not None)

        #Ensure our defaults are set
        self.assertFalse(new_plane.is_deleted, "Aircraft is_deleted not False by default")
        self.assertEqual(new_plane.aircraft_mark, '', "Aircraft mark not empty on new aircraft")
        self.assertEqual(new_plane.aircraft_type_name, self.aircraft_type_details['aircraft_type_name'], "Aircraft type name not saved on new aircraft")

        #Ensure we can still expect multi-table inheritance
        new_plane = AircraftType.objects.get(pk=new_pk)
        self.assertIsNotNone(new_plane.aircraft, "Aircraft Type not retrieving child Aircraft record")

    def test_aircraft_type(self):
        print('====================\n  test_aircraft_type')
        #Create just an aircraft type record
        new_plane = AircraftType(**self.aircraft_type_details)
        new_plane.save()

        new_pk = new_plane.pk

        #Read the object and test
        new_plane = AircraftType.objects.get(pk=new_pk)
        self.assertTrue(new_plane.pk is not None)

        #Ensure our defaults are set
        self.assertFalse(new_plane.is_deleted, "Aircraft Type is_deleted not False by default")
        self.assertEqual(new_plane.aircraft_type_name, self.aircraft_type_details['aircraft_type_name'], "Aircraft type name not saved on new aircraft")

        #Ensure we can still expect multi-table inheritance
        self.assertRaises(ObjectDoesNotExist, lambda x: new_plane.aircraft, "Aircraft Type not retrieving child Aircraft record")

    def _create_initial_log_entries(self):
        # Enter in some flight logs for the aircraft
        log = LogEntry()
        log.aircraft = self.aircraft
        log.entry_time = self.entry_dates['log1']
        log.cycles = 1
        log.flight_time_hrs = 2.5
        log.tach = 34980
        log.hobbs = 2394
        log.save()
        self._print_log_debug(log)
        log = LogEntry()
        log.aircraft = self.aircraft
        log.entry_time = self.entry_dates['log2']
        log.cycles = 1
        log.flight_time_hrs = 2.5
        log.tach = 34980
        log.hobbs = 2394
        log.save()
        self._print_log_debug(log)
        log = LogEntry()
        log.aircraft = self.aircraft
        log.entry_time = self.entry_dates['log3']
        log.cycles = 1
        log.flight_time_hrs = 6.5
        log.tach = 34997
        log.hobbs = 2399
        log.save()
        self._print_log_debug(log)

    def test_calendar_rules_day(self):
        print('====================\n  test_calendar_rules_day')
        
        # Create a new set of rules for the aircraft
        ruleA = AircraftMaintenanceRule()
        ruleA.aircraft = self.aircraft
        ruleA.name = "Inspection A"
        ruleA.calendar_incr = 5
        ruleA.calendar_incr_type = AircraftMaintenanceRule.CAL_TYPE_DAY
        ruleA.calendar_start = datetime.date(2013, 6, 1)
        ruleA.downtime_duration = 1
        ruleA.downtime_type = ruleA.CAL_TYPE_DAY
        ruleA.auto_schedule = True
        ruleA.save()

        # Create some logs
        self._create_initial_log_entries()

        #Test the LogEntryManager
        # Here, no maintenance has been recorded for the aircraft, so all logs should be retrieved
        recent_entries, last_date = LogEntry.objects.get_entries_since_maintenance(ruleA)
        self.assertEqual(recent_entries.count(), 3, "get_entries_since_maintenance answer incorrect")

        # Now, let's enter a maintenance record for Rule A
        log = LogEntry()
        log.aircraft = self.aircraft
        log.entry_time = self.entry_dates['ruleALog']
        log.maintenance_rule = ruleA
        log.maintenance_complete = True
        log.save()

        self._print_log_debug(log)

        # Now the manager should only give us the 1 log since Rule A was last complete, plus the maintenance log entry itself
        recent_entries, last_date = LogEntry.objects.get_entries_since_maintenance(ruleA)
        self.assertEqual(recent_entries.count(), 2, "get_entries_since_maintenance not correct with maintenance")

        scheduler = MaintenanceForecaster()
        scheduler.forecastForRule(ruleA, True)

        # Rule A inspection should be scheduled for 5 days after the above log
        # since there are no conflicting appointments
        self.assertEqual(ruleA.scheduled_event.start.date(), self._forward_one_working_day(log.entry_time + datetime.timedelta(days=5)).date(), "Calendar Rule not scheduled correctly")
        self.assertEqual(ruleA.scheduled_event.end, ruleA.scheduled_event.start + datetime.timedelta(hours=24), "Calendar Rule not for right duration")

        self._print_event_debug(ruleA.scheduled_event)

        # Add a booking on that date and ensure that we default to the next day
        cal = Calendar.objects.get_calendar_for_object(self.aircraft)
        booking = Event()
        booking.title = "Johnny's First Flight"
        booking.start = self._forward_one_working_day(self.entry_dates['ruleALog'] + datetime.timedelta(days=5))
        booking.start.replace(hour=10, minute=0, second=0, microsecond=0)
        booking.end = booking.start + datetime.timedelta(hours=2, minutes=0)
        booking.calendar = cal
        booking.save()

        self._print_event_debug(booking)

        scheduler.forecastForRule(ruleA, True)

        # Rule A inspection should be scheduled for the day after it's due
        self.assertEqual(ruleA.scheduled_event.start.date(), self._forward_one_working_day(log.entry_time + datetime.timedelta(days=6)).date(), "Calendar Rule not scheduled correctly")

        # And now book on the following day as well, to ensure that we go back in time
        booking = Event()
        booking.title = "Johnny's Trial Flight"
        booking.start = self._forward_one_working_day(self.entry_dates['ruleALog'] + datetime.timedelta(days=6))
        booking.start.replace(hour=10, minute=0, second=0, microsecond=0)
        booking.end = booking.start + datetime.timedelta(hours=2, minutes=0)
        booking.calendar = cal
        booking.save()

        self._print_event_debug(booking)

        scheduler.forecastForRule(ruleA, True)

        # Rule A inspection should be scheduled for 4 days after the above log
        # since there are conflicting appointments
        self.assertEqual(ruleA.scheduled_event.start.date(), self._backward_one_working_day(log.entry_time + datetime.timedelta(days=4)).date(), "Calendar Rule not scheduled correctly")

    def test_flight_hrs_hour(self):
        print('====================\n  test_flight_hrs_hour')
        ruleB = AircraftMaintenanceRule()
        ruleB.aircraft = self.aircraft
        ruleB.flight_hrs_incr = 10
        ruleB.auto_schedule = True
        ruleB.downtime_duration = 1
        ruleB.downtime_type = ruleB.CAL_TYPE_HR
        ruleB.name = "Inspection B"
        ruleB.save()

        # Create the initial logs
        self._create_initial_log_entries()

        # Test the auto schedule of Rule B, which should create an Event starting on the next hour
        # since we've logged more hours than the rule allows
        scheduler = MaintenanceForecaster()
        scheduler.forecastForRule(ruleB, True)
        self.assertGreater(ruleB.scheduled_event.pk, 0, "Rule B Event not scheduled")
        self.assertEqual(ruleB.scheduled_event.title, ruleB.name, "Auto schedule event name incorrect")
        self.assertEqual(ruleB.scheduled_event.start.hour, (timezone.now().hour + 1) % 24, "Scheduled event not starting on time")
        self.assertEqual(ruleB.scheduled_event.start.minute, 0, "Scheduled event not starting on the hour")

        self._print_event_debug(ruleB.scheduled_event)

        # Now book in scheduled flights, and test the AircraftEventData object
        cal = Calendar.objects.get_calendar_for_object(self.aircraft)
        booking = Event()
        booking.title = "Johnny's First Flight"
        booking.start = self.entry_dates['booking1']
        booking.end = self.entry_dates['booking1'] + datetime.timedelta(hours=4, minutes=30)
        booking.calendar = cal
        booking.save()

        print('Account: Start=%s;End=%s;Wkend=%s' % (self.account.maint_work_start_hr, self.account.maint_work_end_hr, self.account.maint_work_wkend))

        self._print_event_debug(booking)

        # Enter in a log for Rule B and recalculate, factoring in the future booking we now have
        log = LogEntry()
        log.aircraft = self.aircraft
        log.entry_time = self.entry_dates['ruleBLog']
        log.maintenance_rule = ruleB
        log.maintenance_complete = True
        log.save()

        self._print_log_debug(log)
         
        # Refresh the rule as various caches are now ouf of date
        ruleB = AircraftMaintenanceRule.objects.get(pk=ruleB.pk)
       
        scheduler.forecastForRule(ruleB, True)
        self.assertGreater(ruleB.scheduled_event.pk, 0, "Rule B Event not scheduled")
        self.assertEqual(ruleB.scheduled_event.title, ruleB.name, "Auto schedule event name incorrect")
        ev_start_delta = datetime.timedelta(hours=-1, minutes=self.entry_dates['booking1'].minute*-1, seconds=self.entry_dates['booking1'].second*-1,microseconds=self.entry_dates['booking1'].microsecond*-1)
        self.assertEqual(ruleB.scheduled_event.start, self.entry_dates['booking1'] + ev_start_delta, "Scheduled event not starting on time")
        self.assertEqual(ruleB.scheduled_event.start.minute, 0, "Scheduled event not starting on the hour")

        self._print_event_debug(ruleB.scheduled_event)

        # Now crowd up the calendar and check the forecasting
        booking = Event()
        booking.title = "Timy's Graduating Flight"
        booking.start = self.entry_dates['booking2']
        booking.end = self.entry_dates['booking2'] + datetime.timedelta(hours = 1)
        booking.calendar = cal
        booking.save()

        self._print_event_debug(booking)

        booking = Event()
        booking.title = "Sally's Solo Flight"
        booking.start = self.entry_dates['booking3']
        booking.end = self.entry_dates['booking3'] + datetime.timedelta(minutes=30)
        booking.calendar = cal
        booking.save()

        self._print_event_debug(booking)

        # Refresh the rule as various caches are now ouf of date
        ruleB = AircraftMaintenanceRule.objects.get(pk=ruleB.pk)

        scheduler.forecastForRule(ruleB, True)
        self.assertGreater(ruleB.scheduled_event.pk, 0, "Rule B Event not scheduled")
        self._print_event_debug(ruleB.scheduled_event)
        adjusted_inspection = Event.objects.get(pk=ruleB.scheduled_event.pk)
        self.assertEqual(adjusted_inspection.title, ruleB.name, "Auto schedule event name incorrect")
        #ev_start_delta = datetime.timedelta(hours = -2, minutes=self.entry_dates['booking1'].minute*-1, seconds=self.entry_dates['booking1'].second*-1,microseconds=self.entry_dates['booking1'].microsecond*-1)
        #ev_end_delta = datetime.timedelta(hours = -1, minutes=self.entry_dates['booking1'].minute*-1, seconds=self.entry_dates['booking1'].second*-1,microseconds=self.entry_dates['booking1'].microsecond*-1)
        self.assertEqual(adjusted_inspection.start, self.entry_dates['booking2'] + datetime.timedelta(hours = 1), "Scheduled event not starting on time")
        self.assertEqual(adjusted_inspection.end, self.entry_dates['booking2'] + datetime.timedelta(hours = 2), "Scheduled event not starting on the hour")

    def test_aircraft_data_object(self):
        print('====================\n  test_aircraft_data_object')
        
        # Now book in scheduled flights, and test the AircraftEventData object
        cal = Calendar.objects.get_calendar_for_object(self.aircraft)
        booking = Event()
        booking.title = "Johnny's First Flight"
        booking.start = self.entry_dates['booking1']
        booking.end = self.entry_dates['booking1'] + datetime.timedelta(hours = 1)
        booking.calendar = cal
        booking.save()

        self.assertTrue(booking.aircraft_data.confirmed, "Standard Event not confirmed by default")
        self.assertFalse(booking.aircraft_data.is_maintenance, "Is_Maintenance flag not False by default")
        self.assertEqual(booking.aircraft_data.flight_hours, datetime.timedelta(hours=1), "Flight Hours not calculating off Event correctly")
        self.assertEqual(booking.aircraft_data.cycles, 1, "Cycles not calculating off Event correctly")

        #Update the booking to test the post_save calculations
        booking.end = self.entry_dates['booking1'] + datetime.timedelta(hours=4, minutes=30)
        booking.save()

        self.assertEqual(booking.aircraft_data.flight_hours, datetime.timedelta(hours=4, minutes=30), "Flight Hours not calculating off Event correctly")
        self.assertEqual(booking.aircraft_data.cycles, 2, "Cycles not calculating off Event correctly")
        
        booking.aircraft_data.flight_hours = datetime.timedelta(hours=3, minutes=15)
        booking.aircraft_data.approx_cycles = 3
        self.assertEqual(booking.aircraft_data.flight_hours, datetime.timedelta(hours=3, minutes=15), "Flight Hours not calculating off Event correctly")
        self.assertEqual(booking.aircraft_data.cycles, 3, "Cycles not calculating off Event correctly")
