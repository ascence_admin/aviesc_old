from django import forms
from django.utils.translation import ugettext as _

from crispy_forms_foundation.layout import Layout, Fieldset, Field, SplitDateTimeField, Row, RowFluid, Column, Div, \
    ButtonHolder, Submit, HTML
from crispy_forms.helper import FormHelper

from invoice.models import Invoice, Extras, Receipt


class ExtrasCreateForm(forms.ModelForm):
    class Meta:
        model = Extras
        fields = ('quantity', 'description', 'unit_price', )
        widgets = {
            'description': forms.Textarea(attrs={'rows': 1, 'cols': 15}),
        }

    def __init__(self, *args, **kwargs):
        super(ExtrasCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Column('quantity', css_class="large-4"),
                Column('unit_price', css_class="large-4"),
                Column('description', css_class="large-4"),
            ),
        )


class ExtrasUpdateForm(forms.ModelForm):
    class Meta:
        model = Extras
        fields = ('invoice', 'quantity', 'description', 'unit_price', )
        widgets = {
            'description': forms.Textarea(attrs={'rows': 1, 'cols': 15}),
        }

    def __init__(self, *args, **kwargs):
        super(ExtrasUpdateForm, self).__init__(*args, **kwargs)
        self.fields['invoice'].widget = forms.HiddenInput()
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Field('invoice'),
            ),
            Row(
                Column('quantity', css_class="large-3"),
                Column('unit_price', css_class="large-4"),
                Column('description', css_class="large-4"),
            ),
        )


class InvoiceCreateForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ('id', 'account', 'customer_name', 'customer_address', 'customer_zip', 'customer_city', 'customer',
                  'event_title', 'event_start', 'event_end', 'event', 'pilot_name', 'aircraft_mark', 'aircraft_type',
                  'passengers_number', 'aircraft_takeoff_cost', 'pilot', 'aircraft', 'status', 'date_created',
                  'due_date', 'tax', 'discount', 'comment')

    def __init__(self, *args, **kwargs):
        super(InvoiceCreateForm, self).__init__(*args, **kwargs)
        self.fields['account'].widget = forms.HiddenInput()
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset(
                _('Customer'),
                Field('account'),
                Row(
                    Column('customer_name', css_class='large-6'),
                ),
                Row(
                    Column('customer_address', css_class='large-4'),
                    Column('customer_zip', css_class='large-4'),
                    Column('customer_city', css_class='large-4'),
                ),
                Fieldset(
                    _('References'),
                    HTML("<p>Get customer data from selected</p>"),
                    Row(
                        Column('customer', css_class='large-12'),
                    ),
                ),
            ),
            Fieldset(
                _('Event'),
                Row(
                    Column('event_title', css_class='large-4 disabled'),
                    Column(SplitDateTimeField('event_start'), css_class='large-4'),
                    Column('event_end', css_class='large-4'),
                ),
                Fieldset(
                    _('References'),
                    HTML("<p>Get event data from selected</p>"),
                    Row(
                        Column('event', css_class='large-12'),
                    ),
                ),
            ),
            Fieldset(
                _('Aircraft'),
                Row(
                    Column('pilot_name', css_class='large-4'),
                    Column('aircraft_mark', css_class='large-4'),
                    Column('aircraft_type', css_class='large-4'),
                ),
                Row(
                    Column('passengers_number', css_class='large-4'),
                    Column('aircraft_takeoff_cost', css_class='large-4'),
                ),
                Fieldset(
                    _('References'),
                    HTML("<p>Get pilot and aircraft data from selected</p>"),
                    Row(
                        Column('pilot', css_class='large-6'),
                        Column('aircraft', css_class='large-6'),
                    ),
                )
            ),
            Fieldset(
                _('Invoice information'),
                Row(
                    Column('status', css_class='large-4'),
                    Column('date_created', css_class='large-4'),
                    Column('due_date', css_class='large-4'),
                ),
                Row(
                    Column('tax', css_class='large-3'),
                    Column('discount', css_class='large-3'),
                    Column('comment', css_class='large-6'),
                ),
            ),
        )


class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ('account', 'customer_name', 'customer_address', 'customer_zip', 'customer_city', 'customer',
                  'event_title', 'event_start', 'event_end', 'event', 'pilot_name', 'aircraft_mark', 'aircraft_type',
                  'passengers_number', 'aircraft_takeoff_cost', 'pilot', 'aircraft', 'status', 'date_created',
                  'due_date', 'tax', 'discount', 'comment')


    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset(
                _('Customer'),
                Row(
                    Column('customer_name', css_class='large-6'),
                ),
                Row(
                    Column('customer_address', css_class='large-4'),
                    Column('customer_zip', css_class='large-4'),
                    Column('customer_city', css_class='large-4'),
                ),
            ),
            Fieldset(
                _('Event'),
                Row(
                    Column('event_title', css_class='large-4'),
                    Column('event_start', css_class='large-4'),
                    Column('event_end', css_class='large-4'),
                ),
            ),
            Fieldset(
                _('Aircraft'),
                Row(
                    Column('pilot_name', css_class='large-4'),
                    Column('aircraft_mark', css_class='large-4'),
                    Column('aircraft_type', css_class='large-4'),
                ),
                Row(
                    Column('passengers_number', css_class='large-4'),
                    Column('aircraft_takeoff_cost', css_class='large-4'),
                ),
            ),
            Fieldset(
                _('Invoice information'),
                Row(
                    Column('status', css_class='large-4'),
                    Column('date_created', css_class='large-4'),
                    Column('due_date', css_class='large-4'),
                ),
                Row(
                    Column('tax', css_class='large-3'),
                    Column('discount', css_class='large-3'),
                    Column('comment', css_class='large-6'),
                ),
            ),
            ButtonHolder(
                Submit('submit', _('Save')),
            ),
        )

        super(InvoiceForm, self).__init__(*args, **kwargs)


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ('account', 'invoice', 'amount', 'received_by')

    def __init__(self, *args, **kwargs):
        super(ReceiptForm, self).__init__(*args, **kwargs)
        self.fields['account'].widget = forms.HiddenInput()
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                _('Receipt'),
                Field('account'),
                Row(
                    Column('invoice', css_class="large-4"),
                    Column('amount', css_class="large-4"),
                    Column('received_by', css_class="large-4"),
                ),
                ButtonHolder(
                    Submit('submit', 'Save', css_class='button')
                ),
            ),
        )
