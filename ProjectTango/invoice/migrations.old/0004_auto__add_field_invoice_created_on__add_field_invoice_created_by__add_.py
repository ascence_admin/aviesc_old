# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Invoice.created_on'
        db.add_column('invoice_invoice', 'created_on',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True, default=datetime.datetime(2014, 6, 18, 0, 0)),
                      keep_default=False)

        # Adding field 'Invoice.created_by'
        db.add_column('invoice_invoice', 'created_by',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Invoice.created_ip'
        db.add_column('invoice_invoice', 'created_ip',
                      self.gf('django.db.models.fields.CharField')(default='127.0.0.1', max_length=45),
                      keep_default=False)

        # Adding field 'Invoice.modified_on'
        db.add_column('invoice_invoice', 'modified_on',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 6, 18, 0, 0), auto_now=True, blank=True),
                      keep_default=False)

        # Adding field 'Invoice.modified_by'
        db.add_column('invoice_invoice', 'modified_by',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Invoice.modified_ip'
        db.add_column('invoice_invoice', 'modified_ip',
                      self.gf('django.db.models.fields.CharField')(default='127.0.0.1', max_length=45),
                      keep_default=False)

        # Adding field 'Invoice.account'
        db.add_column('invoice_invoice', 'account',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['core.Account']),
                      keep_default=False)

        # Adding field 'Receipt.created_on'
        db.add_column('invoice_receipt', 'created_on',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True, default=datetime.datetime(2014, 6, 18, 0, 0)),
                      keep_default=False)

        # Adding field 'Receipt.created_by'
        db.add_column('invoice_receipt', 'created_by',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Receipt.created_ip'
        db.add_column('invoice_receipt', 'created_ip',
                      self.gf('django.db.models.fields.CharField')(default='127.0.0.1', max_length=45),
                      keep_default=False)

        # Adding field 'Receipt.modified_on'
        db.add_column('invoice_receipt', 'modified_on',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 6, 18, 0, 0), auto_now=True, blank=True),
                      keep_default=False)

        # Adding field 'Receipt.modified_by'
        db.add_column('invoice_receipt', 'modified_by',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Receipt.modified_ip'
        db.add_column('invoice_receipt', 'modified_ip',
                      self.gf('django.db.models.fields.CharField')(default='127.0.0.1', max_length=45),
                      keep_default=False)

        # Adding field 'Receipt.account'
        db.add_column('invoice_receipt', 'account',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['core.Account']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Invoice.created_on'
        db.delete_column('invoice_invoice', 'created_on')

        # Deleting field 'Invoice.created_by'
        db.delete_column('invoice_invoice', 'created_by')

        # Deleting field 'Invoice.created_ip'
        db.delete_column('invoice_invoice', 'created_ip')

        # Deleting field 'Invoice.modified_on'
        db.delete_column('invoice_invoice', 'modified_on')

        # Deleting field 'Invoice.modified_by'
        db.delete_column('invoice_invoice', 'modified_by')

        # Deleting field 'Invoice.modified_ip'
        db.delete_column('invoice_invoice', 'modified_ip')

        # Deleting field 'Invoice.account'
        db.delete_column('invoice_invoice', 'account_id')

        # Deleting field 'Receipt.created_on'
        db.delete_column('invoice_receipt', 'created_on')

        # Deleting field 'Receipt.created_by'
        db.delete_column('invoice_receipt', 'created_by')

        # Deleting field 'Receipt.created_ip'
        db.delete_column('invoice_receipt', 'created_ip')

        # Deleting field 'Receipt.modified_on'
        db.delete_column('invoice_receipt', 'modified_on')

        # Deleting field 'Receipt.modified_by'
        db.delete_column('invoice_receipt', 'modified_by')

        # Deleting field 'Receipt.modified_ip'
        db.delete_column('invoice_receipt', 'modified_ip')

        # Deleting field 'Receipt.account'
        db.delete_column('invoice_receipt', 'account_id')


    models = {
        'aircraft.aircraft': {
            'Meta': {'_ormbases': ['aircraft.AircraftType'], 'object_name': 'Aircraft'},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '20'}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['aircraft.AircraftType']", 'primary_key': 'True'}),
            'calendar_colour': ('django.db.models.fields.CharField', [], {'default': "'#FFFFFF'", 'max_length': '7'}),
            'casa_record': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['aircraft.CasaAircraft']", 'blank': 'True', 'null': 'True'})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '18'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'airframe': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'coacata': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '50'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'gear': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '6'}),
            'manu': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '60'}),
            'mark': ('django.db.models.fields.CharField', [], {'primary_key': 'True', 'max_length': '3'}),
            'model': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'blank': 'True', 'max_length': '28'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regopname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '74'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '17'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'typecert': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '15'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'null': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Group']", 'blank': 'True', 'related_name': "'user_set'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True', 'related_name': "'user_set'"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'db_table': "'django_content_type'", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType'},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'forecast_ahead_days': ('django.db.models.fields.IntegerField', [], {'default': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'maint_work_end_hr': ('django.db.models.fields.IntegerField', [], {'default': '17'}),
            'maint_work_start_hr': ('django.db.models.fields.IntegerField', [], {'default': '7'}),
            'maint_work_wkend': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'schedule_rule_threshold': ('django.db.models.fields.FloatField', [], {'default': '0.9'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        },
        'customer.customer': {
            'Meta': {'ordering': "('name', 'company')", 'object_name': 'Customer'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'zipcode': ('django.db.models.fields.CharField', [], {'default': "''", 'blank': 'True', 'max_length': '5'})
        },
        'invoice.extras': {
            'Meta': {'object_name': 'Extras'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['invoice.Invoice']", 'related_name': "'invoice_extras'"}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'unit_price': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'})
        },
        'invoice.invoice': {
            'Meta': {'object_name': 'Invoice'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.SET_NULL', 'to': "orm['aircraft.Aircraft']", 'blank': 'True', 'null': 'True', 'related_name': "'invoice_aircraft'"}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'aircraft_takeoff_cost': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'max_digits': '17'}),
            'aircraft_type': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.SET_NULL', 'to': "orm['customer.Customer']", 'blank': 'True', 'null': 'True', 'related_name': "'invoice_customer'"}),
            'customer_address': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer_city': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer_zip': ('django.db.models.fields.CharField', [], {'default': "''", 'blank': 'True', 'max_length': '10'}),
            'date_created': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 6, 18, 0, 0)'}),
            'discount': ('django.db.models.fields.DecimalField', [], {'decimal_places': '2', 'default': '0.0', 'max_digits': '5'}),
            'due_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 6, 25, 0, 0)'}),
            'event': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'blank': 'True', 'related_name': "'invoice_event'", 'on_delete': 'models.SET_NULL', 'to': "orm['schedule.Event']", 'null': 'True'}),
            'event_end': ('django.db.models.fields.DateTimeField', [], {}),
            'event_start': ('django.db.models.fields.DateTimeField', [], {}),
            'event_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'passengers_number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'pilot': ('django.db.models.fields.related.ForeignKey', [], {'on_delete': 'models.SET_NULL', 'to': "orm['auth.User']", 'blank': 'True', 'null': 'True', 'related_name': "'invoice_pilot'"}),
            'pilot_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '128'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tax': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['invoice.Tax']"})
        },
        'invoice.receipt': {
            'Meta': {'object_name': 'Receipt'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'amount': ('django.db.models.fields.DecimalField', [], {'decimal_places': '4', 'default': '0.0', 'max_digits': '17'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['invoice.Invoice']", 'related_name': "'invoice_receipt'"}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'received_by': ('django.db.models.fields.IntegerField', [], {'max_length': '128'})
        },
        'invoice.tax': {
            'Meta': {'object_name': 'Tax'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'value': ('django.db.models.fields.DecimalField', [], {'decimal_places': '2', 'default': '10.0', 'max_digits': '5'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Calendar']", 'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['invoice']