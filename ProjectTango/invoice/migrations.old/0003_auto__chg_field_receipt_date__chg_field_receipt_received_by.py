# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Receipt.date'
        db.alter_column('invoice_receipt', 'date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Changing field 'Receipt.received_by'
        db.alter_column('invoice_receipt', 'received_by', self.gf('django.db.models.fields.IntegerField')(max_length=128))

    def backwards(self, orm):

        # Changing field 'Receipt.date'
        db.alter_column('invoice_receipt', 'date', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Receipt.received_by'
        db.alter_column('invoice_receipt', 'received_by', self.gf('django.db.models.fields.CharField')(max_length=128))

    models = {
        'aircraft.aircraft': {
            'Meta': {'_ormbases': ['aircraft.AircraftType'], 'object_name': 'Aircraft'},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True', 'db_index': 'True'}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['aircraft.AircraftType']", 'primary_key': 'True'}),
            'calendar_colour': ('django.db.models.fields.CharField', [], {'default': "'#FFFFFF'", 'max_length': '7'}),
            'casa_record': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['aircraft.CasaAircraft']", 'blank': 'True'})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4', 'default': '0.0'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4', 'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'default': '0', 'blank': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4', 'default': '0.0'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'max_length': '18', 'blank': 'True'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'airframe': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'coacata': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'max_length': '14', 'blank': 'True'}),
            'gear': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'max_length': '6', 'blank': 'True', 'db_index': 'True'}),
            'manu': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True', 'db_index': 'True'}),
            'mark': ('django.db.models.fields.CharField', [], {'max_length': '3', 'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '28', 'blank': 'True', 'db_index': 'True'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'regopname': ('django.db.models.fields.CharField', [], {'max_length': '74', 'blank': 'True'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'max_length': '39', 'blank': 'True'}),
            'regtype': ('django.db.models.fields.CharField', [], {'max_length': '17', 'blank': 'True'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'typecert': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_set'", 'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_set'", 'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'object_name': 'ContentType', 'ordering': "('name',)", 'db_table': "'django_content_type'", 'unique_together': "(('app_label', 'model'),)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'forecast_ahead_days': ('django.db.models.fields.IntegerField', [], {'default': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'maint_work_end_hr': ('django.db.models.fields.IntegerField', [], {'default': '17'}),
            'maint_work_start_hr': ('django.db.models.fields.IntegerField', [], {'default': '7'}),
            'maint_work_wkend': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'schedule_rule_threshold': ('django.db.models.fields.FloatField', [], {'default': '0.9'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'customer.customer': {
            'Meta': {'object_name': 'Customer', 'ordering': "('name', 'company')"},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'zipcode': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '5', 'blank': 'True'})
        },
        'invoice.extras': {
            'Meta': {'object_name': 'Extras'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'invoice_extras'", 'to': "orm['invoice.Invoice']"}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'unit_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4', 'default': '0.0'})
        },
        'invoice.invoice': {
            'Meta': {'object_name': 'Invoice'},
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'related_name': "'invoice_aircraft'", 'to': "orm['aircraft.Aircraft']", 'blank': 'True', 'on_delete': 'models.SET_NULL'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'aircraft_takeoff_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4'}),
            'aircraft_type': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'related_name': "'invoice_customer'", 'to': "orm['customer.Customer']", 'blank': 'True', 'on_delete': 'models.SET_NULL'}),
            'customer_address': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer_city': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer_zip': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 6, 16, 0, 0)'}),
            'discount': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'default': '0.0'}),
            'due_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 6, 23, 0, 0)'}),
            'event': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'blank': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['schedule.Event']", 'related_name': "'invoice_event'", 'null': 'True'}),
            'event_end': ('django.db.models.fields.DateTimeField', [], {}),
            'event_start': ('django.db.models.fields.DateTimeField', [], {}),
            'event_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'passengers_number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'pilot': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'related_name': "'invoice_pilot'", 'to': "orm['auth.User']", 'blank': 'True', 'on_delete': 'models.SET_NULL'}),
            'pilot_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tax': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['invoice.Tax']"})
        },
        'invoice.receipt': {
            'Meta': {'object_name': 'Receipt'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4', 'default': '0.0'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'invoice_receipt'", 'to': "orm['invoice.Invoice']"}),
            'received_by': ('django.db.models.fields.IntegerField', [], {'max_length': '128'})
        },
        'invoice.tax': {
            'Meta': {'object_name': 'Tax'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'default': '10.0'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Calendar']", 'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['invoice']