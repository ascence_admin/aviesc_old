# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Invoice'
        db.create_table('invoice_invoice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('customer_address', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('customer_zip', self.gf('django.db.models.fields.CharField')(blank=True, max_length=10, default='')),
            ('customer_city', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('event_title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('event_start', self.gf('django.db.models.fields.DateTimeField')()),
            ('event_end', self.gf('django.db.models.fields.DateTimeField')()),
            ('aircraft_mark', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('aircraft_takeoff_cost', self.gf('django.db.models.fields.DecimalField')(max_digits=17, decimal_places=4)),
            ('aircraft_type', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('pilot_name', self.gf('django.db.models.fields.CharField')(blank=True, max_length=128)),
            ('date_created', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 6, 6, 0, 0))),
            ('due_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2014, 6, 13, 0, 0))),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('discount', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2, default=0.0)),
            ('passengers_number', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('comment', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('tax', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['invoice.Tax'])),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(null=True, blank=True, related_name='invoice_customer', to=orm['customer.Customer'], on_delete=models.SET_NULL)),
            ('event', self.gf('django.db.models.fields.related.OneToOneField')(null=True, blank=True, related_name='invoice_event', to=orm['schedule.Event'], unique=True, on_delete=models.SET_NULL)),
            ('pilot', self.gf('django.db.models.fields.related.ForeignKey')(null=True, blank=True, related_name='invoice_pilot', to=orm['auth.User'], on_delete=models.SET_NULL)),
            ('aircraft', self.gf('django.db.models.fields.related.ForeignKey')(null=True, blank=True, related_name='invoice_aircraft', to=orm['aircraft.Aircraft'], on_delete=models.SET_NULL)),
        ))
        db.send_create_signal('invoice', ['Invoice'])

        # Adding model 'Extras'
        db.create_table('invoice_extras', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('invoice', self.gf('django.db.models.fields.related.ForeignKey')(related_name='invoice_extras', to=orm['invoice.Invoice'])),
            ('quantity', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('unit_price', self.gf('django.db.models.fields.DecimalField')(max_digits=17, decimal_places=4, default=0.0)),
        ))
        db.send_create_signal('invoice', ['Extras'])

        # Adding model 'Tax'
        db.create_table('invoice_tax', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2, default=10.0)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('invoice', ['Tax'])


    def backwards(self, orm):
        # Deleting model 'Invoice'
        db.delete_table('invoice_invoice')

        # Deleting model 'Extras'
        db.delete_table('invoice_extras')

        # Deleting model 'Tax'
        db.delete_table('invoice_tax')


    models = {
        'aircraft.aircraft': {
            'Meta': {'_ormbases': ['aircraft.AircraftType'], 'object_name': 'Aircraft'},
            'aircraft_description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20', 'db_index': 'True'}),
            'aircrafttype_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['aircraft.AircraftType']", 'primary_key': 'True', 'unique': 'True'}),
            'calendar_colour': ('django.db.models.fields.CharField', [], {'max_length': '7', 'default': "'#FFFFFF'"}),
            'casa_record': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'to': "orm['aircraft.CasaAircraft']"})
        },
        'aircraft.aircrafttype': {
            'Meta': {'object_name': 'AircraftType'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'aircraft_type_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'avg_speed': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4', 'default': '0.0'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'hourly_rate': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4', 'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_pax': ('django.db.models.fields.IntegerField', [], {'blank': 'True', 'default': '0'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'takeoff_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4', 'default': '0.0'})
        },
        'aircraft.casaaircraft': {
            'Meta': {'object_name': 'CasaAircraft'},
            'aircraftserial': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '18'}),
            'aircrafttype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'airframe': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'coacata': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'coacatc': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '20'}),
            'countrymanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'datefirstreg': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'engmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60'}),
            'engmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '50'}),
            'engnum': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'engtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'fueltype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '14'}),
            'gear': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'icaotypedesig': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '6', 'db_index': 'True'}),
            'manu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60', 'db_index': 'True'}),
            'mark': ('django.db.models.fields.CharField', [], {'max_length': '3', 'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '28', 'db_index': 'True'}),
            'mtow': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'propmanu': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'propmodel': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'regexpirydate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regholdcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regholdcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regholdname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255'}),
            'regholdpostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regholdstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regholdsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regopadd1': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopadd2': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '40'}),
            'regopcommdate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'regopcountry': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100'}),
            'regopname': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '74'}),
            'regoppostcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '11'}),
            'regopstate': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10'}),
            'regopsuburb': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '39'}),
            'regtype': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '17'}),
            'suspenddate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'suspendstatus': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'typecert': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '15'}),
            'yearmanu': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'blank': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'user_set'", 'to': "orm['auth.Group']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'user_set'", 'to': "orm['auth.Permission']", 'symmetrical': 'False'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '30', 'unique': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'unique_together': "(('app_label', 'model'),)", 'db_table': "'django_content_type'", 'object_name': 'ContentType', 'ordering': "('name',)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'forecast_ahead_days': ('django.db.models.fields.IntegerField', [], {'default': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'maint_work_end_hr': ('django.db.models.fields.IntegerField', [], {'default': '17'}),
            'maint_work_start_hr': ('django.db.models.fields.IntegerField', [], {'default': '7'}),
            'maint_work_wkend': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'schedule_rule_threshold': ('django.db.models.fields.FloatField', [], {'default': '0.9'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'customer.customer': {
            'Meta': {'object_name': 'Customer'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'zipcode': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '5', 'default': "''"})
        },
        'invoice.extras': {
            'Meta': {'object_name': 'Extras'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'invoice_extras'", 'to': "orm['invoice.Invoice']"}),
            'quantity': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'unit_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4', 'default': '0.0'})
        },
        'invoice.invoice': {
            'Meta': {'object_name': 'Invoice'},
            'aircraft': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'related_name': "'invoice_aircraft'", 'to': "orm['aircraft.Aircraft']", 'on_delete': 'models.SET_NULL'}),
            'aircraft_mark': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'aircraft_takeoff_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '17', 'decimal_places': '4'}),
            'aircraft_type': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'related_name': "'invoice_customer'", 'to': "orm['customer.Customer']", 'on_delete': 'models.SET_NULL'}),
            'customer_address': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer_city': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'customer_zip': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '10', 'default': "''"}),
            'date_created': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 6, 6, 0, 0)'}),
            'discount': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'default': '0.0'}),
            'due_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2014, 6, 13, 0, 0)'}),
            'event': ('django.db.models.fields.related.OneToOneField', [], {'null': 'True', 'blank': 'True', 'related_name': "'invoice_event'", 'to': "orm['schedule.Event']", 'unique': 'True', 'on_delete': 'models.SET_NULL'}),
            'event_end': ('django.db.models.fields.DateTimeField', [], {}),
            'event_start': ('django.db.models.fields.DateTimeField', [], {}),
            'event_title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'passengers_number': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'pilot': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'related_name': "'invoice_pilot'", 'to': "orm['auth.User']", 'on_delete': 'models.SET_NULL'}),
            'pilot_name': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '128'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'tax': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['invoice.Tax']"})
        },
        'invoice.tax': {
            'Meta': {'object_name': 'Tax'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'default': '10.0'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['schedule.Calendar']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['invoice']