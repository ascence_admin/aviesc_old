# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from datetime import date, timedelta

from aircraft.models.aircraft import Aircraft, AircraftType
from customer.models import Customer
from schedule.models import Event
from core.models import AccountModel


def _date_created_today():
    return date.today()


def _date_due_week():
    return date.today() + timedelta(days=7)


class Invoice(AccountModel):
    """
        Invoice for an event
        :Invoice status:
        - Entered   -> Invoice created
        - Approved  -> Invoice delivered to the customer and it is ready for paid
        - Cancel    -> Invoice was rejected
        - Paid      -> Invoice was paid
    """
    STATUS_ENTERED = 0
    STATUS_APPROVED = 1
    STATUS_CANCEL = 2
    STATUS_PAID = 3
    STATUS = (
        (STATUS_ENTERED, _('Entered')),
        (STATUS_APPROVED, _('Approved')),
        (STATUS_CANCEL, _('Cancel')),
        (STATUS_PAID, _('Paid')),
    )

    customer_name = models.CharField(max_length=64)
    customer_address = models.CharField(max_length=64)
    customer_zip = models.CharField(max_length=10,
                                    blank=True,
                                    default='')
    customer_city = models.CharField(max_length=64)

    event_title = models.CharField(max_length=255)
    event_start = models.DateTimeField()
    event_end = models.DateTimeField()

    aircraft_mark = models.CharField(max_length=20)
    aircraft_takeoff_cost = models.DecimalField(decimal_places=4,
                                                max_digits=17)
    aircraft_type = models.CharField(max_length=255)
    pilot_name = models.CharField(max_length=128, blank=True)

    date_created = models.DateField(default=_date_created_today)
    due_date = models.DateField(verbose_name=_('Payment due date'), default=_date_due_week)
    status = models.IntegerField(choices=STATUS,
                                 default=STATUS_ENTERED)
    discount = models.DecimalField(max_digits=5,
                                   decimal_places=2,
                                   default=0.00)
    passengers_number = models.PositiveIntegerField(default=0)
    comment = models.TextField(blank=True,
                               null=True)
    tax = models.ForeignKey('Tax')

    # references if exist
    customer = models.ForeignKey(Customer,
                                 blank=True,
                                 null=True,
                                 related_name="invoice_customer",
                                 on_delete=models.SET_NULL)
    event = models.OneToOneField(Event,
                                 blank=True,
                                 null=True,
                                 related_name="invoice_event",
                                 on_delete=models.SET_NULL)
    pilot = models.ForeignKey(User,
                              blank=True,
                              null=True,
                              related_name="invoice_pilot",
                              on_delete=models.SET_NULL)
    aircraft = models.ForeignKey(Aircraft,
                                 blank=True,
                                 null=True,
                                 related_name="invoice_aircraft",
                                 on_delete=models.SET_NULL)

    def __str__(self):
        return "#{id:06d}".format(id=self.id)

    def extras_total_no_tax(self):
        return sum(extra.total() for extra in self.invoice_extras.all())

    def total_no_tax(self):
        return self.extras_total_no_tax() + self.aircraft_takeoff_cost

    def total_no_tax_discount(self):
        return self.total_no_tax() - (self.total_no_tax() * self.discount / 100) if self.discount != 0 \
            else self.total_no_tax()

    def total(self):
        return self.total_no_tax_discount() + ((self.tax.value / 100) * self.total_no_tax_discount())

    def amount_outstanding(self):
        return self.total() - sum(receipt.amount for receipt in self.invoice_receipt.all())

    def get_absolute_url(self):
        return reverse('invoice-detail', kwargs={'pk': self.pk})


class Extras(models.Model):
    invoice = models.ForeignKey(Invoice, related_name='invoice_extras')
    quantity = models.PositiveIntegerField(default=1)
    description = models.TextField()
    unit_price = models.DecimalField(max_digits=17,
                                     decimal_places=4,
                                     default=0.00)

    def __str__(self):
        return "{0} x{1}".format(self.description, self.quantity)

    def total(self):
        return self.quantity * self.unit_price


class Tax(models.Model):
    name = models.CharField(max_length=64)
    value = models.DecimalField(max_digits=5,
                                decimal_places=2,
                                default=10.00)
    active = models.BooleanField(default=False)

    def __str__(self):
        return "{0} - {1}%".format(self.name, self.value)


class Receipt(AccountModel):
    PAYMENT_CASH = 0
    PAYMENT_EFTPOS = 1
    PAYMENT_CREDITCARD = 2
    PAYMENT_ONLINE = 3
    PAYMENT_METHOD = (
        (PAYMENT_CASH, _('Cash')),
        (PAYMENT_EFTPOS, _('Eftpos')),
        (PAYMENT_CREDITCARD, _('Credit Card')),
        (PAYMENT_ONLINE, _('Online')),
    )

    invoice = models.ForeignKey(Invoice, related_name='invoice_receipt')
    date = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=17,
                                 decimal_places=4,
                                 default=0.00)
    received_by = models.IntegerField(max_length=128, choices=PAYMENT_METHOD)

    def __str__(self):
        return "{0}, {1} [{2}]".format(self.invoice, self.date, self.amount)

    def get_absolute_url(self):
        return reverse('receipt-detail', kwargs={'pk': self.pk})