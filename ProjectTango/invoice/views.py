from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.utils.safestring import mark_safe
from django.contrib.messages.views import SuccessMessageMixin
from django.forms.formsets import formset_factory
from django.http import HttpResponseRedirect
from django.forms.models import modelformset_factory
from django.template.loader import get_template
from django.shortcuts import HttpResponse, RequestContext, get_object_or_404
from django.conf import settings

from weasyprint import HTML, CSS, default_url_fetcher

from invoice.models import Invoice, Extras, Receipt
from invoice.forms import InvoiceCreateForm, ExtrasCreateForm, ExtrasUpdateForm, ReceiptForm
from core.constructs import ActionList, DetailList
from schedule.models import Event


class InvoiceList(ListView):
    model = Invoice
    template_name = 'invoice/invoice_list.html'

    def get_queryset(self):
        return Invoice.objects.all()

    def get_context_data(self, **kwargs):
        # Add actions to the left hand actions
        context = super(InvoiceList, self).get_context_data(**kwargs)
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('invoice-list', kwargs={}), 'List Invoice', True)
        actions.insertAction(reverse('invoice-create', kwargs={}), 'Add new')
        return context


class InvoiceDetail(DetailView):
    model = Invoice

    def get_queryset(self):
        return Invoice.objects.filter()

    def get_context_data(self, **kwargs):
        context = super(InvoiceDetail, self).get_context_data(**kwargs)
        invoice = context['object']

        # Add an "Update this" link to the left hand actions
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('invoice-list', kwargs={}), 'List Invoice')
        actions.insertAction(reverse('invoice-update', kwargs={'pk': self.object.pk}), 'Update', True)
        # Display the audit fields on the left as well
        details = DetailList.GetLeftDetailsForContext(context)
        details.insertDetail("Invoice Status", self.object.get_status_display)
        details.insertDetail("Payment due date", self.object.due_date)
        details.insertDetail('', mark_safe(
            '<button class="button tiny" id="send_email_invoice">Send invoice to customer</button>'))
        return context


class InvoiceCreate(CreateView):
    model = Invoice
    template_name = 'invoice/invoice_create.html'
    form_class = InvoiceCreateForm
    success_message = "Invoice was added successfully"

    def get(self, request, *args, **kwargs):
        user_account = request.user.userdetails.account

        extrasFormSet = formset_factory(ExtrasCreateForm, extra=5)
        try:
            event = Event.objects.get(id=int(self.request.GET.get('event', 0)))
            kwargs['form'] = InvoiceCreateForm(
                initial={'account': user_account, 'event': event, 'event_start': event.start, 'event_end': event.end,
                         'event_title': event.title})
        except Event.DoesNotExist:
            kwargs['form'] = InvoiceCreateForm(initial={'account': user_account, })

        kwargs['extras_formset'] = extrasFormSet()

        return self.render_to_response(self.get_context_data(**kwargs))

    def post(self, request, *args, **kwargs):

        extrasFormSet = formset_factory(ExtrasCreateForm, extra=5)
        if self.request.POST:
            form = InvoiceCreateForm(self.request.POST)
            extras_formset = extrasFormSet(self.request.POST)
            if form.is_valid() and extras_formset.is_valid():
                invoice = form.save()
                for data in extras_formset.cleaned_data:
                    if data:
                        extra = Extras(invoice=invoice, quantity=data['quantity'], unit_price=data['unit_price'],
                                       description=data['description'])
                        extra.save()
                messages.success(self.request, self.success_message)
                return HttpResponseRedirect(reverse('invoice-detail', args={invoice.id, }))
            else:
                print(form.errors)
                kwargs['form'] = form
                kwargs['extras_formset'] = extras_formset
                context = self.get_context_data(**kwargs)
                return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = kwargs
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('invoice-list', kwargs={}), 'List Invoice')
        return context


class InvoiceUpdate(SuccessMessageMixin, UpdateView):
    model = Invoice
    template_name = 'invoice/invoice_create.html'
    form_class = InvoiceCreateForm
    success_url = '.'
    success_message = "Invoice was updated successfully"

    def get_queryset(self):
        return Invoice.objects.filter()

    def form_valid(self, form):
        response = super(SuccessMessageMixin, self).form_valid(form)
        messages.success(self.request, self.success_message)
        return response

    def post(self, request, *args, **kwargs):
        extrasFormSet = modelformset_factory(Extras, form=ExtrasUpdateForm, extra=4)
        extras_formset = extrasFormSet(self.request.POST)

        if extras_formset.is_valid():
            extras_formset.save()

        return super(InvoiceUpdate, self).post(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        extrasFormSet = modelformset_factory(Extras, form=ExtrasUpdateForm, extra=4)

        # Add actions to the left hand actions
        context = super(InvoiceUpdate, self).get_context_data(**kwargs)
        formset = extrasFormSet(queryset=Extras.objects.filter(invoice=context['object']),
                                initial=[{'invoice': context['object']}])
        user_account = self.request.user.userdetails.account
        kwargs['form'] = InvoiceCreateForm(initial={'account': user_account, })
        actions = ActionList.GetActionsForContext(context)
        context['extras_formset'] = formset
        actions.insertAction(reverse('invoice-list', kwargs={}), 'List Invoice')
        actions.insertAction(reverse('invoice-detail', kwargs={'pk': context['object'].pk}), 'Back', True)
        return context


class ReceiptList(ListView):
    model = Receipt
    template_name = 'invoice/receipt_list.html'

    def get_queryset(self):
        return Receipt.objects.all()

    def get_context_data(self, **kwargs):
        # Add actions to the left hand actions
        context = super(ReceiptList, self).get_context_data(**kwargs)
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('receipt-list', kwargs={}), 'List Receipt', True)
        actions.insertAction(reverse('receipt-create', kwargs={}), 'Add new')
        return context


class ReceiptDetail(DetailView):
    model = Receipt

    def get_queryset(self):
        return Receipt.objects.filter()

    def get_context_data(self, **kwargs):
        context = super(ReceiptDetail, self).get_context_data(**kwargs)
        receipt = context['object']

        # Add an "Update this" link to the left hand actions
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('receipt-list', kwargs={}), 'List Receipt')
        actions.insertAction(reverse('receipt-update', kwargs={'pk': self.object.pk}), 'Update', True)
        # Display the audit fields on the left as well
        details = DetailList.GetLeftDetailsForContext(context)

        return context


class ReceiptCreate(SuccessMessageMixin, CreateView):
    model = Receipt
    template_name = 'invoice/receipt_create.html'
    form_class = ReceiptForm
    success_message = "Receipt was added successfully"

    def get(self, request, *args, **kwargs):
        invoice_id = request.GET.get('invoice', None)
        user_account = self.request.user.userdetails.account
        if invoice_id:
            try:
                invoice = Invoice.objects.get(id=invoice_id)
            except Invoice.DoesNotExist:
                return self.render_to_response(self.get_context_data(**kwargs))
            kwargs['form'] = ReceiptForm(initial={'invoice': invoice, 'account': user_account, })
        else:
            kwargs['form'] = ReceiptForm(initial={'account': user_account, })
        return self.render_to_response(self.get_context_data(**kwargs))

    def form_valid(self, form):
        messages.success(self.request, self.success_message)
        return super(SuccessMessageMixin, self).form_valid(form)

    def get_context_data(self, **kwargs):
        # Add actions to the left hand actions

        context = kwargs
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('receipt-list', kwargs={}), 'List Receipt')
        return context


class ReceiptUpdate(SuccessMessageMixin, UpdateView):
    model = Receipt
    template_name = 'invoice/receipt_create.html'
    form_class = ReceiptForm
    success_message = "Receipt was updated successfully"

    def get_queryset(self):
        return Receipt.objects.all()

    def form_valid(self, form):
        response = super(SuccessMessageMixin, self).form_valid(form)
        messages.success(self.request, self.success_message)
        return response

    def get_context_data(self, **kwargs):
        # Add actions to the left hand actions
        user_account = self.request.user.userdetails.account
        context = super(ReceiptUpdate, self).get_context_data(**kwargs)
        kwargs['form'] = ReceiptForm(initial={'account': user_account, })
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('receipt-list', kwargs={}), 'List Receipt')
        actions.insertAction(reverse('receipt-detail', kwargs={'pk': context['object'].pk}), 'Back', True)
        return context


def invoice_to_pdf(request, invoice_pk):
    invoice = get_object_or_404(Invoice, pk=invoice_pk)
    template = get_template("invoice/pdf/invoice.html")
    context = {'invoice': invoice}
    html = template.render(RequestContext(request, context))
    response = HttpResponse(content_type="application/pdf")
    HTML(string=html, base_url=settings.PDF_ASSETS).write_pdf(response)
    return response