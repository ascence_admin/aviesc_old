import smtplib

from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

from celery.utils.log import get_task_logger

from ProjectTango.celery_settings import celery
from invoice.models import Invoice


logger = get_task_logger(__name__)


@celery.task
def send_email_invoice_to_customer(invoice_id):
    """
    Send email with invoice with given id to customer if customer has email.
    :usage: invoice_tasks.send_email_invoice_to_customer.delay(invoice_id)
    return True if task completed successfully else False
    """
    try:
        invoice = Invoice.objects.get(id=invoice_id)
    except Invoice.DoesNotExist:
        logger.error('Invoice (id={0}) does not exist. Task rejected.'.format(invoice_id))
        return False

    if not invoice.customer:
        logger.error('Email can not be sent because there is no customer linked to invoice.')
        return False

    if not invoice.customer.email:
        logger.error('Email can not be sent because customer email is empty.')
        return False

    context = Context({
        'invoice': invoice,
    })

    subject, from_email = 'Invoice for an Event', 'from@example.com'
    html_content = get_template('invoice/email/invoice_to_customer.html').render(context)
    text_content = get_template('invoice/email/invoice_to_customer.txt').render(context)

    msg = EmailMultiAlternatives(subject, text_content, from_email, [invoice.customer.email, ])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
    return True
