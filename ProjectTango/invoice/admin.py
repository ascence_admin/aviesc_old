from django.contrib import admin
from invoice.models import Tax, Extras, Invoice, Receipt


class ExtrasInline(admin.TabularInline):
    model = Extras
    extra = 1


class InvoiceAdmin(admin.ModelAdmin):
    inlines = [
        ExtrasInline,
    ]


admin.site.register(Invoice, InvoiceAdmin)
admin.site.register([Tax, Receipt])
