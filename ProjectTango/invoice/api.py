from django.utils.translation import ugettext as _

from rest_framework.decorators import api_view
from rest_framework.response import Response

from invoice import tasks


@api_view(['GET'])
def send_email_invoice_to_customer(request, order_id):
    result = tasks.send_email_invoice_to_customer.delay(order_id)
    result.get()  # wait for task
    if result.result is False:
        return Response({"message": _("Email can not be sent because customer email is empty.")})
    return Response({"message": _("Email sent successfully")})