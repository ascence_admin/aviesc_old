# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0001_initial'),
        ('invoice', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='event',
            field=models.OneToOneField(related_name='invoice_event', to='schedule.Event', on_delete=django.db.models.deletion.SET_NULL, blank=True, null=True),
        ),
        migrations.AddField(
            model_name='invoice',
            name='pilot',
            field=models.ForeignKey(related_name='invoice_pilot', to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.SET_NULL, blank=True, null=True),
        ),
        migrations.AddField(
            model_name='invoice',
            name='tax',
            field=models.ForeignKey(to='invoice.Tax'),
        ),
        migrations.AddField(
            model_name='extras',
            name='invoice',
            field=models.ForeignKey(related_name='invoice_extras', to='invoice.Invoice'),
        ),
    ]
