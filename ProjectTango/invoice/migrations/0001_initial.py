# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('aircraft', '0001_initial'),
        ('core', '0001_initial'),
        ('customer', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Extras',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.PositiveIntegerField(default=1)),
                ('description', models.TextField()),
                ('unit_price', models.DecimalField(max_digits=17, default=0.0, decimal_places=4)),
            ],
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('customer_name', models.CharField(max_length=64)),
                ('customer_address', models.CharField(max_length=64)),
                ('customer_zip', models.CharField(blank=True, max_length=10, default='')),
                ('customer_city', models.CharField(max_length=64)),
                ('event_title', models.CharField(max_length=255)),
                ('event_start', models.DateTimeField()),
                ('event_end', models.DateTimeField()),
                ('aircraft_mark', models.CharField(max_length=20)),
                ('aircraft_takeoff_cost', models.DecimalField(max_digits=17, decimal_places=4)),
                ('aircraft_type', models.CharField(max_length=255)),
                ('pilot_name', models.CharField(blank=True, max_length=128)),
                ('date_created', models.DateField(default=datetime.date(2015, 7, 22))),
                ('due_date', models.DateField(verbose_name='Payment due date', default=datetime.date(2015, 7, 29))),
                ('status', models.IntegerField(choices=[(0, 'Entered'), (1, 'Approved'), (2, 'Cancel'), (3, 'Paid')], default=0)),
                ('discount', models.DecimalField(max_digits=5, default=0.0, decimal_places=2)),
                ('passengers_number', models.PositiveIntegerField(default=0)),
                ('comment', models.TextField(blank=True, null=True)),
                ('account', models.ForeignKey(to='core.Account')),
                ('aircraft', models.ForeignKey(related_name='invoice_aircraft', to='aircraft.Aircraft', on_delete=django.db.models.deletion.SET_NULL, blank=True, null=True)),
                ('customer', models.ForeignKey(related_name='invoice_customer', to='customer.Customer', on_delete=django.db.models.deletion.SET_NULL, blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Receipt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('amount', models.DecimalField(max_digits=17, default=0.0, decimal_places=4)),
                ('received_by', models.IntegerField(max_length=128, choices=[(0, 'Cash'), (1, 'Eftpos'), (2, 'Credit Card'), (3, 'Online')])),
                ('account', models.ForeignKey(to='core.Account')),
                ('invoice', models.ForeignKey(related_name='invoice_receipt', to='invoice.Invoice')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Tax',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('value', models.DecimalField(max_digits=5, default=10.0, decimal_places=2)),
                ('active', models.BooleanField(default=False)),
            ],
        ),
    ]
