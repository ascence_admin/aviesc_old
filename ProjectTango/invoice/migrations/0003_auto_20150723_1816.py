# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import invoice.models


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0002_auto_20150722_2333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='date_created',
            field=models.DateField(default=invoice.models._date_created_today),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='due_date',
            field=models.DateField(verbose_name='Payment due date', default=invoice.models._date_due_week),
        ),
    ]
