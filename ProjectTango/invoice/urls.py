from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required, permission_required

from invoice import views, api

urlpatterns = patterns("",
                       # Receipt
                       url(r"^receipt/$", login_required(views.ReceiptList.as_view()), name="receipt-list"),
                       url(r"^receipt/add/$",
                           permission_required('aircraft.add_aircraft')(views.ReceiptCreate.as_view()),
                           name="receipt-create"),
                       url(r'^receipt/(?P<pk>[-_\w]+)/$', login_required(views.ReceiptDetail.as_view()),
                           name='receipt-detail'),
                       url(r"^receipt/(?P<pk>[-_\w]+)/update/$",
                           permission_required('aircraft.change_aircraft')(views.ReceiptUpdate.as_view()),
                           name="receipt-update"),

                       # Invoice
                       url(r"^$", login_required(views.InvoiceList.as_view()), name="invoice-list"),
                       url(r"^add/$", permission_required('aircraft.add_aircraft')(views.InvoiceCreate.as_view()),
                           name="invoice-create"),
                       url(r'^(?P<pk>[-_\w]+)/$', login_required(views.InvoiceDetail.as_view()), name='invoice-detail'),
                       url(r"^(?P<pk>[-_\w]+)/update/$",
                           permission_required('aircraft.change_aircraft')(views.InvoiceUpdate.as_view()),
                           name="invoice-update"),
                       # PDF
                       url(r"^(?P<invoice_pk>[-_\w]+)/invoice.pdf$", views.invoice_to_pdf),

                       # API
                       url(r"^api/prepare_send_email_invoice/(?P<order_id>[0-9]+)",
                           api.send_email_invoice_to_customer,
                           name='api.send_email_invoice_to_customer'),
)