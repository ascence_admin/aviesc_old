import datetime
import os
from dateutil import rrule

from django.test import TestCase
from django.core.urlresolvers import reverse
from django.utils import timezone

from core.tests import AuditableTestCase
from core.models import Account

from aircraft.models import Aircraft

from schedule.models import Event, Rule, Occurrence, Calendar
from schedule.periods import Period, Month, Day
from schedule.utils import EventListManager
from schedule.forms import EventForm

class TestEventRecurrenceForm(AuditableTestCase):
    def setUp(self):
        super().setUp()

        self.start_dt = datetime.datetime.now().replace(hour=9, minute=0, second=0, microsecond=0)

        account_details = {
                                'account_name' : 'Test Account',
                                'inactive' : False,
                                'start_dt' : datetime.date(2013, 1, 1),
                                }

        self.test_account = Account(**account_details)
        self.test_account.save()

        self.aircraft_type_details = {
                                      'account' : self.test_account,
                                      'aircraft_type_name' : 'DH-82A Fleet',
                                      'hourly_rate' : 120.00,
                                      'max_pax' : 10,
                                      'avg_speed' : 120.0,
                                      'takeoff_cost' : 10.0,
                                      }

        self.aircraft_details = dict({
                                 'aircraft_description' : 'a new test aircraft that we can start flying!',
                                 },**self.aircraft_type_details)
        self.aircraft = Aircraft(**self.aircraft_details)
        self.aircraft.save()

        self.cal = Calendar.objects.get_calendar_for_object(self.aircraft)

    def test_daily_rule(self):

        #### Every 2 days ####
        data = {'frequency':'DAILY',
                'interval':2,
                'count':20,
                'title':'Test Rule Event',
                'start_0':self.start_dt.date(),
                'start_1':self.start_dt.time(),
                'end_0':self.start_dt.date(),
                'end_1':(self.start_dt + datetime.timedelta(hours=1)).time(),
                'description':'Test event recurrence',
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }

        form = EventForm(data=data, hour24=True)
        if not form.is_valid():
            print('Form Errors: %s' % form.errors)
            print('Non-Form Errors: %s' % form.non_field_errors())
            print(form.instance)
            self.assertTrue(False, "Every 2nd day Form does not validate")

        event = form.save(commit=False)
        event.calendar = self.cal
        event.save()

        expected = []
        for i in range(data['count']):
            expected.append(self.start_dt + datetime.timedelta(days=i*data['interval']))
        rule = form.instance.rule

        self.assertEqual(expected, rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365),True), "Every 2nd day failed")

        #### Every Weekday ####
        data = {'frequency':'DAILY',
                'daily_option':'wd',
                'count':40,
                'title':'Test Rule Event',
                'start_0':self.start_dt.date(),
                'start_1':self.start_dt.time(),
                'end_0':self.start_dt.date(),
                'end_1':(self.start_dt + datetime.timedelta(hours=1)).time(),
                'description':'Test event recurrence',
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }
        form = EventForm(data=data, hour24=True)
        if not form.is_valid():
            print('Form Errors: %s' % form.errors)
            print('Non-Form Errors: %s' % form.non_field_errors())
            self.assertTrue(False, "Every Weekday Form does not validate")

        event = form.save(commit=False)
        event.calendar = self.cal
        event.save()

        expected = [self._get_next_weekday(self.start_dt)]
        for i in range(1, data['count']):
            expected.append(self._get_next_weekday(expected[i-1]+datetime.timedelta(days=1)))
        rule = form.instance.rule
        self.assertEqual(expected, rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True), "Every Weekday failed")

    def test_weekly_rule(self):

        #### Every x weeks on certain days
        data = {'frequency':'WEEKLY',
                'interval':2,
                'count':8,
                'days_tu':True,
                'days_th':True,
                'title':'Test Rule Event',
                'start_0':self.start_dt.date(),
                'start_1':self.start_dt.time(),
                'end_0':self.start_dt.date(),
                'end_1':(self.start_dt + datetime.timedelta(hours=1)).time(),
                'description':'Test event recurrence',
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }

        form = EventForm(data=data)
        if not form.is_valid():
            print('Form Errors: %s' % form.errors)
            print('Non-Form Errors: %s' % form.non_field_errors())
            self.assertTrue(False, "Form does not validate")

        event = form.save(commit=False)
        event.calendar = self.cal
        event.save()

        #We're not testing rrule, just the Event object's ability to store and use it accurately
        #So we'll use rrule here to also come up with expected values
        expected_rule = rrule.rrule(rrule.WEEKLY, dtstart=self.start_dt,interval=data['interval'],count=data['count'],
                                    byweekday=(rrule.TU, rrule.TH))

        rule = event.rule
        self.assertEqual(expected_rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , "Every 2nd week on Tu & Th failed")

    def test_monthly_rule(self):
        #### Day x of every y months
        data = {'frequency':'MONTHLY',
                'interval':3,
                'count':3,
                'monthly_option':'c',
                'day_num_of_month':3,
                'title':'Test Rule Event',
                'start_0':self.start_dt.date(),
                'start_1':self.start_dt.time(),
                'end_0':self.start_dt.date(),
                'end_1':(self.start_dt + datetime.timedelta(hours=1)).time(),
                'description':'Test event recurrence',
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }

        form = EventForm(data=data)
        if not form.is_valid():
            print('Form Errors: %s' % form.errors)
            print('Non-Form Errors: %s' % form.non_field_errors())
            self.assertTrue(False, "Form does not validate")

        event = form.save(commit=False)
        event.calendar = self.cal
        event.save()

        # Determine the day today
        if self.start_dt.day <= data['day_num_of_month']:
            expected = [datetime.datetime(year=self.start_dt.year, month=self.start_dt.month, day=3, hour=self.start_dt.hour)]
        else:
            expected = [datetime.datetime(year=self.start_dt.year, month=self.start_dt.month+data['interval'], day=3, hour=self.start_dt.hour)]

        for i in range(1, data['count']):
            expected.append(expected[i-1].replace(month=expected[i-1].month + data['interval']))
        rule = event.rule

        self.assertEqual(expected, rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True), "Day 3 of every 3 months failed")

        #### The (first) (Monday) of every y months
        data = {'frequency':'MONTHLY',
                'interval':2,
                'count':5,
                'monthly_option':'v',
                'day_of_month':'2',
                'day_count_in_month':2,
                'title':'Test Rule Event',
                'start_0':self.start_dt.date(),
                'start_1':self.start_dt.time(),
                'end_0':self.start_dt.date(),
                'end_1':(self.start_dt + datetime.timedelta(hours=1)).time(),
                'description':'Test event recurrence',
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }

        form = EventForm(data=data)
        if not form.is_valid():
            print('Form Errors: %s' % form.errors)
            print('Non-Form Errors: %s' % form.non_field_errors())
            self.assertTrue(False, "Form does not validate")

        event = form.save(commit=False)
        event.calendar = self.cal
        event.save()

        #We're not testing rrule, just the Event object's ability to store and use it accurately
        #So we'll use rrule here to also come up with expected values
        expected_rule = rrule.rrule(rrule.MONTHLY, dtstart=self.start_dt,interval=data['interval'],count=data['count'],
                                    byweekday=rrule.WE(data['day_count_in_month']))

        rule = event.rule
        self.assertEqual(expected_rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , "The second Wed of every 2 months failed")

    def test_yearly_rule(self):
        #### Every year April 25
        data = {'frequency':'YEARLY',
                'interval':1,
                'count':3,
                'yearly_option':'c',
                'day_num_of_month':25,
                'chosen_month':4,
                'title':'Test Rule Event',
                'start_0':self.start_dt.date(),
                'start_1':self.start_dt.time(),
                'end_0':self.start_dt.date(),
                'end_1':(self.start_dt + datetime.timedelta(hours=1)).time(),
                'description':'Test event recurrence',
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }

        form = EventForm(data=data)
        if not form.is_valid():
            print('Form Errors: %s' % form.errors)
            print('Non-Form Errors: %s' % form.non_field_errors())
            self.assertTrue(False, "Form does not validate")

        event = form.save(commit=False)
        event.calendar = self.cal
        event.save()

        expected_rule = rrule.rrule(rrule.YEARLY, interval=1, count=data['count'],bymonth=data['chosen_month']
                                    ,bymonthday=data['day_num_of_month'])
        rule = event.rule

        self.assertEqual(expected_rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , "Every April 25 failed")

        #### The (first) (Monday) of in May every other year
        data = {'frequency':'YEARLY',
                'interval':2,
                'count':5,
                'yearly_option':'v',
                'day_of_month':'0',
                'day_count_in_month':1,
                'chosen_month':5,
                'title':'Test Rule Event',
                'start_0':self.start_dt.date(),
                'start_1':self.start_dt.time(),
                'end_0':self.start_dt.date(),
                'end_1':(self.start_dt + datetime.timedelta(hours=1)).time(),
                'description':'Test event recurrence',
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }

        form = EventForm(data=data)
        if not form.is_valid():
            print('Form Errors: %s' % form.errors)
            print('Non-Form Errors: %s' % form.non_field_errors())
            self.assertTrue(False, "Form does not validate")

        event = form.save(commit=False)
        event.calendar = self.cal
        event.save()

        #We're not testing rrule, just the Event object's ability to store and use it accurately
        #So we'll use rrule here to also come up with expected values
        expected_rule = rrule.rrule(rrule.YEARLY, dtstart=self.start_dt,interval=data['interval'],count=data['count'],
                                    byweekday=rrule.MO(data['day_count_in_month']),bymonth=data['chosen_month'])

        rule = event.rule
        self.assertEqual(expected_rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , "The second Wed of every 2 months failed")

    def _get_next_weekday(self, from_date):
        if (from_date.weekday() % 7) < 5: 
            return from_date
        else:
            delta = datetime.timedelta(days=(7 - from_date.weekday()))
            return from_date + delta
