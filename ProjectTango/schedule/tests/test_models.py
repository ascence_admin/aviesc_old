import datetime
import os
from dateutil import rrule

from django.test import TestCase
from django.core.urlresolvers import reverse

from core.tests import AuditableTestCase

from core.models import Account
from aircraft.models.aircraft import Aircraft
from schedule.models import Event, Rule, Occurrence, Calendar
from schedule.periods import Period, Month, Day
from schedule.utils import EventListManager

class TestEvent(AuditableTestCase):
    def setUp(self):
        super(TestEvent, self).setUp()
        rule = Rule(frequency = "WEEKLY")
        rule.save()
        cal = Calendar(name="MyCal")
        cal.save()
        self.recurring_data = {
                'title': 'Recent Event',
                'start': datetime.datetime(2008, 1, 5, 8, 0),
                'end': datetime.datetime(2008, 1, 5, 9, 0),
                'end_recurring_period' : datetime.datetime(2008, 5, 5, 0, 0),
                'rule': rule,
                'calendar': cal
               }
        self.data = {
                'title': 'Recent Event',
                'start': datetime.datetime(2008, 1, 5, 8, 0),
                'end': datetime.datetime(2008, 1, 5, 9, 0),
                'end_recurring_period' : datetime.datetime(2008, 5, 5, 0, 0),
                'calendar': cal
               }


    def test_recurring_event_get_occurrences(self):
        recurring_event = Event(**self.recurring_data)
        occurrences = recurring_event.get_occurrences(start=datetime.datetime(2008, 1, 12, 0, 0),
                                    end=datetime.datetime(2008, 1, 20, 0, 0))
        self.assertEquals(["%s to %s" %(o.start, o.end) for o in occurrences],
            ['2008-01-12 08:00:00 to 2008-01-12 09:00:00', '2008-01-19 08:00:00 to 2008-01-19 09:00:00'])

    def test_event_get_occurrences_after(self):
        recurring_event=Event(**self.recurring_data)
        recurring_event.save()
        occurrences = recurring_event.get_occurrences(start=datetime.datetime(2008, 1, 5),
            end = datetime.datetime(2008, 1, 6))
        occurrence = occurrences[0]
        occurrence2 = next(recurring_event.occurrences_after(datetime.datetime(2008,1,5)))
        self.assertEqual(occurrence, occurrence2)

    def test_get_occurrence(self):
        event = Event(**self.recurring_data)
        event.save()
        occurrence = event.get_occurrence(datetime.datetime(2008, 1, 5, 8, 0))
        self.assertEqual(occurrence.start, datetime.datetime(2008,1,5,8))
        occurrence.save()
        occurrence = event.get_occurrence(datetime.datetime(2008, 1, 5, 8, 0))
        self.assertTrue(occurrence.pk is not None)


class TestOccurrence(AuditableTestCase):
    def setUp(self):
        super(TestOccurrence, self).setUp()

        self.test_account = Account.objects().get(pk=1)

        self.aircraft_details = {
                                      'account' : self.test_account,
                                      'aircraft_type_name' : 'DH-82A Fleet',
                                      'hourly_rate' : 120.00,
                                      'max_pax' : 10,
                                      'avg_speed' : 120.0,
                                      'takeoff_cost' : 10.0,
                                      'aircraft_description' : 'a new test aircraft that we can start flying!',
                                      }

        self.aircraft = Aircraft(**self.aircraft_details)
        self.aircraft.save()

        self.cal = Calendar.objects.get_calendar_for_object(self.aircraft)
        self.recurring_data = {
                'title': 'Recent Event',
                'start': datetime.datetime(2008, 1, 5, 8, 0),
                'end': datetime.datetime(2008, 1, 5, 9, 0),
                'calendar': self.cal
               }
        self.data = {
                'title': 'Recent Event',
                'start': datetime.datetime(2008, 1, 5, 8, 0),
                'end': datetime.datetime(2008, 1, 5, 9, 0),
                'calendar': self.cal
               }
        self.start = datetime.datetime(2008, 1, 12, 0, 0)
        self.end = datetime.datetime(2008, 1, 27, 0, 0)

    def test_presisted_occurrences(self):
        occurrences = self.recurring_event.get_occurrences(start=self.start,
                                    end=self.end)
        persisted_occurrence = occurrences[0]
        persisted_occurrence.save()
        occurrences = self.recurring_event.get_occurrences(start=self.start,
                                    end=self.end)
        self.assertTrue(occurrences[0].pk)
        self.assertFalse(occurrences[1].pk)

    def test_moved_occurrences(self):
        occurrences = self.recurring_event.get_occurrences(start=self.start,
                                    end=self.end)
        moved_occurrence = occurrences[1]
        span_pre = (moved_occurrence.start, moved_occurrence.end)
        span_post = [x + datetime.timedelta(hours=2) for x in span_pre]
        # check has_occurrence on both periods
        period_pre = Period([self.recurring_event], span_pre[0], span_pre[1])
        period_post = Period([self.recurring_event], span_post[0], span_post[1])
        self.assertTrue(period_pre.has_occurrences())
        self.assertFalse(period_post.has_occurrences())
        # move occurrence
        moved_occurrence.move(moved_occurrence.start+datetime.timedelta(hours=2),
                              moved_occurrence.end+datetime.timedelta(hours=2))
        occurrences = self.recurring_event.get_occurrences(start=self.start,
                                    end=self.end)
        self.assertTrue(occurrences[1].moved)
        # check has_occurrence on both periods (the result should be reversed)
        period_pre = Period([self.recurring_event], span_pre[0], span_pre[1])
        period_post = Period([self.recurring_event], span_post[0], span_post[1])
        self.assertFalse(period_pre.has_occurrences())
        self.assertTrue(period_post.has_occurrences())

    def test_cancelled_occurrences(self):
        occurrences = self.recurring_event.get_occurrences(start=self.start,
                                    end=self.end)
        cancelled_occurrence = occurrences[2]
        cancelled_occurrence.cancel()
        occurrences = self.recurring_event.get_occurrences(start=self.start,
                                    end=self.end)
        self.assertTrue(occurrences[2].cancelled)
        cancelled_occurrence.uncancel()
        occurrences = self.recurring_event.get_occurrences(start=self.start,
                                    end=self.end)
        self.assertFalse(occurrences[2].cancelled)

class TestRule(AuditableTestCase):

    def setUp(self):
        super(TestRule, self).setUp()
        self.test_account = Account.objects.get(pk=1)

        self.start_dt = datetime.datetime.now()

        self.start_dt = datetime.datetime.now().replace(hour=9, minute=0, second=0, microsecond=0)

        self.aircraft_details = {
                                      'account' : self.test_account,
                                      'aircraft_type_name' : 'DH-82A Fleet',
                                      'hourly_rate' : 120.00,
                                      'max_pax' : 10,
                                      'avg_speed' : 120.0,
                                      'takeoff_cost' : 10.0,
                                      'aircraft_description' : 'a new test aircraft that we can start flying!',
                                      }

        self.aircraft = Aircraft(**self.aircraft_details)
        self.aircraft.save()

        self.cal = Calendar.objects.get_calendar_for_object(self.aircraft)

    def test_daily_rule(self):

        #### Every 2 days ####
        event_data = {
                'title':'Test Rule Event',
                'start':self.start_dt,
                'end':(self.start_dt + datetime.timedelta(hours=1)),
                'description':'Test event recurrence',
                'calendar': self.cal,
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }
        rule_data = {
                'frequency':'DAILY',
                'interval':2,
                'count':20,
                      }
        event = Event(**event_data)
        event.save()
        rule = Rule(**rule_data)
        rule.event = event
        rule.save()

        expected = []
        for i in range(rule_data['count']):
            expected.append(self.start_dt + datetime.timedelta(days=i*rule_data['interval']))

        occs = [o.start for o in event.get_occurrences(self.start_dt, self.start_dt + datetime.timedelta(days=365))]
        self.assertEqual(expected, occs, "Every 2nd day failed")

        #### Every Weekday ####
        event_data = {
                'title':'Test Rule Event',
                'start':self.start_dt,
                'end':(self.start_dt + datetime.timedelta(hours=1)),
                'calendar': self.cal,
                'description':'Test event recurrence',
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }
        rule_data = {
                'frequency':'DAILY',
                'daily_option':'wd',
                'count':40,
                     }

        event = Event(**event_data)
        event.save()
        rule = Rule(**rule_data)
        rule.event = event
        rule.save()

        expected = [self._get_next_weekday(self.start_dt)]
        for i in range(1, rule_data['count']):
            expected.append(self._get_next_weekday(expected[i-1]+datetime.timedelta(days=1)))

        occs = [o.start for o in event.get_occurrences(self.start_dt, self.start_dt + datetime.timedelta(days=365))]

        self.assertEqual(expected, occs, "Every Weekday failed")

    def test_weekly_rule(self):

        #### Every x weeks on certain days
        event_data = {'title':'Test Rule Event',
                'start':self.start_dt,
                'end':(self.start_dt + datetime.timedelta(hours=1)),
                'description':'Test event recurrence',
                'calendar': self.cal,
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }
        rule_data = {'frequency':'WEEKLY',
                'interval':2,
                'count':8,
                'days_tu':True,
                'days_th':True,
                     }

        event = Event(**event_data)
        event.save()
        rule = Rule(**rule_data)
        rule.event = event
        rule.save()

        expected_rule = rrule.rrule(rrule.WEEKLY, dtstart=self.start_dt,interval=rule_data['interval'],count=rule_data['count'],
                                    byweekday=(rrule.TU, rrule.TH))

        occs = [o.start for o in event.get_occurrences(self.start_dt, self.start_dt + datetime.timedelta(days=365))]

        self.assertEqual(expected_rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , occs
                         , "Every 2nd week on Tu & Th failed")

    def test_monthly_rule(self):
        #### Day x of every y months
        event_data = {'title':'Test Rule Event',
                'start':self.start_dt,
                'end':(self.start_dt + datetime.timedelta(hours=1)),
                'description':'Test event recurrence',
                'calendar': self.cal,
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }
        rule_data = {'frequency':'MONTHLY',
                'interval':3,
                'count':3,
                'monthly_option':'c',
                'day_num_of_month':3,
                    }

        event = Event(**event_data)
        event.save()
        rule = Rule(**rule_data)
        rule.event = event
        rule.save()

        # Determine the day today
        if self.start_dt.day <= rule_data['day_num_of_month']:
            expected = [datetime.datetime(year=self.start_dt.year, month=self.start_dt.month, day=3, hour=self.start_dt.hour)]
        else:
            expected = [datetime.datetime(year=self.start_dt.year, month=self.start_dt.month+rule_data['interval'], day=3, hour=self.start_dt.hour)]

        for i in range(1, rule_data['count']):
            expected.append(expected[i-1].replace(month=expected[i-1].month + rule_data['interval']))

        occs = [o.start for o in event.get_occurrences(self.start_dt, self.start_dt + datetime.timedelta(days=365))]

        self.assertEqual(expected, occs, "Day 3 of every 3 months failed")

        #### The (first) (Monday) of every y months
        event_data = {'title':'Test Rule Event',
                'start':self.start_dt,
                'end':(self.start_dt + datetime.timedelta(hours=1)),
                'description':'Test event recurrence',
                'account':self.test_account,
                'calendar': self.cal,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }
        rule_data = {'frequency':'MONTHLY',
                'interval':2,
                'count':5,
                'monthly_option':'v',
                'day_of_month':'2',
                'day_count_in_month':2,
                }

        event = Event(**event_data)
        event.save()
        rule = Rule(**rule_data)
        rule.event = event
        rule.save()

        expected_rule = rrule.rrule(rrule.MONTHLY, dtstart=self.start_dt,interval=rule_data['interval'],count=rule_data['count'],
                                    byweekday=rrule.WE(rule_data['day_count_in_month']))

        occs = [o.start for o in event.get_occurrences(self.start_dt, self.start_dt + datetime.timedelta(days=365))]

        self.assertEqual(expected_rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , occs
                         , "The second Wed of every 2 months failed")

    def test_yearly_rule(self):
        #### Every year April 25
        event_data = {'title':'Test Rule Event',
                'start':self.start_dt,
                'end':(self.start_dt + datetime.timedelta(hours=1)),
                'description':'Test event recurrence',
                'account':self.test_account,
                'calendar': self.cal,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }
        rule_data = {'frequency':'YEARLY',
                'interval':1,
                'count':3,
                'yearly_option':'c',
                'day_num_of_month':25,
                'chosen_month':4,
                }

        event = Event(**event_data)
        event.save()
        rule = Rule(**rule_data)
        rule.event = event
        rule.save()

        expected_rule = rrule.rrule(rrule.YEARLY, dtstart=self.start_dt, interval=1, count=rule_data['count'],bymonth=rule_data['chosen_month']
                                    ,bymonthday=rule_data['day_num_of_month'])

        occs = [o.start for o in event.get_occurrences(self.start_dt, self.start_dt + datetime.timedelta(days=365))]

        self.assertEqual(expected_rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , occs
                         , "Every April 25 failed")

        #### The (first) (Monday) of in May every other year
        event_data = {'title':'Test Rule Event',
                'start':self.start_dt,
                'end':(self.start_dt + datetime.timedelta(hours=1)),
                'description':'Test event recurrence',
                'calendar': self.cal,
                'account':self.test_account,
                'created_by':1,
                'created_on':self.start_dt,
                'created_ip':'1',
                'modified_by':1,
                'modified_on':self.start_dt,
                'modified_ip':'1',
                }
        rule_data = {'frequency':'YEARLY',
                'interval':2,
                'count':5,
                'yearly_option':'v',
                'day_of_month':'0',
                'day_count_in_month':1,
                'chosen_month':5,
                }

        event = Event(**event_data)
        event.save()
        rule = Rule(**rule_data)
        rule.event = event
        rule.save()

        expected_rule = rrule.rrule(rrule.YEARLY, dtstart=self.start_dt,interval=rule_data['interval'],count=rule_data['count'],
                                    byweekday=rrule.MO(rule_data['day_count_in_month']),bymonth=rule_data['chosen_month'])

        occs = [o.start for o in event.get_occurrences(self.start_dt, self.start_dt + datetime.timedelta(days=365))]

        self.assertEqual(expected_rule.between(self.start_dt, self.start_dt + datetime.timedelta(days=365), True)
                         , occs
                         , "The second Wed of every 2 months failed")

    def _get_next_weekday(self, from_date):
        if (from_date.weekday() % 7) < 5: 
            return from_date
        else:
            delta = datetime.timedelta(days=(7 - from_date.weekday()))
            return from_date + delta
