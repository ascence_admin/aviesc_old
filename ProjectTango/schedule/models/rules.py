from django.db import models
from django.utils.translation import ugettext, ugettext_lazy as _
from django.utils.dates import WEEKDAYS, WEEKDAYS_ABBR, MONTHS
from dateutil import rrule 
from core.models import AccountModel
from schedule.models import Event
import datetime, pytz

weekday_rrule_lookup = {'0':rrule.MO,
                        '1':rrule.TU,
                        '2':rrule.WE,
                        '3':rrule.TH,
                        '4':rrule.FR,
                        '5':rrule.SA,
                        '6':rrule.SU}
freqs_rrule_code = {"YEARLY":rrule.YEARLY,
                    "MONTHLY":rrule.MONTHLY,
                    "WEEKLY":rrule.WEEKLY,
                    "DAILY":rrule.DAILY,
                    "HOURLY":rrule.HOURLY,
                    "MINUTELY":rrule.MINUTELY,
                    "SECONDLY":rrule.SECONDLY}

daily_options = (('wd', 'Every week day'),('d','Every')) #'d' = Daily; 'wd' = Every weekday
monthly_options = (('c','c'),('v','v')) #'c' = Day X of every Y Months; 'v' = First/Last Mon/Tues of every Y months
yearly_options = (('c','c'),('v','v')) #'c' = Month-Day (Feb-4); 'v' = First/Last Mon/Tues of Jan/Feb
day_in_month_counter = ((1, 'first'), (2, 'second'), (3, 'third'), (4, 'fourth'),(-1, 'last'))

freqs = (   ("YEARLY", _("Yearly")),
            ("MONTHLY", _("Monthly")),
            ("WEEKLY", _("Weekly")),
            ("DAILY", _("Daily")),
            ("HOURLY", _("Hourly")),
            ("MINUTELY", _("Minutely")),
            ("SECONDLY", _("Secondly")))

class Rule(AccountModel):
    """
    This defines a rule by which an event will recur.  This is defined by the
    rrule in the dateutil documentation.

    * name - the human friendly name of this kind of recursion.
    * description - a short description describing this type of recursion.
    * frequency - the base recurrence period
    * param - extra params required to define this type of recursion. The params
      should follow this format:

        param = [rruleparam:value;]*
        rruleparam = see list below
        value = int[,int]*

      The options are: (documentation for these can be found at
      http://labix.org/python-dateutil#head-470fa22b2db72000d7abe698a5783a46b0731b57)
        ** count
        ** bysetpos
        ** bymonth
        ** bymonthday
        ** byyearday
        ** byweekno
        ** byweekday
        ** byhour
        ** byminute
        ** bysecond
        ** byeaster
    """
    event = models.OneToOneField(Event)

    frequency = models.CharField(_("frequency"), choices=freqs, max_length=10)
    rule_pickle = models.BinaryField("rrule", null=True)

    count = models.IntegerField(default=1)
    date_until = models.DateField(null=True, blank=True)
    end_by_date = models.BooleanField("End recurrence by", default=False) # False means end by num occurrences
    interval = models.IntegerField(default=1)

    # Daily
    daily_option = models.CharField('Daily', choices=daily_options, null=True, max_length=2)

    # Weekly (select which days of the week to recur on)
    days_mo = models.BooleanField(WEEKDAYS[0], default=False)
    days_tu = models.BooleanField(WEEKDAYS[1], default=False)
    days_we = models.BooleanField(WEEKDAYS[2], default=False)
    days_th = models.BooleanField(WEEKDAYS[3], default=False)
    days_fr = models.BooleanField(WEEKDAYS[4], default=False)
    days_sa = models.BooleanField(WEEKDAYS[5], default=False)
    days_su = models.BooleanField(WEEKDAYS[6], default=False)

    # Monthly params
    # Type of monthly recurrence
    monthly_option = models.CharField(choices=monthly_options, null=True, max_length=2)
    # for the xth day of every <interval> months:
    day_num_of_month = models.IntegerField(null=True) # x
    # for the (first) (Monday) of every <interval> months
    day_count_in_month = models.IntegerField(choices=day_in_month_counter, null=True, blank=True) # first, second, last, etc
    day_of_month = models.IntegerField(choices=WEEKDAYS.items(), null=True, blank=True) # MO, TU, WE, etc

    # Yearly params
    # Type of yearly recurrence
    yearly_option = models.CharField(choices=yearly_options, null=True, max_length=2)
    # for the (first) (Monday) of (Jan) <-- the first and Monday variables are the same as the Monthly params
    chosen_month = models.IntegerField(choices=tuple(MONTHS.items()), null=True) # Jan, Feb, etc

    def rrule(self, start_dt):
        # To ensure rrule obeys timezone, especially if DST applies
        # set its dtstart to the naive, localised time 
        start_dt_n = start_dt.replace(tzinfo=None)
        rr = rrule.rrule(freqs_rrule_code[self.frequency], dtstart=start_dt, **self._create_kwargs())
        return rr

    def _daily_params(self):
        if self.daily_option == 'wd':
            # Every weekday
            self._rrule_kwargs['byweekday'] = (rrule.MO, rrule.TU, rrule.WE, rrule.TH, rrule.FR)
        else:
            # Every x days
            self._rrule_kwargs['interval'] = self.interval

    def _weekly_params(self):
        # Every x weeks on certain days
        selected_days = []
        if self.days_mo: selected_days.append(rrule.MO)
        if self.days_tu: selected_days.append(rrule.TU)
        if self.days_we: selected_days.append(rrule.WE)
        if self.days_th: selected_days.append(rrule.TH)
        if self.days_fr: selected_days.append(rrule.FR)
        if self.days_sa: selected_days.append(rrule.SA)
        if self.days_su: selected_days.append(rrule.SU)
        self._rrule_kwargs['byweekday'] = selected_days
        self._rrule_kwargs['interval'] = self.interval

    def _monthly_params(self):
        if self.monthly_option == 'c':
            # Day x of every y months  ## Constant
            self._rrule_kwargs['bymonthday'] = self.day_num_of_month
            self._rrule_kwargs['interval'] = self.interval
        elif self.monthly_option == 'v':
            # The (first) (Monday) of every <interval> months  ## Variable
            if self.day_count_in_month:
                byweekday = weekday_rrule_lookup[self.day_of_month]
                byweekday.n = int(self.day_count_in_month)
            self._rrule_kwargs['byweekday'] = byweekday if byweekday else weekday_rrule_lookup[self.day_of_month]
            self._rrule_kwargs['interval'] = self.interval

    def _yearly_params(self):
        if self.yearly_option == 'c':
            # On (Jan) (5th) ## Constant
            self._rrule_kwargs['bymonthday'] = self.day_num_of_month
            self._rrule_kwargs['bymonth'] = self.chosen_month
            self._rrule_kwargs['interval'] = self.interval
        elif self.yearly_option == 'v':
            # the (first) (Mon) of (Jan) ## Variable
            if self.day_count_in_month:
                byweekday = weekday_rrule_lookup[self.day_of_month]
                byweekday.n = int(self.day_count_in_month)
            self._rrule_kwargs['byweekday'] = byweekday
            self._rrule_kwargs['bymonth'] = self.chosen_month
            self._rrule_kwargs['interval'] = self.interval

    def _create_kwargs(self):
        self._rrule_kwargs = {}

        # Set up the rrule object with the latest values
        switch = {'YEARLY': self._yearly_params, 'MONTHLY':self._monthly_params, 'WEEKLY':self._weekly_params, 'DAILY':self._daily_params}
        switch[self.frequency]()

        if self.end_by_date: 
            self._rrule_kwargs['until'] = datetime.datetime.combine(self.date_until, datetime.time(23,59,59,0,pytz.UTC))
        else: 
            self._rrule_kwargs['count'] = self.count     

        return self._rrule_kwargs

    class Meta:
        verbose_name = _('rule')
        verbose_name_plural = _('rules')
        app_label = 'schedule'

    def __unicode__(self):
        """Human readable string for Rule"""
        return self.name
