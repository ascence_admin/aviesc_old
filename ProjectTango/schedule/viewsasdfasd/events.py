from django.http import HttpResponseRedirect
from django.shortcuts import render, get_list_or_404, get_object_or_404
from django.core.urlresolvers import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, FormView
from django.contrib import messages
from django.utils import formats

from allauth.account.decorators import verified_email_required

from schedule.models.events import Event
from core.constructs import ActionList, DetailList

CREATE_LOG_AIRCRAFT_REQUEST_KEY = 'Create-Log-Aircraft'
CREATE_LOG_PILOT_REQUEST_KEY = 'Create-Log-Pilot'
CREATE_LOG_RULE_REQUEST_KEY = 'Create-Log-Rule'
CREATE_LOG_INSPECTOR_REQUEST_KEY = 'Create-Log-Inspector'


def EventFormView(FormView):

    model = Event
    template_name = 'aircraft/logentry_create.html'
    form_class = LogForm
    success_message = "Log entry was saved successfully"

    def get_form_kwargs(self):
        kwargs = super(LogCreate, self).get_form_kwargs()
        kwargs.update({'account': self.request.user.userdetails.account})
        kwargs.update({'user':self.request.user})
        # Depending on where we're coming from, some items may be pre-populated for us
        kwargs.update({'aircraft': self.request.session.pop(CREATE_LOG_AIRCRAFT_REQUEST_KEY, None)})
        kwargs.update({'pilot': self.request.session.pop(CREATE_LOG_PILOT_REQUEST_KEY, None)})
        kwargs.update({'rule': self.request.session.pop(CREATE_LOG_RULE_REQUEST_KEY, None)})
        kwargs.update({'inspector': self.request.session.pop(CREATE_LOG_INSPECTOR_REQUEST_KEY, None)})
        self.pre_pop_aircraft = True if kwargs['aircraft'] is not None else False
        self.pre_pop_rule = True if kwargs['rule'] is not None else False
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Tell the template if we are pre-populating
        context['pre_pop_aircraft'] = self.pre_pop_aircraft
        context['pre_pop_rule'] = self.pre_pop_rule
        #Add actions to the left hand actions
        actions = ActionList.GetActionsForContext(context)
        actions.insertAction(reverse('aircraft-list', kwargs={}), 'List Aircraft')
        return context
