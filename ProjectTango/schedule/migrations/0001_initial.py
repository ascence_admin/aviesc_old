# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Calendar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('name', models.CharField(verbose_name='name', max_length=200)),
                ('slug', models.SlugField(verbose_name='slug', max_length=200)),
                ('account', models.ForeignKey(to='core.Account')),
            ],
            options={
                'verbose_name': 'calendar',
                'verbose_name_plural': 'calendar',
            },
        ),
        migrations.CreateModel(
            name='CalendarRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.IntegerField()),
                ('distinction', models.CharField(verbose_name='distinction', null=True, max_length=20)),
                ('inheritable', models.BooleanField(verbose_name='inheritable', default=True)),
                ('calendar', models.ForeignKey(verbose_name='calendar', to='schedule.Calendar')),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'calendar relation',
                'verbose_name_plural': 'calendar relations',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('start', models.DateTimeField(verbose_name='start')),
                ('end', models.DateTimeField(verbose_name='end', help_text='The end time must be later than the start time.')),
                ('title', models.CharField(verbose_name='title', max_length=255)),
                ('description', models.TextField(verbose_name='description', blank=True, null=True)),
                ('account', models.ForeignKey(to='core.Account')),
                ('calendar', models.ForeignKey(blank=True, to='schedule.Calendar')),
            ],
            options={
                'verbose_name': 'event',
                'verbose_name_plural': 'events',
            },
        ),
        migrations.CreateModel(
            name='EventRelation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.IntegerField()),
                ('distinction', models.CharField(verbose_name='distinction', null=True, max_length=20)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('event', models.ForeignKey(verbose_name='event', to='schedule.Event')),
            ],
            options={
                'verbose_name': 'event relation',
                'verbose_name_plural': 'event relations',
            },
        ),
        migrations.CreateModel(
            name='Occurrence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(verbose_name='title', blank=True, max_length=255, null=True)),
                ('description', models.TextField(verbose_name='description', blank=True, null=True)),
                ('start', models.DateTimeField(verbose_name='start')),
                ('end', models.DateTimeField(verbose_name='end')),
                ('cancelled', models.BooleanField(verbose_name='cancelled', default=False)),
                ('original_start', models.DateTimeField(verbose_name='original start')),
                ('original_end', models.DateTimeField(verbose_name='original end')),
                ('event', models.ForeignKey(verbose_name='event', to='schedule.Event')),
            ],
            options={
                'verbose_name': 'occurrence',
                'verbose_name_plural': 'occurrences',
            },
        ),
        migrations.CreateModel(
            name='Rule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(editable=False)),
                ('created_ip', models.CharField(max_length=45, editable=False)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(editable=False)),
                ('modified_ip', models.CharField(max_length=45, editable=False)),
                ('frequency', models.CharField(verbose_name='frequency', max_length=10, choices=[('YEARLY', 'Yearly'), ('MONTHLY', 'Monthly'), ('WEEKLY', 'Weekly'), ('DAILY', 'Daily'), ('HOURLY', 'Hourly'), ('MINUTELY', 'Minutely'), ('SECONDLY', 'Secondly')])),
                ('rule_pickle', models.BinaryField(verbose_name='rrule', null=True)),
                ('count', models.IntegerField(default=1)),
                ('date_until', models.DateField(blank=True, null=True)),
                ('end_by_date', models.BooleanField(verbose_name='End recurrence by', default=False)),
                ('interval', models.IntegerField(default=1)),
                ('daily_option', models.CharField(verbose_name='Daily', null=True, max_length=2, choices=[('wd', 'Every week day'), ('d', 'Every')])),
                ('days_mo', models.BooleanField(verbose_name='Monday', default=False)),
                ('days_tu', models.BooleanField(verbose_name='Tuesday', default=False)),
                ('days_we', models.BooleanField(verbose_name='Wednesday', default=False)),
                ('days_th', models.BooleanField(verbose_name='Thursday', default=False)),
                ('days_fr', models.BooleanField(verbose_name='Friday', default=False)),
                ('days_sa', models.BooleanField(verbose_name='Saturday', default=False)),
                ('days_su', models.BooleanField(verbose_name='Sunday', default=False)),
                ('monthly_option', models.CharField(null=True, max_length=2, choices=[('c', 'c'), ('v', 'v')])),
                ('day_num_of_month', models.IntegerField(null=True)),
                ('day_count_in_month', models.IntegerField(blank=True, null=True, choices=[(1, 'first'), (2, 'second'), (3, 'third'), (4, 'fourth'), (-1, 'last')])),
                ('day_of_month', models.IntegerField(blank=True, null=True, choices=[(0, 'Monday'), (1, 'Tuesday'), (2, 'Wednesday'), (3, 'Thursday'), (4, 'Friday'), (5, 'Saturday'), (6, 'Sunday')])),
                ('yearly_option', models.CharField(null=True, max_length=2, choices=[('c', 'c'), ('v', 'v')])),
                ('chosen_month', models.IntegerField(null=True, choices=[(1, 'January'), (2, 'February'), (3, 'March'), (4, 'April'), (5, 'May'), (6, 'June'), (7, 'July'), (8, 'August'), (9, 'September'), (10, 'October'), (11, 'November'), (12, 'December')])),
                ('account', models.ForeignKey(to='core.Account')),
                ('event', models.OneToOneField(to='schedule.Event')),
            ],
            options={
                'verbose_name': 'rule',
                'verbose_name_plural': 'rules',
            },
        ),
    ]
