from rest_framework import serializers

from schedule.models.events import Event


class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event
        fields = ('id', 'title', 'start', 'end', )