# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Rule.description'
        db.delete_column('schedule_rule', 'description')

        # Deleting field 'Rule.params'
        db.delete_column('schedule_rule', 'params')

        # Deleting field 'Rule.name'
        db.delete_column('schedule_rule', 'name')

        # Adding field 'Rule.created_on'
        db.add_column('schedule_rule', 'created_on',
                      self.gf('django.db.models.fields.DateTimeField')(blank=True, default=datetime.datetime(2014, 2, 18, 0, 0), auto_now_add=True),
                      keep_default=False)

        # Adding field 'Rule.created_by'
        db.add_column('schedule_rule', 'created_by',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Rule.created_ip'
        db.add_column('schedule_rule', 'created_ip',
                      self.gf('django.db.models.fields.CharField')(default='1', max_length=45),
                      keep_default=False)

        # Adding field 'Rule.modified_on'
        db.add_column('schedule_rule', 'modified_on',
                      self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True, default=datetime.datetime(2014, 2, 18, 0, 0)),
                      keep_default=False)

        # Adding field 'Rule.modified_by'
        db.add_column('schedule_rule', 'modified_by',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Rule.modified_ip'
        db.add_column('schedule_rule', 'modified_ip',
                      self.gf('django.db.models.fields.CharField')(default='1', max_length=45),
                      keep_default=False)

        # Adding field 'Rule.account'
        db.add_column('schedule_rule', 'account',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Account'], default=1),
                      keep_default=False)

        # Adding field 'Rule.event'
        db.add_column('schedule_rule', 'event',
                      self.gf('django.db.models.fields.related.OneToOneField')(to=orm['schedule.Event'], unique=True, default=1),
                      keep_default=False)

        # Adding field 'Rule.rule_pickle'
        db.add_column('schedule_rule', 'rule_pickle',
                      self.gf('django.db.models.fields.BinaryField')(null=True),
                      keep_default=False)

        # Adding field 'Rule.count'
        db.add_column('schedule_rule', 'count',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Rule.date_until'
        db.add_column('schedule_rule', 'date_until',
                      self.gf('django.db.models.fields.DateField')(blank=True, null=True),
                      keep_default=False)

        # Adding field 'Rule.end_by_date'
        db.add_column('schedule_rule', 'end_by_date',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Rule.interval'
        db.add_column('schedule_rule', 'interval',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Rule.daily_option'
        db.add_column('schedule_rule', 'daily_option',
                      self.gf('django.db.models.fields.CharField')(max_length=2, null=True),
                      keep_default=False)

        # Adding field 'Rule.days_mo'
        db.add_column('schedule_rule', 'days_mo',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Rule.days_tu'
        db.add_column('schedule_rule', 'days_tu',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Rule.days_we'
        db.add_column('schedule_rule', 'days_we',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Rule.days_th'
        db.add_column('schedule_rule', 'days_th',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Rule.days_fr'
        db.add_column('schedule_rule', 'days_fr',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Rule.days_sa'
        db.add_column('schedule_rule', 'days_sa',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Rule.days_su'
        db.add_column('schedule_rule', 'days_su',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Rule.monthly_option'
        db.add_column('schedule_rule', 'monthly_option',
                      self.gf('django.db.models.fields.CharField')(max_length=2, null=True),
                      keep_default=False)

        # Adding field 'Rule.day_num_of_month'
        db.add_column('schedule_rule', 'day_num_of_month',
                      self.gf('django.db.models.fields.IntegerField')(null=True),
                      keep_default=False)

        # Adding field 'Rule.day_count_in_month'
        db.add_column('schedule_rule', 'day_count_in_month',
                      self.gf('django.db.models.fields.IntegerField')(null=True),
                      keep_default=False)

        # Adding field 'Rule.day_of_month'
        db.add_column('schedule_rule', 'day_of_month',
                      self.gf('django.db.models.fields.IntegerField')(null=True),
                      keep_default=False)

        # Adding field 'Rule.yearly_option'
        db.add_column('schedule_rule', 'yearly_option',
                      self.gf('django.db.models.fields.CharField')(max_length=2, null=True),
                      keep_default=False)

        # Adding field 'Rule.chosen_month'
        db.add_column('schedule_rule', 'chosen_month',
                      self.gf('django.db.models.fields.CharField')(max_length=1, null=True),
                      keep_default=False)

        # Deleting field 'Event.end_recurring_period'
        db.delete_column('schedule_event', 'end_recurring_period')

        # Deleting field 'Event.rule'
        db.delete_column('schedule_event', 'rule_id')


    def backwards(self, orm):
        # Adding field 'Rule.description'
        db.add_column('schedule_rule', 'description',
                      self.gf('django.db.models.fields.TextField')(default=''),
                      keep_default=False)

        # Adding field 'Rule.params'
        db.add_column('schedule_rule', 'params',
                      self.gf('django.db.models.fields.TextField')(blank=True, null=True),
                      keep_default=False)

        # Adding field 'Rule.name'
        db.add_column('schedule_rule', 'name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=32),
                      keep_default=False)

        # Deleting field 'Rule.created_on'
        db.delete_column('schedule_rule', 'created_on')

        # Deleting field 'Rule.created_by'
        db.delete_column('schedule_rule', 'created_by')

        # Deleting field 'Rule.created_ip'
        db.delete_column('schedule_rule', 'created_ip')

        # Deleting field 'Rule.modified_on'
        db.delete_column('schedule_rule', 'modified_on')

        # Deleting field 'Rule.modified_by'
        db.delete_column('schedule_rule', 'modified_by')

        # Deleting field 'Rule.modified_ip'
        db.delete_column('schedule_rule', 'modified_ip')

        # Deleting field 'Rule.account'
        db.delete_column('schedule_rule', 'account_id')

        # Deleting field 'Rule.event'
        db.delete_column('schedule_rule', 'event_id')

        # Deleting field 'Rule.rule_pickle'
        db.delete_column('schedule_rule', 'rule_pickle')

        # Deleting field 'Rule.count'
        db.delete_column('schedule_rule', 'count')

        # Deleting field 'Rule.date_until'
        db.delete_column('schedule_rule', 'date_until')

        # Deleting field 'Rule.end_by_date'
        db.delete_column('schedule_rule', 'end_by_date')

        # Deleting field 'Rule.interval'
        db.delete_column('schedule_rule', 'interval')

        # Deleting field 'Rule.daily_option'
        db.delete_column('schedule_rule', 'daily_option')

        # Deleting field 'Rule.days_mo'
        db.delete_column('schedule_rule', 'days_mo')

        # Deleting field 'Rule.days_tu'
        db.delete_column('schedule_rule', 'days_tu')

        # Deleting field 'Rule.days_we'
        db.delete_column('schedule_rule', 'days_we')

        # Deleting field 'Rule.days_th'
        db.delete_column('schedule_rule', 'days_th')

        # Deleting field 'Rule.days_fr'
        db.delete_column('schedule_rule', 'days_fr')

        # Deleting field 'Rule.days_sa'
        db.delete_column('schedule_rule', 'days_sa')

        # Deleting field 'Rule.days_su'
        db.delete_column('schedule_rule', 'days_su')

        # Deleting field 'Rule.monthly_option'
        db.delete_column('schedule_rule', 'monthly_option')

        # Deleting field 'Rule.day_num_of_month'
        db.delete_column('schedule_rule', 'day_num_of_month')

        # Deleting field 'Rule.day_count_in_month'
        db.delete_column('schedule_rule', 'day_count_in_month')

        # Deleting field 'Rule.day_of_month'
        db.delete_column('schedule_rule', 'day_of_month')

        # Deleting field 'Rule.yearly_option'
        db.delete_column('schedule_rule', 'yearly_option')

        # Deleting field 'Rule.chosen_month'
        db.delete_column('schedule_rule', 'chosen_month')

        # Adding field 'Event.end_recurring_period'
        db.add_column('schedule_event', 'end_recurring_period',
                      self.gf('django.db.models.fields.DateTimeField')(blank=True, null=True),
                      keep_default=False)

        # Adding field 'Event.rule'
        db.add_column('schedule_event', 'rule',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['schedule.Rule'], blank=True, null=True),
                      keep_default=False)


    models = {
        'contenttypes.contenttype': {
            'Meta': {'object_name': 'ContentType', 'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)", 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'forecast_ahead_days': ('django.db.models.fields.IntegerField', [], {'default': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'maint_work_end_hr': ('django.db.models.fields.IntegerField', [], {'default': '17'}),
            'maint_work_start_hr': ('django.db.models.fields.IntegerField', [], {'default': '7'}),
            'maint_work_wkend': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'schedule_rule_threshold': ('django.db.models.fields.FloatField', [], {'default': '0.9'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        'schedule.calendarrelation': {
            'Meta': {'object_name': 'CalendarRelation'},
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Calendar']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'distinction': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inheritable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['schedule.Calendar']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'schedule.eventrelation': {
            'Meta': {'object_name': 'EventRelation'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'distinction': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Event']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'schedule.occurrence': {
            'Meta': {'object_name': 'Occurrence'},
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Event']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'original_end': ('django.db.models.fields.DateTimeField', [], {}),
            'original_start': ('django.db.models.fields.DateTimeField', [], {}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '255', 'null': 'True'})
        },
        'schedule.rule': {
            'Meta': {'object_name': 'Rule'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'chosen_month': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'}),
            'count': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'daily_option': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'}),
            'date_until': ('django.db.models.fields.DateField', [], {'blank': 'True', 'null': 'True'}),
            'day_count_in_month': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'day_num_of_month': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'day_of_month': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'days_fr': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'days_mo': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'days_sa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'days_su': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'days_th': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'days_tu': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'days_we': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'end_by_date': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'event': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['schedule.Event']", 'unique': 'True'}),
            'frequency': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interval': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'monthly_option': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'}),
            'rule_pickle': ('django.db.models.fields.BinaryField', [], {'null': 'True'}),
            'yearly_option': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True'})
        }
    }

    complete_apps = ['schedule']