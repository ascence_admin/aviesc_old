# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Renaming column for 'Calendar.modified_by' to match new field type.
        db.rename_column('schedule_calendar', 'modified_by_id', 'modified_by')
        # Changing field 'Calendar.modified_by'
        db.alter_column('schedule_calendar', 'modified_by', self.gf('django.db.models.fields.IntegerField')())
        # Removing index on 'Calendar', fields ['modified_by']
        db.delete_index('schedule_calendar', ['modified_by_id'])


        # Renaming column for 'Calendar.created_by' to match new field type.
        db.rename_column('schedule_calendar', 'created_by_id', 'created_by')
        # Changing field 'Calendar.created_by'
        db.alter_column('schedule_calendar', 'created_by', self.gf('django.db.models.fields.IntegerField')())
        # Removing index on 'Calendar', fields ['created_by']
        db.delete_index('schedule_calendar', ['created_by_id'])


        # Renaming column for 'Event.modified_by' to match new field type.
        db.rename_column('schedule_event', 'modified_by_id', 'modified_by')
        # Changing field 'Event.modified_by'
        db.alter_column('schedule_event', 'modified_by', self.gf('django.db.models.fields.IntegerField')())
        # Removing index on 'Event', fields ['modified_by']
        db.delete_index('schedule_event', ['modified_by_id'])


        # Renaming column for 'Event.created_by' to match new field type.
        db.rename_column('schedule_event', 'created_by_id', 'created_by')
        # Changing field 'Event.created_by'
        db.alter_column('schedule_event', 'created_by', self.gf('django.db.models.fields.IntegerField')())
        # Removing index on 'Event', fields ['created_by']
        db.delete_index('schedule_event', ['created_by_id'])


    def backwards(self, orm):
        # Adding index on 'Event', fields ['created_by']
        db.create_index('schedule_event', ['created_by_id'])

        # Adding index on 'Event', fields ['modified_by']
        db.create_index('schedule_event', ['modified_by_id'])

        # Adding index on 'Calendar', fields ['created_by']
        db.create_index('schedule_calendar', ['created_by_id'])

        # Adding index on 'Calendar', fields ['modified_by']
        db.create_index('schedule_calendar', ['modified_by_id'])


        # Renaming column for 'Calendar.modified_by' to match new field type.
        db.rename_column('schedule_calendar', 'modified_by', 'modified_by_id')
        # Changing field 'Calendar.modified_by'
        db.alter_column('schedule_calendar', 'modified_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))

        # Renaming column for 'Calendar.created_by' to match new field type.
        db.rename_column('schedule_calendar', 'created_by', 'created_by_id')
        # Changing field 'Calendar.created_by'
        db.alter_column('schedule_calendar', 'created_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))

        # Renaming column for 'Event.modified_by' to match new field type.
        db.rename_column('schedule_event', 'modified_by', 'modified_by_id')
        # Changing field 'Event.modified_by'
        db.alter_column('schedule_event', 'modified_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))

        # Renaming column for 'Event.created_by' to match new field type.
        db.rename_column('schedule_event', 'created_by', 'created_by_id')
        # Changing field 'Event.created_by'
        db.alter_column('schedule_event', 'created_by_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))

    models = {
        'contenttypes.contenttype': {
            'Meta': {'object_name': 'ContentType', 'db_table': "'django_content_type'", 'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.account': {
            'Meta': {'object_name': 'Account'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inactive': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'start_dt': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        'schedule.calendar': {
            'Meta': {'object_name': 'Calendar'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '200'})
        },
        'schedule.calendarrelation': {
            'Meta': {'object_name': 'CalendarRelation'},
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Calendar']"}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'distinction': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inheritable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'schedule.event': {
            'Meta': {'object_name': 'Event'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Account']"}),
            'calendar': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Calendar']", 'blank': 'True'}),
            'created_by': ('django.db.models.fields.IntegerField', [], {}),
            'created_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'end_recurring_period': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified_by': ('django.db.models.fields.IntegerField', [], {}),
            'modified_ip': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'modified_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'rule': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['schedule.Rule']", 'blank': 'True'}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'schedule.eventrelation': {
            'Meta': {'object_name': 'EventRelation'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'distinction': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Event']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'schedule.occurrence': {
            'Meta': {'object_name': 'Occurrence'},
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end': ('django.db.models.fields.DateTimeField', [], {}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['schedule.Event']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'original_end': ('django.db.models.fields.DateTimeField', [], {}),
            'original_start': ('django.db.models.fields.DateTimeField', [], {}),
            'start': ('django.db.models.fields.DateTimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True', 'null': 'True'})
        },
        'schedule.rule': {
            'Meta': {'object_name': 'Rule'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'frequency': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'params': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['schedule']