from django import forms
from django.forms.models import BaseInlineFormSet, inlineformset_factory
from django.utils.dates import WEEKDAYS, WEEKDAYS_ABBR, MONTHS
from django.utils.translation import ugettext_lazy as _
from schedule.conf.settings import FIRST_DAY_OF_WEEK, SHOW_CANCELLED_OCCURRENCES
from core.forms import InlineSplitDateTimeWidget, AccountModelForm
from schedule.models import Calendar, Event, Occurrence, Rule
import datetime
import time
from dateutil import rrule 

freqs_select_choices = (("DAILY", _("Daily")), 
                        ("WEEKLY", _("Weekly")),
                        ("MONTHLY", _("Monthly")),
                        ("YEARLY", _("Yearly")))
freqs_rrule_code = {"YEARLY":rrule.YEARLY,
                    "MONTHLY":rrule.MONTHLY,
                    "WEEKLY":rrule.WEEKLY,
                    "DAILY":rrule.DAILY,
                    }
daily_options = (('wd', 'Every week day'),('d','Every')) #'d' = Daily; 'wd' = Every weekday
monthly_options = (('c','c'),('v','v')) #'c' = Day X of every Y Months; 'v' = First/Last Mon/Tues of every Y months
yearly_options = (('c','c'),('v','v')) #'c' = Month-Day (Feb-4); 'v' = First/Last Mon/Tues of Jan/Feb
day_in_month_counter = ((1, 'first'), (2, 'second'), (3, 'third'), (4, 'fourth'),(-1, 'last'))
end_by_options = ((False, 'End after'),(True,'End by'))

class EventForm(AccountModelForm):
    def __init__(self, hour24=False, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        # Set the value of has_recurrence to whether or not the Rule is present
        self.has_recurrence = self.instance is not None and self.instance.has_rule

    title = forms.CharField(label="Title", widget=forms.TextInput(attrs={'placeholder':'Title'}))
    start = forms.DateTimeField(label="Start time", widget=InlineSplitDateTimeWidget(attrs={}))
    end = forms.DateTimeField(label="End time", widget=InlineSplitDateTimeWidget(attrs={}))
    description = forms.CharField(label="Description", widget=forms.Textarea(attrs={'placeholder':'A brief description'}))
    calendar = forms.ModelChoiceField(label="Aircraft", required=False, queryset=Calendar.objects.all())

    # Recurrence fields
    has_recurrence = forms.BooleanField(label="Repeat this event", required=False, initial=False)

    def clean_end(self):
        if self.cleaned_data['end'] <= self.cleaned_data['start']:
            raise forms.ValidationError(_("The end time must be later than start time."))
        return self.cleaned_data['end']

    class Meta:
        model = Event
        exclude = ('creator', 'created_on', 'account')
   
    class Media:
        js = ('core/js/foundation/foundation.forms.js',)
     

class OccurrenceForm(AccountModelForm):
    def clean_end(self):
        if self.cleaned_data['end'] <= self.cleaned_data['start']:
            raise forms.ValidationError(_("The end time must be later than start time."))
        return self.cleaned_data['end']
    
    class Meta:
        model = Occurrence
        exclude = ('original_start', 'original_end', 'event', 'cancelled')

class RuleForm(AccountModelForm):

    frequency = forms.ChoiceField(label="Frequency", choices=freqs_select_choices, widget=forms.RadioSelect(attrs={'class':'rule_form','data-customforms':"disabled"}), required=False, initial="DAILY")
    count = forms.IntegerField(label="End after", required=False, initial=1)
    date_until = forms.DateField(label="End by", widget=forms.DateInput(attrs={'type':'date'}) , required=False, initial=datetime.date.today())
    end_by_date = forms.ChoiceField(label="End recurrence by", choices=end_by_options, widget=forms.RadioSelect(attrs={'class':'rule_form'}), required=False, initial=False) # False means end by num occurrences
    interval = forms.IntegerField(required=False)

    # Daily
    daily_option = forms.ChoiceField(label='Daily',widget=forms.RadioSelect(attrs={'class':'rule_form'}), choices=daily_options, required=False, initial='wd')
    daily_interval = forms.IntegerField(label="day(s)", required=False, initial=1)

    # Weekly (select which days of the week to recur on)
    days_mo = forms.BooleanField(label=WEEKDAYS[0], required=False)
    days_tu = forms.BooleanField(label=WEEKDAYS[1], required=False)
    days_we = forms.BooleanField(label=WEEKDAYS[2], required=False)
    days_th = forms.BooleanField(label=WEEKDAYS[3], required=False)
    days_fr = forms.BooleanField(label=WEEKDAYS[4], required=False)
    days_sa = forms.BooleanField(label=WEEKDAYS[5], required=False)
    days_su = forms.BooleanField(label=WEEKDAYS[6], required=False)
    weekly_interval = forms.IntegerField(required=False, initial=1)

    # Monthly params
    # Type of monthly recurrence
    monthly_option = forms.ChoiceField(label='every',widget=forms.RadioSelect(attrs={'class':'rule_form'}), choices=monthly_options, required=False, initial='c')
    # for the xth day of every <interval> months:
    day_num_of_month = forms.IntegerField(required=False) # x
    # for the (first) (Monday) of every <interval> months
    day_count_in_month = forms.ChoiceField(label='every',choices=day_in_month_counter, required=False) # first, second, last, etc
    day_of_month = forms.ChoiceField(label='day',choices=WEEKDAYS.items(), required=False) # MO, TU, WE, etc
    monthly_interval = forms.IntegerField(required=False, initial=1)

    # Yearly params
    # Type of yearly recurrence
    yearly_option = forms.ChoiceField(label='every', widget=forms.RadioSelect(attrs={'class':'rule_form'}), choices=yearly_options, required=False, initial='c')
    # for the (first) (Monday) of (Jan) <-- the first and Monday variables are the same as the Monthly params
    chosen_month = forms.ChoiceField(label='month', choices=tuple(MONTHS.items()), required=False) # Jan, Feb, etc
    yearly_day_num_of_month = forms.IntegerField(required=False)
    yearly_day_count_in_month = forms.ChoiceField(label='every',choices=day_in_month_counter, required=False)
    yearly_day_of_month = forms.ChoiceField(label='day',choices=WEEKDAYS.items(), required=False)
    yearly_interval = forms.IntegerField(required=False, initial=1)

    def __init__(self, *args, **kwargs):
        super(RuleForm, self).__init__(*args, **kwargs)
        #Ensure that if we have initial data
        # that the 'interval' value gets distributed to be displayed appropriately

        if 'interval' in self.initial:
            self.initial['yearly_interval'] = self.initial['interval']
            self.initial['monthly_interval'] = self.initial['interval']
            self.initial['weekly_interval'] = self.initial['interval']
            self.initial['daily_interval'] = self.initial['interval']
        if 'day_num_of_month' in self.initial:
            self.initial['yearly_day_num_of_month'] = self.initial['day_num_of_month']
        if 'day_count_in_month' in self.initial:
            self.initial['yearly_day_count_in_month'] = self.initial['day_count_in_month']
        if 'day_of_month' in self.initial:
            self.initial['yearly_day_of_month'] = self.initial['day_of_month']

    def clean(self):
        if self.cleaned_data['frequency'] == 'YEARLY':
            self.cleaned_data['interval'] = self.cleaned_data['yearly_interval']
        elif self.cleaned_data['frequency'] == 'MONTHLY':
            self.cleaned_data['interval'] = self.cleaned_data['monthly_interval']
        elif self.cleaned_data['frequency'] == 'WEEKLY':
            self.cleaned_data['interval'] = self.cleaned_data['weekly_interval']
        elif self.cleaned_data['frequency'] == 'DAILY':
            self.cleaned_data['interval'] = self.cleaned_data['daily_interval']
        return self.cleaned_data

    class Meta:
        model = Rule
        exclude = ('creator', 'created_on', 'event', 'account')

class CustomRuleFormSet(BaseInlineFormSet):
    def clean(self):
        super(CustomRuleFormSet, self).clean()

        if self.instance:
            if not self.data['has_recurrence'] and self.instance.has_rule:
                # Ensure we clear out the rrule then as this is no longer set to recur
                self.instance.rule.delete(commit=False)

RuleFormSet = inlineformset_factory(Event, Rule, form=RuleForm, extra=1, can_delete=True, max_num=1, validate_max=1, formset=CustomRuleFormSet)
                