from urllib.parse import quote
from django.shortcuts import render_to_response, get_object_or_404
from django.views.generic import TemplateView
from django.views.generic.edit import DeleteView
from django.views.generic.list import ListView
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder, json
import datetime
import pytz
from django.utils import timezone

from allauth.account.decorators import verified_email_required

from schedule.conf.settings import GET_EVENTS_FUNC, OCCURRENCE_CANCEL_REDIRECT
from schedule.forms import EventForm, OccurrenceForm, RuleFormSet
from schedule.models import *
from schedule.periods import weekday_names, Period
from schedule.utils import check_event_permissions, coerce_date_dict
from schedule.templatetags.scheduletags import querystring_for_date

from core.constructs import ActionList, DetailList

def day_url(slug, date):
    return "%s%s" % (reverse( "day_calendar", kwargs= {'calendar_slug': slug,}), querystring_for_date(date))
def week_url(slug, date):
    return "%s%s" % (reverse( "week_calendar", kwargs= {'calendar_slug': slug,}), querystring_for_date(date))
def month_url(slug, date):
    return "%s%s" % (reverse( "month_calendar", kwargs= {'calendar_slug': slug,}), querystring_for_date(date))
def year_url(slug, date):
    return "%s%s" % (reverse( "year_calendar", kwargs= {'calendar_slug': slug,}), querystring_for_date(date))

url_list = {'day' : day_url,
            'week' : week_url,
            'month' : month_url,
            'year' : year_url,
            }

full_calendar_avail_views = {'month' : 'month',
                             'week' : 'agendaWeek',
                             'day': 'agendaDay',
                             }
class ScheduleView(ListView):
    queryset = Calendar.objects.all()
    template_name='schedule/calendar_list.html'

def calendar(request, calendar_slug, template='schedule/calendar.html'):
    """
    This view returns a calendar.  This view should be used if you are
    interested in the meta data of a calendar, not if you want to display a
    calendar.  It is suggested that you use calendar_by_periods if you would
    like to display a calendar.

    Context Variables:

    ``calendar``
        The Calendar object designated by the ``calendar_slug``.
    """
    calendar = get_object_or_404(Calendar, slug=calendar_slug)
    return render_to_response(template, {
        "calendar": calendar,
    }, context_instance=RequestContext(request))

def calendar_by_periods(request, calendar_slug=None, periods=None,
    template_name="schedule/cal.html"):
    """
    This view is for getting a calendar, but also getting periods with that
    calendar.  Which periods you get, is designated with the list periods. You
    can designate which date you the periods to be initialized to by passing
    a date in request.GET. See the template tag ``query_string_for_date``

    Context Variables

    ``date``
        This was the date that was generated from the query string.

    ``periods``
        this is a dictionary that returns the periods from the list you passed
        in.  If you passed in Month and Day, then your dictionary would look
        like this

        {
            'month': <schedule.periods.Month object>
            'day':   <schedule.periods.Day object>
        }

        So in the template to access the Day period in the context you simply
        use ``periods.day``.

    ``calendar``
        This is the Calendar that is designated by the ``calendar_slug``.

    ``weekday_names``
        This is for convenience. It returns the local names of weekedays for
        internationalization.

    """
    user_account = request.user.userdetails.account
    slugs = None
    period_objects = None
    calendar = None

    date = coerce_date_dict(request.GET)
    if date:
        try:
            date = pytz.timezone(timezone.get_current_timezone_name()).localize(datetime.datetime(**date)) #datetime.datetime(**date) 
        except ValueError:
            raise Http404
    else:
        date = timezone.now()

    #Determine if we should display a specific calendar
    # or the daily agenda for the account (all their aircrafts' calendars)

    if calendar_slug is not None:
        calendar = get_object_or_404(Calendar, slug=calendar_slug, account=user_account)
        event_list = GET_EVENTS_FUNC(request, calendar)
        period_objects = dict([(period.__name__.lower(), period(event_list, date)) for period in periods])
        default_view = full_calendar_avail_views[list(period_objects)[0]]
    else:
        # get all slugs for this account
        slugs = {cal.slug: cal.calendarrelation_set.all()[0].content_object.calendar_colour for cal in Calendar.objects.filter(account=user_account)}
        default_view = full_calendar_avail_views['day']

    context = RequestContext(request)
    actions = ActionList.GetActionsForContext(context)
    #Add a "New" action for events
    if calendar_slug is not None:
        actions.insertAction(reverse( "calendar_create_event", kwargs= {'calendar_slug': calendar_slug,}), 'New Event', divider_before=True)
    else:
        actions.insertAction(reverse( "calendar_create_event_no_cal"), 'New Event', divider_before=True)
    #List the other aircraft calendars available 
    calendars = Calendar.objects.exclude(slug=calendar_slug)
    for idx, cal in enumerate(calendars, start = 1):
        actions.insertAction(url_list[periods[0].__name__.lower()](cal.slug, date), cal.name, divider_before=(idx == 1))

    return render_to_response(template_name,{
            'date': date,
            'periods': period_objects,
            'view': default_view,
            'calendar': calendar,
            'slugs': slugs,
            'weekday_names': weekday_names,
            'here':quote(request.get_full_path()),
        },context_instance=context,)

def ajax_events_for_period(request, calendar_slug):
    user_account = request.user.userdetails.account
    calendar = get_object_or_404(Calendar, slug=calendar_slug, account=user_account)
    start = datetime.datetime.strptime(request.GET.get('start', datetime.date.today().strftime("%Y-%m-%d")), "%Y-%m-%d")
    end = datetime.datetime.strptime(request.GET.get('end', datetime.date.today().strftime("%Y-%m-%d")), "%Y-%m-%d")
    show_cancelled_events = (request.GET.get('show_canc').lower() == 'true')
    tz = request.GET.get('timezone', timezone.get_current_timezone_name())
    if start == end:
        end += datetime.timedelta(days=1)
    event_list = GET_EVENTS_FUNC(request, calendar, start, end)
    requested_period = Period(event_list, start, end)
    json_list = []
    for entry in requested_period.get_occurrences():
        if not show_cancelled_events and entry.cancelled: continue
        id = entry.event.id
        title = entry.title
        occ_start = entry.start.astimezone(pytz.timezone(tz)).isoformat()
        occ_end = entry.end.astimezone(pytz.timezone(tz)).isoformat()
        allDay = False
        if entry.event.has_rule:
            url = entry.get_edit_url()
        else:
            url = reverse('edit_event', kwargs={'calendar_slug':calendar_slug, 'event_id':entry.event.id})
        cls = ''
        if entry.event.is_aircraft_event:
            if entry.event.aircraft_data.is_maintenance: cls=' maint-event' 
            if not entry.event.aircraft_data.confirmed: cls = cls + ' unconfirmed'
        if entry.cancelled: cls = cls + ' canc-event'

        json_entry = {'id':id, 'start':occ_start, 'end':occ_end, 'allDay':allDay, 'title': title, 'url': url, 'className': cls}
        json_list.append(json_entry)
    return HttpResponse(json.dumps(json_list), content_type='application/json')

def event(request, event_id, template_name="schedule/event.html"):
    """
    This view is for showing an event. It is important to remember that an
    event is not an occurrence.  Events define a set of reccurring occurrences.
    If you would like to display an occurrence (a single instance of a
    recurring event) use occurrence.

    Context Variables:

    event
        This is the event designated by the event_id

    back_url
        this is the url that referred to this view.
    """
    user_account = request.user.userdetails.account
    event = get_object_or_404(Event, id=event_id, account=user_account)
    back_url = request.META.get('HTTP_REFERER', None)
    try:
        cal = event.calendar_set.get()
    except:
        cal = None
    return render_to_response(template_name, {
        "event": event,
        "back_url" : back_url,
    }, context_instance=RequestContext(request))

def occurrence(request, event_id,
    template_name="schedule/occurrence.html", *args, **kwargs):
    """
    This view is used to display an occurrence.

    Context Variables:

    ``event``
        the event that produces the occurrence

    ``occurrence``
        the occurrence to be displayed

    ``back_url``
        the url from which this request was refered
    """
    event, occurrence = get_occurrence(event_id, *args, **kwargs)
    back_url = request.META.get('HTTP_REFERER', None)
    return render_to_response(template_name, {
        'event': event,
        'occurrence': occurrence,
        'back_url': back_url,
    }, context_instance=RequestContext(request))


@check_event_permissions
def edit_occurrence(request, event_id,
    template_name="schedule/edit_occurrence.html", *args, **kwargs):
    user_account = request.user.userdetails.account
    event, occurrence = get_occurrence(event_id, *args, **dict(user_account=user_account, **kwargs))
    if event.account != user_account:
        raise Http404
    next = kwargs.get('next', None)
    form = OccurrenceForm(data=request.POST or None, instance=occurrence)
    if form.is_valid():
        occurrence = form.save(commit=False)
        occurrence.event = event
        occurrence.save()
        next = next or get_next_url(request, occurrence.get_absolute_url())
        return HttpResponseRedirect(next)
    next = next or get_next_url(request, occurrence.get_absolute_url())
    return render_to_response(template_name, {
        'form': form,
        'occurrence': occurrence,
        'next':next,
    }, context_instance=RequestContext(request))


@check_event_permissions
def cancel_occurrence(request, event_id,
    template_name='schedule/cancel_occurrence.html', *args, **kwargs):
    """
    This view is used to cancel an occurrence. If it is called with a POST it
    will cancel the view. If it is called with a GET it will ask for
    conformation to cancel.
    """
    event, occurrence = get_occurrence(event_id, *args, **kwargs)
    next = kwargs.get('next',None) or get_next_url(request, event.get_absolute_url())
    if request.method != "POST":
        return render_to_response(template_name, {
            "occurrence": occurrence,
            "next":next,
        }, context_instance=RequestContext(request))
    occurrence.cancel()
    return HttpResponseRedirect(next)


def get_occurrence(event_id, occurrence_id=None, year=None, month=None,
    day=None, hour=None, minute=None, second=None, user_account=None):
    """
    Because occurrences don't have to be persisted, there must be two ways to
    retrieve them. both need an event, but if its persisted the occurrence can
    be retrieved with an id. If it is not persisted it takes a date to
    retrieve it.  This function returns an event and occurrence regardless of
    which method is used.
    """
    if(occurrence_id):
        occurrence = get_object_or_404(Occurrence, id=occurrence_id)
        event = occurrence.event
    elif(all((year, month, day, hour, minute, second))):
        event = get_object_or_404(Event, id=event_id, account=user_account.id)
        # TODO For now, assume we're being passed UTC times
        occurrence = event.get_occurrence(
            datetime.datetime(int(year), int(month), int(day), int(hour),
                int(minute), int(second),0,tzinfo=pytz.UTC))
        if occurrence is None:
            raise Http404
    else:
        raise Http404
    return event, occurrence


@check_event_permissions
def create_or_edit_event(request, calendar_slug=None, event_id=None, next=None,
    template_name='schedule/create_event.html', form_class=EventForm):
    """
    This function, if it receives a GET request or if given an invalid form in a
    POST request it will generate the following response

    Template:
        schedule/create_event.html

    Context Variables:

    form:
        an instance of EventForm

    calendar:
        a Calendar with id=calendar_id

    if this function gets a GET request with ``year``, ``month``, ``day``,
    ``hour``, ``minute``, and ``second`` it will auto fill the form, with
    the date specifed in the GET being the start and 30 minutes from that
    being the end.

    If this form receives an event_id it will edit the event with that id, if it
    recieves a calendar_id and it is creating a new event it will add that event
    to the calendar with the id calendar_id

    If it is given a valid form in a POST request it will redirect with one of
    three options, in this order

    # Try to find a 'next' GET variable
    # If the key word argument redirect is set
    # Lastly redirect to the event detail of the recently create event
    """
    user_account = request.user.userdetails.account
    date = coerce_date_dict(request.GET)
    initial_data = None
    if date:
        try:
            start = datetime.datetime(**date)
            initial_data = {
                "start": start,
                "end": start + datetime.timedelta(minutes=30)
            }
        except TypeError:
            raise Http404
        except ValueError:
            raise Http404

    instance = None
    if event_id is not None:
        instance = get_object_or_404(Event, id=event_id, account=user_account)

    calendar = None
    if calendar_slug is not None:
        calendar = get_object_or_404(Calendar, slug=calendar_slug, account=user_account)

    form = form_class(data=request.POST or None, instance=instance,
        hour24=True, initial=initial_data)
    rule_set = RuleFormSet(data=request.POST or None, instance=instance)

    form_valid = form.is_valid()
    has_recur = request.POST.get('has_recurrence', 'False') == 'True'
    rule_valid = rule_set.is_valid() and rule_set.forms[0].is_valid()

    if form_valid and (not has_recur or rule_valid):
        event = form.save(commit=False)
        if instance is None and calendar_slug is not None:
            #event.creator = request.user <-- No longer required, handled by core.middleware
            event.calendar = calendar
        event.save()
        if has_recur and rule_valid: 
            rule_form = rule_set.forms[0]
            if rule_form.is_valid():
                rule_item = rule_form.save(commit=False)
                rule_item.event = event
                rule_item.save()

        if not request.is_ajax():
            next = next or reverse('event', args=[event.id])
            next = get_next_url(request, next)
            return HttpResponseRedirect(next)
        else:
            return "ok"

    if request.is_ajax():
        return render_to_response('schedule/_event_form.html', {
            "form": form,
            "formset": rule_set,
            "calendar": calendar,
            "ajax": True,
        }, context_instance=RequestContext(request))

    next = get_next_url(request, next)
    return render_to_response(template_name, {
        "form": form,
        "formset": rule_set,
        "calendar": calendar,
        "next":next,
        "event":instance,
    }, context_instance=RequestContext(request))

@check_event_permissions
def delete_event(request, event_id, next=None, login_required=True):
    """
    After the event is deleted there are three options for redirect, tried in
    this order:

    # Try to find a 'next' GET variable
    # If the key word argument redirect is set
    # Lastly redirect to the event detail of the recently create event
    """
    user_account = request.user.userdetails.account
    event = get_object_or_404(Event, id=event_id, account=user_account)
    next = next or reverse('day_calendar', args=[event.calendar.slug])
    next = get_next_url(request, next)
    return delete_object(request,
                         model = Event,
                         object_id = event_id,
                         post_delete_redirect = next,
                         template_name = "schedule/delete_event.html",
                         extra_context = dict(next=next),
                         login_required = login_required
                        )

#TODO Remove?
def edit_rule(request):
    if request.method == 'POST': # If the form has been submitted...
        form = EventForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            return HttpResponseRedirect('/thanks/') # Redirect after POST
    else:
        form = EventForm() # An unbound form

    return render_to_response('schedule/rule_form.html', {
        'form': form,
    },context_instance=RequestContext(request))


def check_next_url(next):
    """
    Checks to make sure the next url is not redirecting to another page.
    Basically it is a minimal security check.
    """
    if not next or '://' in next:
        return None
    return next

def get_next_url(request, default):
    next = default
    if OCCURRENCE_CANCEL_REDIRECT:
        next = OCCURRENCE_CANCEL_REDIRECT
    if 'next' in request.REQUEST and check_next_url(request.REQUEST['next']) is not None:
        next = request.REQUEST['next']
    return next
