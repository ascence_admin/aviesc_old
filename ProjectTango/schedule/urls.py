#from django.conf.urls.defaults import *
from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic.list import ListView
from schedule.models import Calendar
from schedule.feeds import UpcomingEventsFeed
from schedule.feeds import CalendarICalendar
from schedule.periods import Year, Month, Week, Day
from schedule.views import ScheduleView

from rest_framework import routers
from schedule import api


info_dict = {
    'queryset': Calendar.objects.all(),
}

router = routers.DefaultRouter()
router.register(r'events', api.EventViewSet)

urlpatterns = patterns('',
                       url(r'^api/', include(router.urls)),
                       # urls for Calendars
                       url(r'^calendar/$',
                           'schedule.views.calendar_by_periods', name="schedule",
                           kwargs={'periods': [Day], 'template_name': 'schedule/cal.html'}),

                       url(r'^calendar/year/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.calendar_by_periods',
                           name="year_calendar",
                           kwargs={'periods': [Year], 'template_name': 'schedule/calendar_year.html'}),

                       url(r'^calendar/tri_month/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.calendar_by_periods',
                           name="tri_month_calendar",
                           kwargs={'periods': [Month], 'template_name': 'schedule/calendar_tri_month.html'}),

                       url(r'^calendar/compact_month/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.calendar_by_periods',
                           name="compact_calendar",
                           kwargs={'periods': [Month], 'template_name': 'schedule/calendar_compact_month.html'}),

                       url(r'^calendar/month/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.calendar_by_periods',
                           name="month_calendar",
                           kwargs={'periods': [Month]}),

                       url(r'^calendar/week/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.calendar_by_periods',
                           name="week_calendar",
                           kwargs={'periods': [Week]}),

                       url(r'^calendar/daily/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.calendar_by_periods',
                           name="day_calendar",
                           kwargs={'periods': [Day]}),

                       url(r'^calendar/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.calendar',
                           name="calendar_home",
                       ),
                       url(r'^calendar/dyn/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.calendar_by_periods',
                           name="ajax_calendar",
                           kwargs={'periods': [Week], 'template_name': 'schedule/cal.html'}),
                       url(r'^calendar/dynfeed/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.ajax_events_for_period',
                           name="ajax_event_feed",
                       ),

                       #Event Urls
                       url(r'^event/create/$',
                           'schedule.views.create_or_edit_event',
                           name='calendar_create_event_no_cal'),
                       url(r'^event/create/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.create_or_edit_event',
                           name='calendar_create_event'),
                       url(r'^event/edit/(?P<calendar_slug>[-\w]+)/(?P<event_id>\d+)/$',
                           'schedule.views.create_or_edit_event',
                           name='edit_event'),
                       url(r'^event/(?P<event_id>\d+)/$',
                           'schedule.views.event',
                           name="event"),
                       url(r'^event/delete/(?P<event_id>\d+)/$',
                           'schedule.views.delete_event',
                           name="delete_event"),
                       url(r'^event/createa/(?P<calendar_slug>[-\w]+)/$',
                           'schedule.views.create_or_edit_event',
                           name='ajax_create_event',
                           kwargs={'template_name': 'schedule/_event_form.html'}),
                       url(r'^event/edita/(?P<calendar_slug>[-\w]+)/(?P<event_id>\d+)/$',
                           'schedule.views.create_or_edit_event',
                           name='ajax_edit_event',
                           kwargs={'template_name': 'schedule/_event_form.html'}),

                       #Rule Urls
                       url(r'^rule/create/$',
                           'schedule.views.edit_rule',
                           name="create_rule"),

                       #urls for already persisted occurrences
                       url(r'^occurrence/(?P<event_id>\d+)/(?P<occurrence_id>\d+)/$',
                           'schedule.views.occurrence',
                           name="occurrence"),
                       url(r'^occurrence/cancel/(?P<event_id>\d+)/(?P<occurrence_id>\d+)/$',
                           'schedule.views.cancel_occurrence',
                           name="cancel_occurrence"),
                       url(r'^occurrence/edit/(?P<event_id>\d+)/(?P<occurrence_id>\d+)/$',
                           'schedule.views.edit_occurrence',
                           name="edit_occurrence"),

                       #urls for unpersisted occurrences
                       url(
                           r'^occurrence/(?P<event_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/(?P<hour>\d+)/(?P<minute>\d+)/(?P<second>\d+)/$',
                           'schedule.views.occurrence',
                           name="occurrence_by_date"),
                       url(
                           r'^occurrence/cancel/(?P<event_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/(?P<hour>\d+)/(?P<minute>\d+)/(?P<second>\d+)/$',
                           'schedule.views.cancel_occurrence',
                           name="cancel_occurrence_by_date"),
                       url(
                           r'^occurrence/edit/(?P<event_id>\d+)/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/(?P<hour>\d+)/(?P<minute>\d+)/(?P<second>\d+)/$',
                           'schedule.views.edit_occurrence',
                           name="edit_occurrence_by_date"),


                       #feed urls
                       #url(r'^feed/calendar/(.*)/$',
                       #    'django.contrib.syndication.views.Feed',
                       #    { "feed_dict": { "upcoming": UpcomingEventsFeed } }),

                       (r'^ical/calendar/(.*)/$', CalendarICalendar()),


                       url(r'$', ScheduleView.as_view(), info_dict, name='schedule'),
)