from django.conf.urls import patterns, include, url
from core import views
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),
    # url(r'^ProjectTango/', include('ProjectTango.ProjectTango.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^aircraft/', include('aircraft.urls')),
    url(r'^log/', include('aircraft.urls_logs')),
    url(r'^rule/', include('aircraft.urls_rules')),

    # AllAuth URLS
    url(r'^accounts/', include('allauth.urls')),

    # Calendar URLs
    url(r'^schedule/', include('schedule.urls')),

    # Customer
    url(r'^customer/', include('customer.urls')),

    # Invoices
    url(r'^invoice/', include('invoice.urls')),

    # Django rest framework
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # Core app URLs
    url(r'^/', include('core.urls')),


 ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
print(settings.MEDIA_ROOT)