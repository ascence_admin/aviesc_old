### -*- coding: utf-8 -*- ###

import os.path

ADMINS = (
    ('Jakub', 'jwisniowski@milosolutions.com'),
)


PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tango',
        'USER': 'django_dev',
        'PASSWORD': 'Password1',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}

# More overwrites here.
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(PROJECT_ROOT, 'tmp', 'emails')


LOCAL_APPS = ()
