from __future__ import absolute_import
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ProjectTango.settings')

# instantiate Celery object
celery = Celery(include=[
    'invoice.tasks',
], backend='djcelery.backends.database:DatabaseBackend')

celery.config_from_object('django.conf:settings')

if __name__ == '__main__':
    celery.start()