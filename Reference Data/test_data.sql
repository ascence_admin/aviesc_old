/* Aircraft to an Account
*/
INSERT INTO aircraftAccount (account_id, aircraft_mark, hourly_rate, max_pax, takeoff_cost, avg_speed) VALUES (1, 'RDU', 500.0, 4, 75.0, 300.0);
INSERT INTO aircraftAccount (account_id, aircraft_mark, hourly_rate, max_pax, takeoff_cost, avg_speed) VALUES (1, 'UJE', 600.0, 6, 100.0, 350.0);
INSERT INTO aircraftAccount (account_id, aircraft_mark, hourly_rate, max_pax, takeoff_cost, avg_speed) VALUES (1, 'UJY', 700.0, 6, 100.0, 350.0);
INSERT INTO aircraftAccount (account_id, aircraft_mark, hourly_rate, max_pax, takeoff_cost, avg_speed) VALUES (1, 'VHC', 800.0, 4, 150.0, 275.0);
INSERT INTO aircraftAccount (account_id, aircraft_mark, hourly_rate, max_pax, takeoff_cost, avg_speed) VALUES (1, 'VHE', 900.0, 5, 70.0, 300.0);

/* Populate Aircraft Type Table */
DELETE FROM aircraftType;
INSERT INTO aircraftType (aircraft_name, hourly_rate, max_pax, takeoff_cost, avg_speed, account_id)
    SELECT DISTINCT CONCAT(aircraft.Type, ' (', aircraft.Manu, ')'), 350.0, 6, 50.0, 400.0, 1
    FROM aircraft, aircraftAccount
    WHERE aircraftAccount.account_id = 1
    AND aircraft.mark = aircraftAccount.aircraft_mark;

/* Update the aircraftAccount table to refer to the proper Aircraft Type ID
*/
UPDATE aircraftAccount, aircraftType, aircraft
SET aircraftAccount.aircraft_type_id = aircraftType.aircraft_type_id, aircraftAccount.account_id = 1
WHERE aircraftType.aircraft_name = CONCAT(aircraft.Type, ' (', aircraft.Manu, ')')
AND aircraft.mark = aircraftAccount.aircraft_mark;

/*Journey Test data
*/
INSERT INTO journeyMaster (journey_id, account_id, aircraft_id, pilot_id, start_dt) SELECT 1, 1, aircraft_id, 0, '2011-04-18' FROM aircraftAccount WHERE aircraft_mark = 'RDU';
INSERT INTO journeyLocation (journey_id, sequence_no, location_id) SELECT max(journey_id), 1, 1000 FROM journeyMaster;
INSERT INTO journeyLocation (journey_id, sequence_no, location_id) SELECT max(journey_id), 2, 1001 FROM journeyMaster;
INSERT INTO journeyLocation (journey_id, sequence_no, location_id) SELECT max(journey_id), 3, 1002 FROM journeyMaster;
INSERT INTO journeyMaster (journey_id, account_id, aircraft_id, pilot_id, start_dt) SELECT 2, 1, aircraft_id, 100, '2011-04-18' FROM aircraftAccount WHERE aircraft_mark = 'UJE';
INSERT INTO journeyLocation (journey_id, sequence_no, location_id) SELECT max(journey_id), 1, 1010 FROM journeyMaster;
INSERT INTO journeyLocation (journey_id, sequence_no, location_id) SELECT max(journey_id), 2, 1011 FROM journeyMaster;
INSERT INTO journeyLocation (journey_id, sequence_no, location_id) SELECT max(journey_id), 3, 1012 FROM journeyMaster;


/* Quote Test Data
*/
INSERT INTO client (client_id, account_id, client_name, phone1, address_1, suburb, postcode, country, email, is_deleted)
    VALUES (1, 1, 'First client', '0712345678', 'PO Box 1', 'Cairns', '4900', 'Australia', '', 'N');
INSERT INTO client (client_id, account_id, client_name, phone1, address_1, suburb, postcode, country, email, is_deleted)
    VALUES (2, 1, 'Second client', '08987654321', NULL, NULL, NULL, NULL, NULL, 'N');

INSERT INTO quote (quote_id, account_id, client_id, journey_id, quote_dt, operating_cost, takeoff_cost, other_cost_1, other_cost_2, aircraft_hourly_rate, is_deleted)
    VALUES (1, 1, 1, 1, '2011-06-01', 100.0, 50.0, 10.0, 0.0, 200.0, 'N');
INSERT INTO quote (quote_id, account_id, client_id, journey_id, quote_dt, operating_cost, takeoff_cost, other_cost_1, other_cost_2, aircraft_hourly_rate, is_deleted)
    VALUES (2, 1, 1, 2, '2011-06-02', 110.0, 50.0, 0.0, 0.0, 150.0, 'N');
