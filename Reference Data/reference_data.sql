/*
*   Application Reference Data
*
*/

/* Global Account ID */
DELETE FROM account;
INSERT INTO account (account_id, account_name, start_dt, inactive) VALUES (1, 'Global Account', '2011-06-01', 0);


/* Passenger Cateogory for Quotes */
DROP TABLE IF EXISTS ref_pax_category;
CREATE TABLE ref_pax_category (
     pax_category_id SERIAL PRIMARY KEY
    ,pax_category VARCHAR(50)
    ,default_weight INT NOT NULL DEFAULT 0
);
INSERT INTO ref_pax_category (pax_category, default_weight) VALUES ('Male', 80);
INSERT INTO ref_pax_category (pax_category, default_weight) VALUES ('Female', 70);
INSERT INTO ref_pax_category (pax_category, default_weight) VALUES ('Adolescent', 65);
INSERT INTO ref_pax_category (pax_category, default_weight) VALUES ('Child', 40);
INSERT INTO ref_pax_category (pax_category, default_weight) VALUES ('Baby', 20);
