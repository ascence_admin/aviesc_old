# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Account(models.Model):
    account_id = models.BigIntegerField(primary_key=True)
    account_name = models.CharField(max_length=255)
    start_dt = models.DateField(blank=True, null=True)
    inactive = models.CharField(max_length=1)
    class Meta:
        db_table = 'account'

class Aircrafttype(models.Model):
    aircraft_type_id = models.BigIntegerField(primary_key=True)
    account_id = models.ForeignKey(Account)
    aircraft_name = models.CharField(max_length=255, blank=True)
    hourly_rate = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    max_pax = models.IntegerField(blank=True, null=True)
    takeoff_cost = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    avg_speed = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    created_dt = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50)
    modified_dt = models.DateTimeField(auto_now=True)
    modified_by = models.CharField(max_length=50)
    class Meta:
        db_table = 'aircrafttype'

class Aircraft(models.Model):
    mark = models.CharField(primary_key=True, max_length=3)
    manu = models.CharField(max_length=60, blank=True, db_column='Manu') # Field name made lowercase.
    aircrafttype = models.CharField(max_length=30, blank=True, db_column='aircraftType') # Field name made lowercase.
    model = models.CharField(max_length=28, blank=True, db_column='Model') # Field name made lowercase.
    aircraftserial = models.CharField(max_length=18, blank=True, db_column='aircraftSerial') # Field name made lowercase.
    mtow = models.IntegerField(blank=True, null=True, db_column='MTOW') # Field name made lowercase.
    engnum = models.IntegerField(blank=True, null=True)
    engmanu = models.CharField(max_length=60, blank=True, db_column='Engmanu') # Field name made lowercase.
    engtype = models.CharField(max_length=14, blank=True, db_column='Engtype') # Field name made lowercase.
    engmodel = models.CharField(max_length=50, blank=True, db_column='Engmodel') # Field name made lowercase.
    fueltype = models.CharField(max_length=14, blank=True, db_column='Fueltype') # Field name made lowercase.
    regtype = models.CharField(max_length=17, blank=True, db_column='regType') # Field name made lowercase.
    regholdname = models.CharField(max_length=255, blank=True)
    regholdadd1 = models.CharField(max_length=40, blank=True)
    regholdadd2 = models.CharField(max_length=40, blank=True)
    regholdsuburb = models.CharField(max_length=39, blank=True, db_column='regholdSuburb') # Field name made lowercase.
    regholdstate = models.CharField(max_length=10, blank=True, db_column='regholdState') # Field name made lowercase.
    regholdpostcode = models.CharField(max_length=11, blank=True, db_column='regholdPostcode') # Field name made lowercase.
    regholdcountry = models.CharField(max_length=100, blank=True, db_column='regholdCountry') # Field name made lowercase.
    regholdcommdate = models.DateField(blank=True, null=True, db_column='regholdCommdate') # Field name made lowercase.
    regopname = models.CharField(max_length=74, blank=True, db_column='regopName') # Field name made lowercase.
    regopadd1 = models.CharField(max_length=40, blank=True)
    regopadd2 = models.CharField(max_length=40, blank=True)
    regopsuburb = models.CharField(max_length=39, blank=True, db_column='regopSuburb') # Field name made lowercase.
    regopstate = models.CharField(max_length=10, blank=True, db_column='regopState') # Field name made lowercase.
    regoppostcode = models.CharField(max_length=11, blank=True, db_column='regopPostcode') # Field name made lowercase.
    regopcountry = models.CharField(max_length=100, blank=True, db_column='regopCountry') # Field name made lowercase.
    regopcommdate = models.DateField(blank=True, null=True, db_column='regopCommdate') # Field name made lowercase.
    datefirstreg = models.DateField(blank=True, null=True, db_column='Datefirstreg') # Field name made lowercase.
    gear = models.CharField(max_length=30, blank=True)
    airframe = models.CharField(max_length=30, blank=True, db_column='Airframe') # Field name made lowercase.
    coacata = models.CharField(max_length=20, blank=True, db_column='CoAcata') # Field name made lowercase.
    coacatb = models.CharField(max_length=20, blank=True, db_column='CoAcatb') # Field name made lowercase.
    coacatc = models.CharField(max_length=20, blank=True, db_column='CoAcatc') # Field name made lowercase.
    propmanu = models.CharField(max_length=100, blank=True, db_column='Propmanu') # Field name made lowercase.
    propmodel = models.CharField(max_length=30, blank=True, db_column='Propmodel') # Field name made lowercase.
    typecert = models.CharField(max_length=15, blank=True, db_column='Typecert') # Field name made lowercase.
    countrymanu = models.CharField(max_length=100, blank=True, db_column='Countrymanu') # Field name made lowercase.
    yearmanu = models.IntegerField(blank=True, null=True, db_column='Yearmanu') # Field name made lowercase.
    regexpirydate = models.DateField(blank=True, null=True, db_column='Regexpirydate') # Field name made lowercase.
    suspendstatus = models.CharField(max_length=30, blank=True)
    suspenddate = models.DateField(blank=True, null=True)
    icaotypedesig = models.CharField(max_length=6, blank=True, db_column='ICAOtypedesig') # Field name made lowercase.
    class Meta:
        db_table = 'aircraft'

class Journeymaster(models.Model):
    journey_id = models.BigIntegerField(primary_key=True)
    account_id = models.ForeignKey(Account)
    aircraft_id = models.ForeignKey(Aircraftaccount, blank=True)
    pilot_id = models.IntegerField(blank=True, null=True) # TODO
    start_dt = models.DateField(blank=True, null=True)
    flight_time = models.TimeField(blank=True, null=True)
    landing_count = models.IntegerField(blank=True, null=True)
    engine_start = models.IntegerField(blank=True, null=True)
    has_occurred = models.BooleanField()
    is_deleted = models.BooleanField()
    created_dt = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50)
    modified_dt = models.DateTimeField(auto_now=True)
    modified_by = models.CharField(max_length=50)
    class Meta:
        db_table = 'journeymaster'

class Aircraftaccount(models.Model):
    aircraft_id = models.BigIntegerField(primary_key=True)
    account_id = models.ForeignKey(Account)
    aircraft_mark = models.ForeignKey(Aircraft, blank=True)
    aircraft_type_id = models.ForeignKey(Aircrafttype)
    aircraft_description = models.CharField(max_length=100, blank=True)
    hourly_rate = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    max_pax = models.IntegerField(blank=True, null=True)
    takeoff_cost = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    avg_speed = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    is_deleted = models.BooleanField()
    created_dt = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50)
    modified_dt = models.DateTimeField(auto_now=True)
    modified_by = models.CharField(max_length=50)
    class Meta:
        db_table = 'aircraftaccount'

class Client(models.Model):
    client_id = models.BigIntegerField(primary_key=True)
    account_id = models.ForeignKey(Account)
    client_name = models.CharField(max_length=255)
    phone1 = models.CharField(max_length=20, blank=True)
    phone2 = models.CharField(max_length=20, blank=True)
    address_1 = models.CharField(max_length=50, blank=True)
    address_2 = models.CharField(max_length=50, blank=True)
    suburb = models.CharField(max_length=70, blank=True)
    postcode = models.CharField(max_length=10, blank=True)
    country = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=255, blank=True)
    is_deleted = models.BooleanField()
    created_dt = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50)
    modified_dt = models.DateTimeField(auto_now=True)
    modified_by = models.CharField(max_length=50)
    class Meta:
        db_table = 'client'

class Invoice(models.Model):
    invoice_id = models.BigIntegerField(primary_key=True)
    account_id = models.ForeignKey(Account)
    client_id = models.ForeignKey(Client)
    journey_id = models.ForeignKey(Journeymaster)
    aircraft_id = models.ForeignKey(Aircraftaccount)
    quote_dt = models.DateField(blank=True, null=True)
    operating_cost = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    takeoff_cost = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    other_cost_1 = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    other_cost_2 = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    aircraft_hourly_rate = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    is_deleted = models.BooleanField()
    created_dt = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=50)
    modified_dt = models.DateTimeField(auto_now=True)
    modified_by = models.CharField(max_length=50)
    class Meta:
        db_table = 'invoice'

class Journeylocation(models.Model):
    journey_id = models.BigIntegerField()
    sequence_no = models.IntegerField()
    location_id = models.IntegerField()
    is_landing = models.CharField(max_length=1)
    created_dt = models.DateTimeField()
    created_by = models.CharField(max_length=50)
    modified_dt = models.DateTimeField()
    modified_by = models.CharField(max_length=50)
    class Meta:
        db_table = 'journeylocation'

class Location(models.Model):
    location_id = models.BigIntegerField(unique=True)
    account_id = models.IntegerField()
    location_name = models.CharField(max_length=255)
    country = models.CharField(max_length=100, blank=True)
    iata_cd = models.CharField(max_length=5, blank=True)
    icao_cd = models.CharField(max_length=5, blank=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    is_deleted = models.CharField(max_length=1)
    created_dt = models.DateTimeField()
    created_by = models.CharField(max_length=50)
    modified_dt = models.DateTimeField()
    modified_by = models.CharField(max_length=50)
    class Meta:
        db_table = 'location'

class Preferences(models.Model):
    account_id = models.IntegerField()
    preference_name = models.CharField(max_length=50)
    preference_setting = models.CharField(max_length=100, blank=True)
    class Meta:
        db_table = 'preferences'

class Quote(models.Model):
    quote_id = models.BigIntegerField(unique=True)
    account_id = models.IntegerField()
    client_id = models.IntegerField()
    journey_id = models.IntegerField()
    aircraft_type_id = models.IntegerField()
    quote_dt = models.DateField(blank=True, null=True)
    operating_cost = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    takeoff_cost = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    other_cost_1 = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    other_cost_2 = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    aircraft_hourly_rate = models.DecimalField(decimal_places=4, max_digits=17, null=True, blank=True)
    total_cargo_weight = models.IntegerField()
    is_deleted = models.CharField(max_length=1)
    created_dt = models.DateTimeField()
    created_by = models.CharField(max_length=50)
    modified_dt = models.DateTimeField()
    modified_by = models.CharField(max_length=50)
    class Meta:
        db_table = 'quote'

class QuotePax(models.Model):
    pax_id = models.BigIntegerField(unique=True)
    quote_id = models.IntegerField()
    account_id = models.IntegerField()
    pax_name = models.CharField(max_length=200, blank=True)
    pax_category_id = models.IntegerField()
    pax_luggage_weight = models.IntegerField()
    created_dt = models.DateTimeField()
    created_by = models.CharField(max_length=50)
    modified_dt = models.DateTimeField()
    modified_by = models.CharField(max_length=50)
    class Meta:
        db_table = 'quote_pax'

class RefPaxCategory(models.Model):
    pax_category_id = models.BigIntegerField(unique=True)
    pax_category = models.CharField(max_length=50, blank=True)
    default_weight = models.IntegerField()
    class Meta:
        db_table = 'ref_pax_category'


